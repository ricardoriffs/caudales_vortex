<?php

/*
|--------------------------------------------------------------------------
| Rutas Web
|--------------------------------------------------------------------------
|
| Aqui se crean las rutas de la aplicación 
| 
| 
|
*/

Auth::routes();

Route::get('/', function () {
    return view('auth.login');
});


// rutas para acceder si ya se ha iniciado sesion
Route::get('/home', 'HomeController@index')->name('home');
 



Route::group(['middleware'=>'auth'], function()
{
    
  Route::name('register')->get('/register','Auth\RegisterController@showRegistrationForm');
  Route::name('register')->post('/register','Auth\RegisterController@register');
  Route::resource('notificacions','NotificacionController');
  Route::resource('parametros','ParametroController');
  Route::resource('tipofuentes','TipoFuentesController');
  Route::resource('tipousos','TipousoController');
  Route::resource('buscarcaptaciones','BuscarCaptacionesController'); 
  Route::resource('captaciones','CaptacionController');
  Route::resource('fuenteshidrograficas','FuenteHidrograficaController');
  Route::resource('cuencashidrograficas','CuencaHidrograficaController');
  Route::resource('subcuencashidrograficas','SubCuencaHidrograficaController');
  Route::resource('tipoestructura','TipoEstructuraController');
  Route::resource('estructuracaptaciones','EstructuraCaptacionController');
  Route::resource('encuestados','EncuestadoController');
  Route::resource('predios','PredioController');
  Route::resource('aforos','AforoController');
  Route::resource('calidadesagua','CalidadAguaController');
  Route::resource('proposito','PropositoController');
  Route::resource('cuencas','CuencaController');
  Route::resource('acueductosvym','AcueductoVyMController');
  Route::resource('expedientesjuridicos','ExpedienteJuridicoController');
  Route::resource('tipousocategorias','TipoUsoCategoriaController');
  Route::resource('encuestadores','EncuestadoresController');
  Route::resource('fichatecnica','FichaTecnicaController');
  Route::resource('perfiles','PerfilController');
  Route::resource('dependencias','DependenciaController');
  Route::resource('departamentos','DepartamentoController');
  Route::resource('municipios','MunicipioController');
  Route::resource('users','userController');
  Route::resource('veredas','VeredaController'); 
  Route::resource('acueductos','AcueductoController');
  Route::resource('modulos','ModuloController');
  Route::resource('usosdelpredio','UsoDelPredioController');
  Route::resource('logs','LogController');
  Route::resource('tipousoscategorias','TipoUsoCategoriaController');
  Route::resource('modulos','ModuloController');
  Route::resource('pisostermicos','PisoTermicoController');
  Route::resource('medidasconsumo','MedidaConsumoController');
  Route::resource('usospredio','UsoPredioController');

  Route::post('reportefichatecnica',array('as'=>'fichatecnica.reportefichatecnica','uses'=>'FichaTecnicaController@reportefichatecnica'));
  Route::get('captaciones/SubCuencas/{id}',array('as'=>'captaciones.SubCuencas','uses'=>'CaptacionController@consultarSubCuencas'));
  Route::get('consultafichatecnica',array('as'=>'fichatecnica.consultafichatecnica','uses'=>'FichaTecnicaController@consultarfichatecnica'));
  Route::post('generarfichatecnica',array('as'=>'fichatecnica.generarfichatecnica','uses'=>'FichaTecnicaController@generarfichatecnica'));    
  Route::get('captaciones/ConsultarFuentes/{id}',array('as'=>'captaciones.Fuentes','uses'=>'CaptacionController@consultarFuentes'));
  Route::get('predios/Municipios/{id}',array('as'=>'predios.Municipios','uses'=>'MunicipioController@consultarMunicipios'));
  Route::get('predios/Veredas/{id}',array('as'=>'predios.Veredas','uses'=>'VeredaController@consultarVeredas'));
  Route::get('captaciones/TipoUso/{id}',array('as'=>'captaciones.TipoUso','uses'=>'CaptacionController@consultarTiposDeUsos'));
  Route::get('captaciones/consultarCategoriasDelTipoUso/{id}',array('as'=>'captaciones.CategoriasTipoUso','uses'=>'CaptacionController@consultarCategoriasDelTipoUso'));
  Route::get('tipousos/Lineas/{id}',array('as'=>'tipousos.Lineas','uses'=>'TipoUsoController@consultarLineas'));
  Route::get('tipousos/Unidades/{id}',array('as'=>'tipousos.Unidades','uses'=>'TipoUsoController@consultarUnidades'));
  Route::get('nombreCuenca/{IDCuenca}/{IDSubCuenca}/{CodigoFuente}',array('as'=>'captaciones.Fuentes', 'uses'=>'CaptacionController@consultarNombreCuenca'));

});



