@extends('layouts.dashboard')
@section('page_heading','Crear expedientes juridicos')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('expedientesjuridicos.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Expedientes Juridicos
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Nuevo expediente juridico
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                        {!!Form::open(['route' => 'expedientesjuridicos.store'])!!}
                         {{ Form::hidden('TipoFormulario', 'individual') }}       
                        @include('partialsgeneral.nocaptacionsection')
                        @include('expedientesjuridicos.partials.form')
                        {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop

