@extends('layouts.dashboard')
@section('page_heading','Expedientes Juridicos')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <a class="btn btn-primary pull-right" href=" {{ route('expedientesjuridicos.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de expedientes juridicos
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            No.Captacion
                                        </th>
                                        <th>
                                            Concesion
                                        </th>
                                        <th>
                                            No.Expediente 
                                        </th>
                                       
                                        <th>
                                            No.Resolucion
                                        </th>
                                        
                                        <th>
                                            Vigencia
                                        </th>
                                        
                                        
                                        <th>
                                            Fecha Obra
                                        </th>
                                        <th>
                                            Oficina Provincial
                                        </th>
                                    
                                       
                                        

                                </thead>
                                <tbody>
                                    @foreach($expedientesjuridicos as $expedientejuridico)
                                    <tr>
                                        <td>
                                            {{$expedientejuridico->IDCaptacion}}
                                        </td>
                                        <td>
                                            {{$expedientejuridico->Concesion}}
                                        </td>
                                        <td>
                                            {{$expedientejuridico->ExpedienteNo}}
                                        </td>
                                        
                                        <td>
                                            {{$expedientejuridico->ResolucionNo}}
                                        </td>
                                        
                                        <td>
                                            Desde:{{$expedientejuridico->VigenciaDesde}}   Hasta: {{$expedientejuridico->VigenciaHasta}}
                                        </td>                                      
                                        
                                      
                                        <td>
                                            {{$expedientejuridico->FechaObra}}
                                        </td>
                                        <td>
                                            {{$expedientejuridico->OficinaProvincial}}
                                        </td>
                                      
                                      
                                      

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('expedientesjuridicos.edit', $expedientejuridico->IDExpediente)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $expedientesjuridicos->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

