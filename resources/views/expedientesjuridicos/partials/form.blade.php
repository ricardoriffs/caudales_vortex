<div class="row">
    <div class="col-md-6">
        <fieldset>        
            <legend>Resolución</legend>
            <br>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('lblResolucionNo','*No.') !!}
                    {!! Form::number('ResolucionNo',null, ['class' => 'form-control','step' => 'any']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('lblResolucionFecha','*Fecha') !!}
                    {!! Form::date('ResolucionFecha',null, ['class' => 'form-control']) !!}
                </div>     
            </div>
        </fieldset>
    </div>
    <div class="col-md-6">
        <fieldset>        
            <legend>Expediente</legend>
            <br>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('lblExpedienteNo','*No.') !!}
                    {!! Form::number('ExpedienteNo',null, ['class' => 'form-control','step' => 'any']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('lblExpedienteFecha','*Fecha') !!}
                    {!! Form::date('ExpedienteFecha',null, ['class' => 'form-control']) !!}
                </div>    
            </div>
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <fieldset>        
            <legend>Vigencia</legend>
            <br>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('lblVigenciaDesde','*Desde') !!}
                    {!! Form::date('VigenciaDesde',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('lblVigenciaHasta','*Hasta') !!}
                    {!! Form::date('VigenciaHasta',null, ['class' => 'form-control']) !!}
                </div>   
            </div>
        </fieldset>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4">
        <div class="form group">
            {!! Form::label('lblConcesion','*Concesión de aguas') !!}
            <select class="form-control" name="Concesion">
                <option value="">-- Seleccion --</option>
                <option value="Si">Si</option>
                <option value="No">No</option>
            </select>
        </div>  
    </div>
    <div class="col-md-4">
        <div class="form group">
            {!! Form::label('lblAprovacionDiseno','*Aprobación de Diseños') !!}
            {!! Form::text('AprovacionDiseno',null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form group">
            {!! Form::label('lblFechaObra','*Fecha recibo obra') !!}
            {!! Form::date('FechaObra',null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-4">
        <div class="form group">
            {!! Form::label('lblCaudalUtilizadoLPS','*Caudal Utilizado (LPS)') !!}
            {!! Form::text('CaudalUtilizadoLPS',null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form group">
            {!! Form::label('lblCaudalConcesionadoLPS','*Caudal Concesionado (LPS)') !!}
            {!! Form::text('CaudalConcesionadoLPS',null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form group">
            {!! Form::label('lblOficinaProvincial','*Oficina provincial') !!}
            {!! Form::select('OficinaProvincial',$dependencias,null,['class' => 'form-control']) !!}

        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-4">
        <div class="form group">
            {!! Form::label('lblCaudalUsoDomestico','*Caudal uso doméstico') !!}
            {!! Form::text('CaudalUsoDomestico',null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form group">
            {!! Form::label('lblCaudalUsoPecuario','*Caudal uso pecuario') !!}
            {!! Form::text('CaudalUsoPecuario',null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form group">
            {!! Form::label('lblCaudalUsoAgricola','*Caudal uso agrícola') !!}
            {!! Form::text('CaudalUsoAgricola',null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<br/>
<div class="form group">
    {!! Form::label('lblObservaciones','*Observaciones') !!}
    {!! Form::text('Observaciones',null, ['class' => 'form-control']) !!}
</div>
</br>
<div class="form group">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>
