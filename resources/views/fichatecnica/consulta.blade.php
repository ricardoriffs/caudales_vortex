@extends('layouts.dashboard')
@section('page_heading','Ficha Tecnica')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
          
        </div>
        <div class="col-sm-12">
            <br>
            <div class="panel panel-default">
                <div class="panel-heading">
                   Consultar ficha técnica
                </div>
                <div class="panel-body">
                    @include('widgets.info')
                    @include('widgets.error')

                    <form id="consulta-ficha-form">
                        @include('fichatecnica.partials.consulta-form')
                    </form>
                </div>
            </div>         
        </div>
    </div>
</div>  

<script type="text/javascript">
    
    $(document).ready(function() {
        $("form#consulta-ficha-form").on("submit", function(e) {
            
            $("#tabla-ficha-tecnica").html("");
            e.preventDefault();
            $.ajax({
                type:'POST',
                url:'generarfichatecnica',
                data: {
                    'NoCapatacion': $('input[name=NoCapatacion]').val(), 
                    '_token': $('input[name=_token]').val()
                },
                dataType: 'json',
                success:function(data){
                $("#tabla-ficha-tecnica").append(data);
                }
            });
            return false;
        });    
    });    
</script>
 @stop

