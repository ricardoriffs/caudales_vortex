@extends('layouts.dashboard')
@section('page_heading','Ficha Tecnica')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
          
        </div>
        <div class="col-sm-12">

            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                       Descargar ficha técnica
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')

                 {!!Form::open(['route' => 'fichatecnica.reportefichatecnica'])!!}
                      
                      
                        <div class="col-sm-2">                        
                          <div class="form group">
                        {!! Form::label('lblNoCaptacion','No de captación') !!}
                        {!! Form::text('NoCapatacion',null, ['class' => 'form-control']) !!}
                        </div>

                        </div>


                         <div class="col-sm-2">
                        <div class="form group">
                        <br>
                      
                         {!! Form::submit('Descargar', ['class' => 'btn btn-primary']) !!}
                        </div>
                        </div>
                     {!! Form::close()!!}

                    </div>




                </div>
              




<br>
<div class="panel panel-default">
                    <div class="panel-heading">
                       Caracteristicas de la captación
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                           <table class="table table-hover table-striped">
                                <thead>
                                    <tr><th>
                                            No. Captación
                                        </th>
                                        
                                        <th>
                                            CodigoFuente
                                        </th>
                                       
                                        <th>
                                            Coordx
                                        </th>
                                        <th>
                                            Coordy
                                        </th>
                                        <th>
                                            Altura
                                        </th>
                                        <th>
                                            Pendiente
                                        </th>
                                        <th>
                                            TipoEstructura
                                        </th>
                                        <th>
                                            Estado
                                        </th>
                                        <th>
                                            Dimension
                                        </th>
                                        <th>
                                            Capacidad LPS
                                        </th>
                                       

                                </thead>
                                <tbody>
                                    @foreach($captaciones as $captacion)
                                    <tr>
                                     <td>
                                        <b>{{$captacion->idcaptacion}}</b>
                                        </td>
                                        
                                        <td>
                                            {{$captacion->codigofuente}}
                                        </td>
                                        
                                        <td>
                                            {{$captacion->coordx}}
                                        </td>
                                        <td>
                                            {{$captacion->coordy}}
                                        </td>
                                        <td>
                                            {{$captacion->altura}}
                                        </td>
                                        <td>
                                            {{$captacion->pendiente}}
                                        </td>
                                        <td>
                                            {{$captacion->tipoestructura}}
                                        </td>
                                        <td>
                                            {{$captacion->estado}}
                                        </td>
                                        <td>
                                            {{$captacion->dimension}}
                                        </td>
                                        <td>
                                            {{$captacion->capacidadinstaladalps}}
                                        </td>
                                       

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('captaciones.edit', $captacion->idcaptacion)}}"> Ver Detalle </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $captaciones->render() !!}
                        </div>
                    </div>




                </div>



            </br>

             <div class="panel panel-default">
                    <div class="panel-heading">
                        encuestados
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                          No. Captacion
                                        </th>                                        <th>
                                            Cédula
                                        </th>                                         <th>
                                            Nombres
                                        </th>                                        <th>
                                            Apellidos
                                        </th>                                      <th>
                                            Telefono
                                        </th>                                        <th>
                                            Celular
                                        </th>                                        <th>
                                            Email
                                        </th>
                                </thead>
                                <tbody>
                                    @foreach($encuestados as $encuestado)
                                    <tr>
                                        <td>
                                            {{$encuestado->idcaptacion}}
                                        </td>
                                        <td>
                                            {{$encuestado->cedula}}
                                        </td>
                                       
                                        <td>
                                            {{$encuestado->nombres}}
                                        </td>
                                        <td>
                                            {{$encuestado->apellidos}}
                                        </td>
                                       
                                        <td>
                                            {{$encuestado->telefono}}
                                        </td>
                                        <td>
                                            {{$encuestado->celular}}
                                        </td>
                                        <td>
                                            {{$encuestado->email}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('encuestados.edit', $encuestado->cedula)}}"> Ver Detalle </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $encuestados->render() !!}
                        </div>
                    </div>
                </div>





 <div class="panel panel-default">
                    <div class="panel-heading">
                        Predios
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                          No.Captacion
                                        </th>                                        <th>
                                            Cedula Catastral  
                                        </th>                                        <th>
                                            Nombre Predio
                                        </th>                                        <th>
                                            Direccion Predio
                                        </th>                                        <th>
                                            Matricula
                                        </th>                                        <th>
                                            Altura
                                        </th>                                        <th>
                                            Areatotal
                                        </th>
                                </thead>
                                <tbody>
                                    @foreach($predios as $predio)
                                    <tr>
                                     <td>
                                            {{$predio->idcaptacion}}
                                        </td>
                                        <td>
                                            {{$predio->cedulacatastral}}
                                        </td>
                                       
                                       
                                       
                                        <td>
                                            {{$predio->nombrepredio}}
                                        </td>
                                        <td>
                                            {{$predio->direccionpredio}}
                                        </td>
                                        <td>
                                            {{$predio->matricula}}
                                        </td>
                                       
                                        <td>
                                            {{$predio->altura}}
                                        </td>
                                        <td>
                                            {{$predio->areatotal}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('predios.edit', $predio->cedulacatastral)}}"> Ver Detalle </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $predios->render() !!}
                        </div>
                    </div>
                </div>





<div class="panel panel-default">
                    <div class="panel-heading">
                        Expedientes Juridicos
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            No.Captacion
                                        </th>
                                        <th>
                                            Concesion
                                        </th>
                                        <th>
                                            No.Expediente 
                                        </th>
                                       
                                        <th>
                                            No.Resolucion
                                        </th>
                                        
                                        <th>
                                            Vigencia
                                        </th>
                                        
                                        
                                        <th>
                                            Fecha Obra
                                        </th>
                                        <th>
                                            Oficina Provincial
                                        </th>
                                    
                                       
                                        

                                </thead>
                                <tbody>
                                    @foreach($expedientesjuridicos as $expedientejuridico)
                                    <tr>
                                        <td>
                                            {{$expedientejuridico->idcaptacion}}
                                        </td>
                                        <td>
                                            {{$expedientejuridico->concesion}}
                                        </td>
                                        <td>
                                            {{$expedientejuridico->expedienteno}}
                                        </td>
                                        
                                        <td>
                                            {{$expedientejuridico->resolucionno}}
                                        </td>
                                        
                                        <td>
                                            Desde:{{$expedientejuridico->vigenciadesde}}   Hasta: {{$expedientejuridico->vigenciahasta}}
                                        </td>                                      
                                        
                                      
                                        <td>
                                            {{$expedientejuridico->fechaobra}}
                                        </td>
                                        <td>
                                            {{$expedientejuridico->oficinaprovincial}}
                                        </td>
                                      
                                      
                                      

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('expedientesjuridicos.edit', $expedientejuridico->idexpediente)}}"> Ver Detalle </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $expedientesjuridicos->render() !!}
                        </div>
                    </div>
                </div>




<div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de aforos
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            No.Captacion
                                        </th>
                                        <th>
                                            Diametro
                                        </th>
                                        <th>
                                            Velocidad
                                        </th>
                                        <th>
                                            Hora
                                        </th>
                                        <th>
                                            Profundidad
                                        </th>
                                        <th>
                                            Perimetro
                                        </th>
                                        <th>
                                            Caudal Total
                                        </th>
                                        <th>
                                            Area
                                        </th>
                                       
                                        
                                        <th>
                                            Fecha Aforo
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($aforos as $aforo)
                                    <tr>
                                        <td>
                                            {{$aforo->idcaptacion}}
                                        </td>
                                        <td>
                                            {{$aforo->diametro}}
                                        </td>
                                        <td>
                                            {{$aforo->velocidad}}
                                        </td>
                                        <td>
                                            {{$aforo->hora}}
                                        </td>
                                        <td>
                                            {{$aforo->profundidad}}
                                        </td>
                                        <td>
                                            {{$aforo->perimetro}}
                                        </td>
                                        <td>
                                            {{$aforo->caudaltotal}}
                                        </td>
                                        <td>
                                            {{$aforo->area}}
                                        </td>
                                       
                                       
                                        <td>
                                            {{$aforo->fechaaforo}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('aforos.edit', $aforo->idaforo)}}"> Ver Detalle </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $aforos->render() !!}
                        </div>
                    </div>
                </div>





        </div>
        </div>
    </div>  
 @stop

