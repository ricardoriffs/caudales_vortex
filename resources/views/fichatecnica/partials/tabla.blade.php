<div>
    <table class="table table-striped tabla-ficha">
        <thead>
            <tr class="row0">
                <th class="column0 style23 s style23" colspan="15"><br />
                    CORPORACIÓN AUTÓNOMA REGIONAL DE CUNDINAMARCA<br />
                    &nbsp;DIRECCIÓN DE GESTIÓN DEL ORDENAMIENTO AMBIENTAL Y TERRITORIAL <br />
                    FICHA PROYECTO DE DISTRIBUCIÓN DE CAUDALES
                </th>
            </tr>            
        </thead>
        <tbody>
            <tr class="row3">
                <td class="column0 style21 s style21" colspan="11">A.  INFORMACIÓN ENCUESTADOS</td>
                <td class="column11 style22 s style22" colspan="4">FOTOGRAFÍA CAPTACIÓN</td>
            </tr>
            <tr class="row4">
                <td class="column0 style1 s">Documento </td>
                <td class="column1 style1 s">Tipo Documento</td>
                <td class="column2 style1 s">Relación </td>
                <td class="column3 style1 s style1" colspan="4">Nombre</td>
                <td class="column7 style8 s style8" colspan="2">Direccion correspondencia</td>
                <td class="column9 style1 s">telefono</td>
                <td class="column10 style8 s">email</td>
                <td class="column11 style19 null style19" colspan="4" rowspan="3">
                    @if($capt_pre and $capt_pre->NombreImagen)
                        <img class="img image-border" src="{{ asset( 'storage/' . $capt_pre->NombreImagen) }}" alt="{{ $capt_pre->NombreImagen }}" height="200" width="240">
                    @endif
                </td>
            </tr>
            @foreach($encuestados as $encuestado)
                <tr class="row5">
                    <td class="column0 style4 null">{{ $encuestado->cedula }}</td>
                    <td class="column1 style9 null">{{ $encuestado->idtipoDoc }}</td>
                    <td class="column2 style9 null">{{ $encuestado->relacion }}</td>
                    <td class="column3 style9 null style9" colspan="4">{{ $encuestado->nombres.' '.$encuestado->apellidos }}</td>
                    <td class="column7 style9 null style9" colspan="2">{{ $encuestado->direccioncorrespondencia }}</td>
                    <td class="column9 style9 null">{{ $encuestado->telefono }}</td>
                    <td class="column10 style9 null">{{ $encuestado->email }}  </td>
                </tr>
            @endforeach
            <tr class="row9">
                <td class="column0 style17 s style17" colspan="11">B. INFORMACIÓN FISICA DEL PREDIO</td>
            </tr>
            <tr class="row10">
                <td class="column0 style7 s">Nombre del predio</td>
                <td class="column1 style25 null style25" colspan="6">
                    @if($capt_pre)
                        {{ $capt_pre->nombrepredio }}
                    @endif    
                </td>
                <td class="column7 style24 s style24" colspan="2">Cédula Catastral (17 Digitos)</td>
                <td class="column9 style26 null style26" colspan="2">
                    @if($capt_pre)
                        {{ $capt_pre->cedulacatastral }}
                    @endif    
                </td>
            </tr>
            <tr class="row11">
                <td class="column0 style7 s">Coordenadas predio</td>
                <td class="column1 style6 s">Norte (X,m)</td>
                <td class="column2 style27 null">
                    @if($capt_pre)
                        {{ $capt_pre->coordx }}
                    @endif
                </td>
                <td class="column3 style6 s">Este (Y,m)</td>
                <td class="column4 style27 null">
                    @if($capt_pre)
                        {{ $capt_pre->coordy }}
                    @endif
                </td>
                <td class="column5 style6 s">Altura (msnm)</td>
                <td class="column6 style28 null">
                    @if($capt_pre)
                        {{ $capt_pre->altura }}
                    @endif
                </td>
                <td class="column7 style16 s">Área (Ha)</td>
                <td class="column8 style27 null">
                    @if($capt_pre)
                        {{ $capt_pre->areatotal }}
                    @endif
                </td>
                <td class="column9 style6 s">Área en uso (HA)</td>
                <td class="column10 style29 null">
                    @if($capt_pre)
                        {{ $capt_pre->dimension }}
                    @endif
                </td>
            </tr>
            <tr class="row12">
                <td class="column0 style5 s">Departamento</td>
                <td class="column1 style30 null"></td>
                <td class="column2 style5 s">Municipio</td>
                <td class="column3 style31 null style31" colspan="2"></td>
                <td class="column5 style5 s">Vereda</td>
                <td class="column6 style32 null"></td>
                <td class="column7 style5 s">Dirección predio</td>
                <td class="column8 style31 null style31" colspan="3">
                    @if($capt_pre)
                        {{ $capt_pre->direccionpredio }}
                    @endif
                </td>
            </tr>
            <tr class="row13">
                <td class="column0 style18 s style18" colspan="15">C. CARACTERISITICAS DE LA CAPTACIÓN</td>
            </tr>
            <tr class="row14">
                <td class="column0 style11 s">
                    Coordenadas <br />
                    <span style="color:#000000; font-family:'Calibri'; font-size:8pt">punto de captación:</span></td>
                <td class="column1 style12 s">Norte (Y,m)</td>
                <td class="column2 style33 null"></td>
                <td class="column3 style12 s">Este (X,m)</td>
                <td class="column4 style33 null"></td>
                <td class="column5 style13 s">Altura (msnm)</td>
                <td class="column6 style33 null"></td>
                <td class="column7 style14 s">Tipo de estructura</td>
                <td class="column8 style33 null"></td>
                <td class="column9 style15 s">Dimensión (pulgs.)</td>
                <td class="column10 style33 null"></td>
                <td class="column11 style8 s style8" colspan="2">Capacidad (lt/seg)</td>
                <td class="column13 style34 null style34" colspan="2"></td>
            </tr>
            <tr class="row15">
                <td class="column0 style17 s style17" colspan="15">D. INFORMACIÓN DE USOS ACTUALES</td>
            </tr> 
        </tbody>
    </table>
</div>      