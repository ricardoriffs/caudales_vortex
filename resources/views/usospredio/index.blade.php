@extends('layouts.dashboard')
@section('page_heading','UsosPredios')
@section('section')
<div class="container">
    <div class="col-sm-8 col-sm-offset-2">
        <a class="btn btn-primary pull-right" href=" {{ route('usospredio.create') }}">
            <i class="fa fa-plus-circle" ></i> Nuevo
        </a>
    </div>
    @include('usospredio.partials.list')    
</div>  
 @stop

