@extends('layouts.dashboard')
@section('page_heading','Nuevo uso predio')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-12"> 
            <a class="btn btn-primary pull-left" href =" {{ route('usospredio.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver UsosPredios
            </a>
        </div>
        <div class="col-sm-12" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Nuevo usopredio
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                        {!!Form::open(['route' => 'usospredio.store'])!!}
                        {{ Form::hidden('TipoFormulario', 'individual') }} 
                         @include('partialsgeneral.nocaptacionsection') 
                          <hr>
                        @include('usospredio.partials.form')
                        {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

          $('select[name="IDModulo"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '../captaciones/consultarCategoriasDelTipoUso/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="IDTipoUsosCategoria"]').empty();
                        $.each(data, function(key, value) {

                            $('select[name="IDTipoUsosCategoria"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                    }
                });
            }else{
                $('select[name="IDTipoUsosCategoria"]').empty();
            }
        });


          
          $('select[name="IDTipoUsosCategoria"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '../captaciones/TipoUso/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="IDTipoUso"]').empty();
                        $.each(data, function(key, value) {

                            $('select[name="IDTipoUso"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                    }
                });
            }else{
                $('select[name="IDTipoUso"]').empty();
            }
        });

    });
</script>
@stop

