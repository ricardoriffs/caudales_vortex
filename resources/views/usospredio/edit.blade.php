@extends('layouts.dashboard')
@section('page_heading','Editar UsosPredios')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('usospredio.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver UsosPredios
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar usopredio
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($usopredio,['route' => ['usospredio.update',$usopredio->IDUsoPredio], 'method' =>'PUT']) !!}
                  	  @include('usospredio.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
