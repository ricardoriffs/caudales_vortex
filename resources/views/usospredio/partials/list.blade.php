<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <br>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de usospredio
                </div>
                <div class="panel-body">
               @include('widgets.info')
               @include('widgets.error')
                    <div class="col-sm-12">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Idcaptacion
                                    </th>
                                    <th>
                                        Idmodulo
                                    </th>
                                    <th>
                                        Idtipousoscategoria
                                    </th>
                                    <th>
                                        Idtipouso
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>
                                        Totalconsumo
                                    </th>

                            </thead>
                            <tbody>
                                @foreach($usospredio as $usopredio)
                                <tr>
                                    <td>
                                        {{$usopredio->Idcaptacion}}
                                    </td>
                                    <td>
                                        {{$usopredio->Idmodulo}}
                                    </td>
                                    <td>
                                        {{$usopredio->Idtipousoscategoria}}
                                    </td>
                                    <td>
                                        {{$usopredio->Idtipouso}}
                                    </td>
                                    <td>
                                        {{$usopredio->Cantidad}}
                                    </td>
                                    <td>
                                        {{$usopredio->Totalconsumo}}
                                    </td>

                                    <td>
                                   <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('usospredio.edit', $usopredio->IDUsoPredio)}}"> Editar </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!!  $usospredio->render() !!}
                    </div>
                </div>
            </div>
        </br>
    </div>
</div>
  