<br/>
<div class="row">
    <div class="col-md-3">
        <div class="form group">
            {!! Form::label('lblmodulo','Modulo de Consumo') !!}
            {!! Form::select('IDModulo',$modulos,null,['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form group">
            {!! Form::label('lblcategoria','Categoria del tipo de uso') !!}
            <select name="IDTipoUsosCategoria" class="form-control"></select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form group">
            {!! Form::label('lbltipouso','Tipo de Uso') !!}
            <select name="IDTipoUso" class="form-control"></select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form group">
            {!! Form::label('lblCantidad','Cantidad') !!}
            {!! Form::number('Cantidad',null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-1">
        <br>
        <div class="form group">
            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
</div>

@if(isset($usospredio))
@include('usospredio.partials.list')
@endif