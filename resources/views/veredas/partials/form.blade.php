<div class="form group">
{!! Form::label('lblIDVereda','Idvereda') !!}
{!! Form::text('IDVereda',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblIDMunicipio','Idmunicipio') !!}
{!! Form::text('IDMunicipio',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblNombre','Nombre') !!}
{!! Form::text('Nombre',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblDescripcion','Descripcion') !!}
{!! Form::text('Descripcion',null, ['class' => 'form-control']) !!}
</div>

</br>
<div class="form group">
 {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>
