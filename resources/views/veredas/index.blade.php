@extends('layouts.dashboard')
@section('page_heading','Veredas')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('veredas.create') }}">
                <i class="fa fa-plus-circle" ></i> Nueva
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de veredas
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Código vereda
                                        </th>                                        <th>
                                             Código municipio
                                        </th>                                        <th>
                                            Nombre
                                        </th>                                        <th>
                                            Descripción
                                        </th>
                                </thead>
                                <tbody>
                                    @foreach($veredas as $vereda)
                                    <tr>
                                        <td>
                                            {{$vereda->idvereda}}
                                        </td>
                                        <td>
                                            {{$vereda->idmunicipio}}
                                        </td>
                                        <td>
                                            {{$vereda->nombre}}
                                        </td>
                                        <td>
                                            {{$vereda->descripcion}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('veredas.edit', $vereda->idvereda)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $veredas->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

