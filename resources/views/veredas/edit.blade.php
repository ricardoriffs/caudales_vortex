@extends('layouts.dashboard')
@section('page_heading','Editar Veredas')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('veredas.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Veredas
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar vereda
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($vereda,['route' => ['veredas.update',$vereda->IDVereda], 'method' =>'PUT']) !!}
                  	  @include('veredas.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
