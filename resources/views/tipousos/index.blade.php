@extends('layouts.dashboard')
@section('page_heading','TipoUsos')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a class="btn btn-primary pull-right" href=" {{ route('tipousos.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-12">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de tipousos
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>                                      

                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            PisoTermico
                                        </th>
                                        <th class="text-danger">
                                            Litrosdiapersona
                                        </th>
                                        <th>
                                            Categoria y módulo
                                        </th>
                                        <th>
                                            Linea
                                        </th>
                                        <th>
                                            Unidad
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($tipousos as $tipouso)
                                    <tr>
                                        
                                        <td>
                                            {{$tipouso->nombretu}}
                                        </td>
                                        <td>
                                           {{$tipouso->pisotermico}} - {{$tipouso->descripcionpt}} 
                                        </td>
                                        <td>
                                            {{$tipouso->litrosdiapersona}}
                                        </td>
                                        <td>
                                            {{$tipouso->nombrecategoria}} -  {{$tipouso->descripcion}} 
                                        </td>
                                        <td>
                                            {{$tipouso->nombre}}
                                        </td>
                                        <td>
                                             {{$tipouso->unidad}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('tipousos.edit', $tipouso->idtipouso)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $tipousos->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

