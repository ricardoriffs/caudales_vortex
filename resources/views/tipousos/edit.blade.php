@extends('layouts.dashboard')
@section('page_heading','Editar TipoUsos')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('tipousos.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver TipoUsos
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar tipouso
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($tipouso,['route' => ['tipousos.update',$tipouso->IDTipoUso], 'method' =>'PUT']) !!}
                  	  @include('tipousos.partials.formedit')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>






<script type="text/javascript">


    $(document).ready(function() {
        
        $('select[name="IDTipoUsosCategorias"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '../Lineas/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="Linea"]').empty();
                        $.each(data, function(key, value) {

                            $('select[name="Linea"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                    }
                });
            }else{
                $('select[name="Linea"]').empty();
            }
        });





    
        
        $('select[name="IDTipoUsosCategorias"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '../Unidades/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="Unidad"]').empty();
                        $.each(data, function(key, value) {

                            $('select[name="Unidad"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                    }
                });
            }else{
                $('select[name="Unidad"]').empty();
            }
        });

    });
    </script>
@stop
