
<div class="form group">
{!! Form::label('lblNombre','Nombre') !!}
{!! Form::text('Nombre',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblIDPisoTermico','Altitud') !!}
{!! Form::select('IDPisoTermico',$pisostermicos,null,['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblLitrosDiaPersona','Litros dia persona') !!}
{!! Form::Number('LitrosDiaPersona',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblIDTipoUsoCategorias','Categoria Tipo Uso') !!}
{!! Form::select('IDTipoUsosCategorias',$tiposusoscategorias,null,['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblLinea','Linea') !!}
<select name="Linea" class="form-control">
	@foreach($lineas as $linea)
                <option value="{{$linea->IDMedidaConsumo}}"  {{ $linea->IDMedidaConsumo == $tipouso->Linea ? 'selected="selected"' : '' }}>{{$linea->Nombre}}</option>
    @endforeach
</select>
</div>
<div class="form group">
{!! Form::label('lblUnidad','Unidad') !!}
<select name="Unidad" class="form-control">
	@foreach($unidades as $unidad)
                <option value="{{$unidad->IDMedidaConsumo}}" {{ $unidad->IDMedidaConsumo == $tipouso->Unidad ? 'selected="selected"' : '' }}>{{$unidad->Nombre}}</option>
    @endforeach
</select>
</div>
</br>
<div class="form group">
 {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>
