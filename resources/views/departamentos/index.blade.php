@extends('layouts.dashboard')
@section('page_heading','Departamentos')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('departamentos.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de departamentos
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Código Departamento
                                        </th>                                        <th>
                                            Nombre
                                        </th>
                                </thead>
                                <tbody>
                                    @foreach($departamentos as $departamento)
                                    <tr>
                                        <td>
                                            {{$departamento->iddepartamento}}
                                        </td>
                                        <td>
                                            {{$departamento->nombre}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('departamentos.edit', $departamento->iddepartamento)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $departamentos->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

