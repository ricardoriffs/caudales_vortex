@extends('layouts.plane')

@section('body')
 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url ('home') }}">{{ config('app.name', 'Laravel') }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                  @if (Auth::guest())
                          <!-- Sin  autenticarse -->
                        @else
            
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> {{ Auth::user()->name }} 
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> Mi perfil</a>
                       
                        <li class="divider"></li>
                        <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out fa-fw"></i>
                           Cerrar sesión
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
                 @endif
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Buscar captación...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                    
 @if (Auth::user()->idperfil == "1" )

 <li {{ (Request::is('/') ? 'class="active"' : '') }}>
  <a href="{{ url ('home') }}"><i class="fa fa-dashboard fa-fw"></i> Administración<span class="fa arrow"></span></a>
                          
                            <ul class="nav nav-second-level">
                             
                                        <li>
                                        <li><a href="{{route('proposito.index')}}">Propósito</a>
                                        </li>
                                        </li> 

                                         <li>
                                            <a href="#">Usos del predio <span class="fa arrow"></span></a>
                                            <ul class="nav nav-third-level">
                                                        <li>
                                                        <a href="{{route('tipousoscategorias.index')}}">Categorias de Tipos de Usos </a></li>
                                                        <li>
                                                        <a href="{{route('modulos.index')}}">Modulos</a>
                                                        </li>
                                                        <li>
                                                        <a href="{{route('pisostermicos.index')}}">Pisos Termicos</a>
                                                        </li>
                                                        <li>
                                                        <a href="{{route('medidasconsumo.index')}}">Medidas de Consumo</a>
                                                        </li>
                                                        <li>
                                                        <a href="{{route('tipousos.index')}}">Tipo de usos</a>
                                                        </li>
                                            </ul>

                                        </li>
                                        <li>
                                        <a href="{{ route('tipofuentes.index')}}">Tipos de fuentes</a>
                                        </li>
                                        <li>
                                        <a href="{{ route('cuencashidrograficas.index')}}">Cuencas Hidrograficas</a>
                                        </li>  
                                        <li>
                                        <a href="{{ route('subcuencashidrograficas.index')}}">SubCuencas Hidrograficas</a>
                                        </li>  
                                        <li>
                                        <a href="{{ route('fuenteshidrograficas.index')}}">Fuentes Hidrograficas</a>
                                        </li>  
                                        <li>
                                        <a href="{{ route('estructuracaptaciones.index')}}">Tipo Captación</a>
                                        </li>                      
                                        <li>
                                        <a href="{{route('logs.index')}}">Logs</a>
                                        </li>
                                        <li>
                                        <a href="{{route('perfiles.index')}}">Roles</a>
                                        </li>
                                        <li>
                                        <a href="{{route('dependencias.index')}}">Dependencias</a>
                                        </li>
                                        <li>
                                        <a href="{{route('departamentos.index')}}">Departamentos</a>
                                        </li>
                                        <li>
                                        <a href="{{route('municipios.index')}}">Municipios</a>
                                        </li>
                                        <li>
                                        <a href="{{route('veredas.index')}}">Veredas</a>
                                        </li>
<!--                                        <li>
                                            <a href="{{route('fichatecnica.consultafichatecnica')}}">Ficha técnica</a>
                                        </li>                                      -->

                                       
                                <li>
                            </ul>
                           
                        </li>
  @endif

                      <li>
                            <a href="#"><i class="fa fa-edit fa-fw"></i> Captación<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                              <!-- <li { (Request::is('buscarcaptaciones') ? 'class="active"' : '') }}>
                                    <a href="{ route('buscarcaptaciones.index')}}">Buscar captación</a>




                                </li> -->


                                 <li><a href="{{ route('captaciones.create')}}"><i class="fa fa-plus fa-fw"></i>Crear</a></li>
                                 <li><a href="{{ route('captaciones.index')}}"><i class="fa fa-search fa-fw"></i>Consultar</a></li>

                              

  <!-- 
                                <li>
                                    <a href="#"><i class="fa fa-plus fa-fw"></i>Crear <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{ route('captaciones.create')}}"> Registrar Caracteristicas</a>
                                        </li>
                                         <li>
                                <a href="#"> Adicionar Elementos<span class="fa arrow"></span></a>
                                  <ul class="nav nav-third-level">


                                        
                                         <li><a href="{route('encuestados.create')}}"> <b>1)</b> Agregar Encuestados</a></li>
                                        
                                            <li><a href="{route('predios.create')}}"><b>2)</b> Agregar Predios</a></li>
                                       
                                        </li>





                                        <li><a href="{route('expedientesjuridicos.create')}}"> <b>3)</b> Agregar Expedientes Juridicos</a></li>
                                         <li><a href="{route('aforos.create')}}"><b>4)</b> Agregar Aforos</a></li>
                                          <li><a href="{route('acueductos.create')}}"><b>5)</b> Agregar Acueducto</a></li>
                                       
                                        <li><a href="{route('calidadesagua.create')}}"><b>6)</b> Agregar Calidad del Agua</a></li> 

                                         <li><a href="{route('cuencas.create')}}"><b>7)</b> Agregar Cuencas</a></li>
                                         
                                          <li><a href="{route('usospredio.create')}}"><b>8)</b>UsoDelPredio</a></li>
                                   
                                         
                                        
                                        <li>
                                            <a href="{route('encuestadores.create')}}"><b>9)</b> Agregar Encuestador</a>
                                        </li> 

                                  </ul>

                                 </li>

                                        
                                      

                                                                             
                                    </ul>
                             
                                </li>
                              
                               
                                <li>
                                    
                              
                               
                                
                              
                               <li>
                                    <a href="#"><i class="fa fa-search fa-fw"></i> Consultar<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                          <li>
                                            <a href="{ route('captaciones.index')}}"> Ver Caracteristicas</a>
                                        </li>
                                        <li><a href="{route('encuestados.index')}}">Encuestados</a></li>
                                        <li><a href="{route('predios.index')}}">Predios</a></li>
                                        
                                          <li><a href="{route('expedientesjuridicos.index')}}">ExpedienteJuridico</a></li>
                                        <li><a href="{route('aforos.index')}}">Aforo</a></li>

                                        <li><a href="{route('acueductos.index')}}">Acueductos</a></li>
                                       
                                         <li><a href="{route('calidadesagua.index')}}">Calidad del Agua</a></li>
                                         <li><a href="{route('cuencas.index')}}">Cuencas</a></li>
                                  
                                         <li><a href="{route('tipousocategorias.index')}}">Usos del predio</a></li> 
                                        
                                        <li>
                                            <a href="{route('encuestadores.index')}}">Encuestador</a>
                                        </li> 
                                    
                                    </ul>
                                  
                                </li>
                              
                                  
                                --> 
                            </ul>
                          
                       </li>

  </li>




                    <li {{ (Request::is('*charts') ? 'class="active"' : '') }}>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Reportes</a>
                             <ul class="nav nav-third-level">


                                        
                                         
                                          <li><a href="{{route('fichatecnica.index')}}"> Ficha técnica</a></li>

                        </li>


                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
			 <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('page_heading')</h1>
                </div>
                <!-- /.col-lg-12 -->
           </div>
			<div class="row">  
				@yield('section')

            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
@stop

