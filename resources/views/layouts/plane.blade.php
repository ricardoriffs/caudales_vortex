<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8"/>
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/logo_caudales.ico') }}" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="C.A.R." name="author"/>
        <meta name="_token" content="{{ csrf_token() }}"/>
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/styles.css") }}" />
	      {!! Charts::assets() !!}
</head>
<body>
	@yield('body')
	<script src="{{ asset("assets/scripts/frontend.js") }}" type="text/javascript"></script>
</body>
</html>