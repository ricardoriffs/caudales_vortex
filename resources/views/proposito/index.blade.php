@extends('layouts.dashboard')
@section('page_heading','Propósito del aplicativo caudales')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                        <h3>
                                        <p align="justify">
                                    
                                               El aplicativo de caudales es una herramienta que permite realizar la distribución del agua de acuerdo a la necesidad del predio teniendo en cuenta la capacidad de la microcuenca.  Esta relación de demanda vs Oferta se realiza mediante fórmulas que son cargadas al sistema  y que bajo esta parametrización le permiten a la Corporación establecer de forma veraz y optima cuanto se debe otorgar.

                                       
                                           </p>
                                           </h3>
                                        </th>

                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                       
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

