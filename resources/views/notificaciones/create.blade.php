@extends('layouts.dashboard')
@section('page_heading','Notificaciones')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Nueva notificación
                </div>
                <div class="panel-body">
                    @include('widgets.info')
                    @include('widgets.error')
                     {!!Form::open(['route' => 'notificacions.store'])!!}

                      
                    @include('notificaciones.partials.form')

                      {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
