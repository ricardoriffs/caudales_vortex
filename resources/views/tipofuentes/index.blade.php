@extends('layouts.dashboard')
@section('page_heading','Tipos de fuente')
@section('section')
<div class="container">
    <div class="row">
       
        <div class="col-sm-8 col-sm-offset-2">
            <br>


            <div class="panel panel-default">
                    <div class="panel-heading">
                       Ingresar tipo de fuente
                    </div>
                    <div class="panel-body">
                     {!!Form::open(['route' => 'tipofuentes.store'])!!}

                        <div class="form group">
                        {!! Form::label('Nombre','Nombre') !!}
                        {!! Form::text('Nombre',null, ['class' => 'form-control']) !!}
                        </div>

                        </br>
                        <div class="form group">
                        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                        </div>               

                      {!! Form::close()!!}
                    </div>
            </div>
                   
                   @include('widgets.info')
                    @include('widgets.error')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de tipos de fuente
                    </div>
                    <div class="panel-body">
                  
                        <div class="col-sm-12">

               

                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th with="20px">
                                            IDFuente
                                        </th>
                                        <th>
                                          Nombre
                                        </th>
                                       
                                    </tr>
                                </thead>
                                <tbody>                                    
                                    @foreach($tipofuentes as $tipofuente)
                                    <tr>
                                        <td>
                                            {{$tipofuente->idfuente}}
                                        </td>
                                        <td>
                                            {{$tipofuente->nombre}}
                                        </td> 
                                         </tr>
                                    @endforeach                                 
                                </tbody>
                            </table>
                         {!!  $tipofuentes->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>    
 @stop
