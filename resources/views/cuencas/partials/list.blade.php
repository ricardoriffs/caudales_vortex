<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <br>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de cuencas
                </div>
                <div class="panel-body">
               @include('widgets.info')
               @include('widgets.error')
                    <div class="col-sm-12">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        No.captacion
                                    </th>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Corriente
                                    </th>
                                    <th>
                                        Codigo Área Hidrográfica
                                    </th>
                                    <th>
                                        Nombre Área Hidrográfica
                                    </th>

                            </thead>
                            <tbody>
                                @foreach($cuencas as $cuenca)
                                <tr>
                                    <td>
                                        {{$cuenca->IDCaptacion}}
                                    </td>
                                    <td>
                                        {{$cuenca->Nombre}}
                                    </td>
                                    <td>
                                        {{$cuenca->Corriente}}
                                    </td>
                                    <td>
                                        {{$cuenca->CodigoAreaHidrografica}}
                                    </td>
                                    <td>
                                        {{$cuenca->NombreAreaHidrografica}}
                                    </td>

                                    <td>
                                   <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('cuencas.edit', $cuenca->IDCuenca)}}"> Editar </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!!  $cuencas->render() !!}
                    </div>
                </div>
            </div>
        </br>
    </div>
</div>