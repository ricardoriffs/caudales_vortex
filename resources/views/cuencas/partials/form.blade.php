<br>
<div class="row">
    <div class="col-md-12">
		<div class="row">
		    <div class="col-md-3">
		        <div class="form group">
		            {!! Form::label('modulo','*Cuenca 2do ordenasdasdas') !!}
		            {!!  Form::select('IDCuenca',$cuencashidrograficas,null,['class' => 'form-control', 'onchange' => 'traerNombreCodigoCuenta()', 'id' => 'Pes_CuencaIDCuenca']) !!}
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="form group">
		            {!! Form::label('modulo','*Cuenca 3do orden') !!}
		            <select name="IDSubCuenca" class="form-control" id="Pes_CuencaIDSubCuenca" onchange="traerNombreCodigoCuenta()"></select>
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="form group">
		            {!! Form::label('modulo','*Cuenca 4do orden') !!}
		            <select name="CodigoFuente" class="form-control" id="Pes_CuencaCodigoFuente" onchange="traerNombreCodigoCuenta()"></select>
		        </div>
		    </div>
		</div>
	</div>
	<div class="col-md-12">
		<br/>
	    <div class="form group">
			{!! Form::label('IDCuenca','Código de la Cuenca') !!}
			{!! Form::text('IDCuenca',null, ['class' => 'form-control','style' => 'display:none']) !!}
			<p id="IDCuenca_P"></p>
		</div>
	</div>
	<br>
	<div class="col-md-12">
		<br/>
	    <div class="form group">
			{!! Form::label('lblNombre','Nombre  de la cuenca:') !!}
			{!! Form::text('Nombre',null, ['class' => 'form-control','style' => 'display:none']) !!}
			<p id="NombreCuenca_P"></p>
		</div>
	</div>
	<br>
	<div class="col-md-12">
		<br/>
	    <div class="form group">
			{!! Form::label('lblCorriente','Nombre de la corriente') !!}
			{!! Form::text('Corriente',null, ['class' => 'form-control']) !!}
		</div>
	</div>
	<br>
	<div class="col-md-12">
		<br/>
	    <div class="form group">
			{!! Form::label('lblCodigoAreaHidrografica','Código del área Hidrográfica') !!}
			{!! Form::text('CodigoAreaHidrografica',null, ['class' => 'form-control']) !!}
		</div>
	</div>
	<br>
	<div class="col-md-12">
		<br/>
	    <div class="form group">
			{!! Form::label('lblNombreAreaHidrografica','Nombre del área Hidrográfica') !!}
			{!! Form::text('NombreAreaHidrografica',null, ['class' => 'form-control']) !!}
		</div>
	</div>
	<br>
	<div class="col-md-12">
		<br/>
	    <div class="form group">
			{!! Form::label('modulo','*Tipo de Fuente') !!}
			{!! Form::select('TipoFuente',$tipofuentes,null,['class' => 'form-control']) !!}
		</div>
	</div>
	<br>
	<div class="col-md-12">
		<br/>
		<div class="form group">
		 	{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
		</div>
	</div>
</div>

@if(isset($cuencas))
    @include('cuencas.partials.list')
@endif
    