@extends('layouts.dashboard')
@section('page_heading','Editar Cuencas')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('cuencas.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Cuencas
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar cuenca
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($cuenca,['route' => ['cuencas.update',$cuenca->IDCuenca], 'method' =>'PUT']) !!}
                  	  @include('cuencas.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
