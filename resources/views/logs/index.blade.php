@extends('layouts.dashboard')
@section('page_heading','Logs')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
           
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de logs
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        
                                       
                                   
                                         <th>
                                            Detalle 
                                        </th>
                                        <th>
                                            Accion
                                        </th>
                                        <th>
                                            Fecha
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($logs as $log)
                                    <tr>
                                    
                                       
                                        
                                         <td>
                                         {{$log->detalle}}
                                        </td>
                                       
                                        <td>
                                            {{$log->accion}}
                                        </td>
                                        
                                        <td>
                                            {{$log->fecha}}
                                        </td>

                                       
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $logs->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

