@extends('layouts.dashboard')
@section('page_heading','Parámetros')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('parametros.create') }}">
                <i class="fa fa-plus-circle"></i> Nuevo
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de parámetros
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                    @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th with="20px">
                                            Valor
                                        </th>
                                        <th>
                                          Nombre
                                        </th>
                                        <th >
                                        Detalle
                                        </th>
                                    </tr>
                                </thead>
                                <tbody> 

                                    @foreach($parametros as $parametro)
                                    <tr>
                                        <td>
                                            {{$parametro->Parametro}}
                                        </td>
                                        <td>
                                            {{$parametro->Modulo}}
                                        </td>
                                        <td>
                                            {{$parametro->Observaciones}}
                                        </td>
                                        <td>
                                        <a class="btn btn-primary btn-outline btn-sm pull-right" 
                                        href=" {{ route('parametros.edit', $parametro->IDParametro)}}"> Editar  </a>
                                        </td>
                                       
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $parametros->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>    
 @stop
