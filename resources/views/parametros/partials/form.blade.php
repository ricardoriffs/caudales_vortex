<div class="form group">
{!! Form::label('parametro','Valor') !!}
{!! Form::number('Parametro',null, ['class' => 'form-control','step' => 'any']) !!}
</div>
<div class="form group">
{!! Form::label('modulo','Nombre') !!}
{!! Form::text('Modulo',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('observaciones','Detalle') !!}
{!! Form::text('Observaciones',null, ['class' => 'form-control']) !!}
</div>
</br>
<div class="form group">
{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>