@extends('layouts.dashboard')
@section('page_heading','Editar Parámetro')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
              <a class="btn btn-primary pull-left" href=" {{ route('parametros.index') }}">
                <i class="fa fa-backward">
                </i>
                Ver Parámetros
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Modificar parámetro
                    </div>
                    <div class="panel-body">
                        @include('widgets.info')
                    @include('widgets.error')
                     {!!Form::model($parametro,['route' => ['parametros.update',$parametro->IDParametro], 'method' =>'PUT']) !!}

                      
                  		  @include('parametros.partials.form')

                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
