@extends('layouts.dashboard')
@section('page_heading','Editar Dependencias')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('dependencias.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Dependencias
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar dependencia
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($dependencia,['route' => ['dependencias.update',$dependencia->IdDependencia], 'method' =>'PUT']) !!}
                  	  @include('dependencias.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
