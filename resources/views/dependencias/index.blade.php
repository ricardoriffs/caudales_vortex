@extends('layouts.dashboard')
@section('page_heading','Dependencias')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('dependencias.create') }}">
                <i class="fa fa-plus-circle" ></i> Nueva
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de dependencias
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Dependencia
                                        </th>
                                        <th>
                                            Acronimo
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($dependencias as $dependencia)
                                    <tr>
                                        <td>
                                            {{$dependencia->dependencia}}
                                        </td>
                                        <td>
                                            {{$dependencia->acronimo}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('dependencias.edit', $dependencia->iddependencia)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $dependencias->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

