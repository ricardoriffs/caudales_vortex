@extends('layouts.dashboard')
@section('page_heading','EstructuraCaptaciones')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('estructuracaptaciones.create') }}">
                <i class="fa fa-plus-circle" ></i> Nueva
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de estructuracaptaciones
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Nombre
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($estructuracaptaciones as $estructuracaptacion)
                                    <tr>
                                        <td>
                                            {{$estructuracaptacion->nombre}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('estructuracaptaciones.edit', $estructuracaptacion->idestructura)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $estructuracaptaciones->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop



