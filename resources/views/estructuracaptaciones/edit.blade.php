@extends('layouts.dashboard')
@section('page_heading','Editar EstructuraCaptaciones')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('estructuracaptaciones.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver EstructuraCaptaciones
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar estructuracaptacion
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($estructuracaptacion,['route' => ['estructuracaptaciones.update',$estructuracaptacion->IDEstructura], 'method' =>'PUT']) !!}
                  	  @include('estructuracaptaciones.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
op
