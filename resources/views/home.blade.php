@extends('layouts.dashboard')
@section('page_heading', 'Inicio')
@section('section')
           
         @if (Auth::user()->IdPerfil == "1" )
            <!-- /.row -->
            <div class="col-sm-12">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">                                 
                                    <div>Administración de perfiles</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('users.create')}}">
                            <div class="panel-footer">
                                <span class="pull-left">Ir a usuarios</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa fa-bell fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    
                                    <div>Notificaciones</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('notificacions.create')}}">
                            <div class="panel-footer">
                                <span class="pull-left">Ir a notificaciones</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-wrench fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"></div>
                                    <div>Parámetros Iniciales</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('parametros.index')}}">
                            <div class="panel-footer">
                                <span class="pull-left">Ver parametros iniciales</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-table fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-left">
                                
                                    <div>Tipos de usos del suelo</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('tipousos.index')}}">
                            <div class="panel-footer">
                                <span class="pull-left">Ver tipos de usos</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->

            @endif
            <div class="row">
               
                <!-- /.col-lg-4 -->
              


                  <div class="col-lg-6">
                    @section ('pane1_panel_title', 'Notificaciones recientes')
                    @section ('pane1_panel_body')
                     
                        
                            <div class="list-group">

                            @foreach($notificacions as $notificacion)
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-comment fa-fw"></i> {{$notificacion->notificacion}}
                                    <span class="pull-right text-muted small"><em>{{$notificacion->created_at}}</em>
                                    </span>
                                </a>
                              @endforeach
                              
                             
                            </div>

                            {!! $notificacions->render() !!}
                            <!-- /.list-group -->
                           <!--     <a href="#" class="btn btn-default btn-block">Ver todas las notificaciones</a> -->
                        
                        <!-- /.panel-body -->
                  
                    @endsection
                    @include('widgets.panel', array('header'=>true, 'as'=>'pane1'))
                      
                        </div>


                 <div class="col-lg-6">

            <div class="panel panel-default">
                <div class="panel-heading">Ranking</div>
                <div class="panel-body">

                {!! $chart->render() !!}
            </div>
            </div>
            </div>
                    
                   
                </div>
                <!-- /.col-lg-4 -->
            
@stop
