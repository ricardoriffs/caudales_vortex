
<div class="row">       
    <div class="col-md-8">
        <fieldset>        
            <legend>Tipo de Aforo</legend>  
            <br/>        
            <div class="acueductos-field-t">
                <div class="form group">
                    {!! Form::label('lblDiametro','*Diámetro') !!}
                    {!! Form::number('Diametro',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-t">               
                <div class="form group">
                    {!! Form::label('lblVelocidad','*Velocidad') !!}
                    {!! Form::number('Velocidad',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-t">               

                <div class="form group">
                    {!! Form::label('lblHora','*Hora Vadeo') !!}
                    {!! Form::time('Hora',null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </fieldset>
    </div>          
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <fieldset>        
            <legend>Características Geométricas</legend>   
            <br/>
            <div class="acueductos-field-q">
                <div class="form group">
                    {!! Form::label('lblProfundidad','*Profundidad media (m)') !!}
                    {!! Form::number('Profundidad',null, ['class' => 'form-control']) !!}
                </div>

            </div>
            <div class="acueductos-field-q">
                <div class="form group">
                    {!! Form::label('lblPerimetro','*Perímetro mojado') !!}
                    {!! Form::number('Perimetro',null, ['class' => 'form-control']) !!}
                </div>
            </div>      
            <div class="acueductos-field-q">               
                <div class="form group">
                    {!! Form::label('lblArea','*Área') !!}
                    {!! Form::number('Area',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-q">
                <div class="form group">
                    {!! Form::label('lblRadioHidraulico','*Radio hidráulico') !!}
                    {!! Form::number('RadioHidraulico',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-q">
                <div class="form group">
                    {!! Form::label('lblVelocidadMedia','*Velocidad media') !!}
                    {!! Form::number('VelocidadMedia',null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </fieldset>
    </div>
</div>  
<br/>
<div class="row">
    <div class="col-md-12">
        <div class="acueductos-field-q">               
            <div class="form group">
                {!! Form::label('lblCaudalTotal','*Caudal total') !!}
                {!! Form::number('CaudalTotal',null, ['class' => 'form-control']) !!}
            </div>
        </div>        
        <div class="acueductos-field-q">               
            <div class="form group">
                {!! Form::label('lblFechaAforo','*Fecha aforo') !!}
                {!! Form::date('FechaAforo',null, ['class' => 'form-control']) !!}

            </div>
        </div>
    </div>
</div>
<br/>
<div class="form group">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>

@if(isset($aforos))
@include('aforos.partials.list')
@endif