<div class="row">
    <div class="col-sm-12">
        <br>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de Aforos
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Diámetro</th>
                                    <th>Velocidad</th>
                                    <th>Hora</th>
                                    <th>Profundidad</th>
                                    <th>Perímetro</th>
                                    <th>Caudal Total</th>    
                                    <th>Área</th>
                                    <th>Fecha Aforo</th>                                          
                                </tr>    
                            </thead>
                            <tbody>
                                @foreach($aforos as $aforo)
                                <tr>
                                    <td>{{ $aforo->Diametro }}</td>
                                    <td>{{ $aforo->Velocidad }}</td>
                                    <td>{{ $aforo->Hora }}</td>
                                    <td>{{ $aforo->Profundidad }}</td>
                                    <td>{{ $aforo->Perimetro }}</td>
                                    <td>{{ $aforo->CaudalTotal }}</td>
                                    <td>{{ $aforo->Area }}</td>
                                    <td>{{ $aforo->FechaAforo }}</td>                                   
                                    <td>
                                        <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('aforos.edit', $aforo->IDAforo)}}"> Editar</a>
                                    </td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                        @if($aforos)                     
                            {!! $aforos->render() !!}
                        @endif    
                    </div>
                </div>
            </div>
        </br>
    </div>
</div>