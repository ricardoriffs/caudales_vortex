@extends('layouts.dashboard')
@section('page_heading','Crear aforos')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('aforos.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Aforos
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Nuevo aforo
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                        {!!Form::open(['route' => 'aforos.store'])!!}
                          {{ Form::hidden('TipoFormulario', 'individual') }} 
                          @include('partialsgeneral.nocaptacionsection')                          
                        @include('aforos.partials.form')
                        {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop

