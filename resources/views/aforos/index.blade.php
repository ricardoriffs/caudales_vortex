@extends('layouts.dashboard')
@section('page_heading','Aforos')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <a class="btn btn-primary pull-right" href=" {{ route('aforos.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de aforos
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            No.Captacion
                                        </th>
                                        <th>
                                            Diametro
                                        </th>
                                        <th>
                                            Velocidad
                                        </th>
                                        <th>
                                            Hora
                                        </th>
                                        <th>
                                            Profundidad
                                        </th>
                                        <th>
                                            Perimetro
                                        </th>
                                        <th>
                                            Caudal Total
                                        </th>
                                        <th>
                                            Area
                                        </th>
                                       
                                        
                                        <th>
                                            Fecha Aforo
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($aforos as $aforo)
                                    <tr>
                                        <td>
                                            {{$aforo->IDCaptacion}}
                                        </td>
                                        <td>
                                            {{$aforo->Diametro}}
                                        </td>
                                        <td>
                                            {{$aforo->Velocidad}}
                                        </td>
                                        <td>
                                            {{$aforo->Hora}}
                                        </td>
                                        <td>
                                            {{$aforo->Profundidad}}
                                        </td>
                                        <td>
                                            {{$aforo->Perimetro}}
                                        </td>
                                        <td>
                                            {{$aforo->CaudalTotal}}
                                        </td>
                                        <td>
                                            {{$aforo->Area}}
                                        </td>
                                       
                                       
                                        <td>
                                            {{$aforo->FechaAforo}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('aforos.edit', $aforo->IDAforo)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $aforos->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

