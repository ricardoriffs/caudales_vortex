@extends('layouts.dashboard')
@section('page_heading','Municipios')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('municipios.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de municipios
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Código municipio
                                        </th>                                        <th>
                                           Código departamento
                                        </th>                                        <th>
                                            Nombre
                                        </th>
                                </thead>
                                <tbody>
                                    @foreach($municipios as $municipio)
                                    <tr>
                                        <td>
                                            {{$municipio->idmunicipio}}
                                        </td>
                                        <td>
                                            {{$municipio->iddepartamento}}
                                        </td>
                                        <td>
                                            {{$municipio->nombre}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('municipios.edit', $municipio->idmunicipio)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $municipios->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

