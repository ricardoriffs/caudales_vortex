@extends('layouts.dashboard')
@section('page_heading','Editar Municipios')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('municipios.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Municipios
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar municipio
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($municipio,['route' => ['municipios.update',$municipio->IDMunicipio], 'method' =>'PUT']) !!}
                  	  @include('municipios.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
