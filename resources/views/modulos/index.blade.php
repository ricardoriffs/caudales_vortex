@extends('layouts.dashboard')
@section('page_heading','Modulos')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('modulos.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de modulos
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Descripcion
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($modulos as $modulo)
                                    <tr>
                                        <td>
                                            {{$modulo->descripcion}}
                                        </td>

                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $modulos->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

