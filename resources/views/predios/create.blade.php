@extends('layouts.dashboard')
@section('page_heading','Agregar predios')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('predios.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Predios
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Nuevo predio
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                        {!!Form::open(['route' => 'predios.store', 'files' => true])!!}
                        @include('partialsgeneral.nocaptacionsection')
                        {{ Form::hidden('TipoFormulario', 'individual') }}   
                        @include('predios.partials.form')
                        {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>





<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="IDDepartamento"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '../predios/Municipios/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="IDMunicipio"]').empty();
                        $.each(data, function(key, value) {

                            $('select[name="IDMunicipio"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                    }
                });
            }else{
                $('select[name="IDMunicipio"]').empty();
            }
        });

         $('select[name="IDMunicipio"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '../predios/Veredas/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="IDVereda"]').empty();
                        $.each(data, function(key, value) {

                            $('select[name="IDVereda"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                    }
                });
            }else{
                $('select[name="IDVereda"]').empty();
            }
        });
    });
</script>
@stop

