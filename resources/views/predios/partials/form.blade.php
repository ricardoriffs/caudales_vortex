<br/>
<div class="row">
    <div class="col-md-6">
        <div class="form group">
            {!! Form::label('lblNombrePredio','*Nombre predio') !!}
            {!! Form::text('NombrePredio',null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form group">
            {!! Form::label('lblCedulaCatastral','*Cédula Catastral') !!}
            {!! Form::text('CedulaCatastral',null, ['class' => 'form-control']) !!} 
        </div>
    </div>
    <div class="col-md-3">
        <div class="form group">
            {!! Form::label('lblMatricula','*Matrícula Inmobiliaria') !!}
            {!! Form::text('Matricula',null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-4">       
        <div class="form group">
            {!! Form::label('lblIDDepartamento','*Departamento') !!}
            {!! Form::select('IDDepartamento',$departamentos,null,['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form group">
            {!! Form::label('lblIDMunicipio','*Municipio') !!}
            <select name="IDMunicipio" class="form-control"></select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form group">
            {!! Form::label('lblIDVereda','*Vereda') !!}
            <select name="IDVereda" class="form-control"></select>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <div class="form group">
            {!! Form::label('lblDireccionPredio','*Dirección predio') !!}
            {!! Form::text('DireccionPredio',null, ['class' => 'form-control']) !!}
        </div>
    </div>           
</div>
<div class="row">
    <div class="col-md-6">
        <fieldset>        
            <legend>Cordenadas Predio</legend>
            <br>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('lblCoordX','*Norte (X m):') !!}
                    {!! Form::number('CoordX',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('lblCoordY','*Este (Y m):') !!}
                    {!! Form::number('CoordY',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('lblAltura','*Altura (msnm)') !!}
                    {!! Form::number('Altura',null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </fieldset>
    </div>
    <div class="col-md-6">
    <br><br>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblAreaTotal','*Área total predio (Hectáreas)') !!}
                {!! Form::number('AreaTotal',null, ['class' => 'form-control','step' => 'any']) !!}
            </div>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <div class="form group">
            {!! Form::label('lblImagenPredio','*Ruta fotos',['class' => 'control-label']) !!}
            {!! Form::file('ImagenPredio', ['class' => 'form-control']) !!}     
        </div>
    </div>           
</div>
@if(isset($nombreimagen))
<br/>
<div class="row">
    <div class="col-md-12">
        <img class="img image-border" src="{{ asset( 'storage/' . $nombreimagen) }}" alt="{{ $nombreimagen }}" height="200" width="240">
    </div>           
</div> 
@endif
<br/>

{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}

