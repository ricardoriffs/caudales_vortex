@extends('layouts.dashboard')
@section('page_heading','Predios')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <a class="btn btn-primary pull-right" href=" {{ route('predios.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de predios
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                          No.Captacion
                                        </th>                                        <th>
                                            Cedula Catastral  
                                        </th>                                        <th>
                                            Nombre Predio
                                        </th>                                        <th>
                                            Direccion Predio
                                        </th>                                        <th>
                                            Matricula
                                        </th>                                        <th>
                                            Altura
                                        </th>                                        <th>
                                            Areatotal
                                        </th>
                                </thead>
                                <tbody>
                                    @foreach($predios as $predio)
                                    <tr>
                                     <td>
                                            {{$predio->IDCaptacion}}
                                        </td>
                                        <td>
                                            {{$predio->CedulaCatastral}}
                                        </td>
                                       
                                       
                                       
                                        <td>
                                            {{$predio->NombrePredio}}
                                        </td>
                                        <td>
                                            {{$predio->DireccionPredio}}
                                        </td>
                                        <td>
                                            {{$predio->Matricula}}
                                        </td>
                                       
                                        <td>
                                            {{$predio->Altura}}
                                        </td>
                                        <td>
                                            {{$predio->AreaTotal}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('predios.edit', $predio->CedulaCatastral)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $predios->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

