@extends('layouts.dashboard')
@section('page_heading','Editar Predios')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('predios.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Predios
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar predio
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($predio,['route' => ['predios.update',$predio->CedulaCatastral], 'method' =>'PUT', 'files' => true]) !!}
                  	  @include('predios.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
