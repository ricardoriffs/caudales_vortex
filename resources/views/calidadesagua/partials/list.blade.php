<div class="row">
    <div class="col-sm-12">
        <br>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de calidades agua
                </div>
                <div class="panel-body">
               @include('widgets.info')
               @include('widgets.error')
                    <div class="col-sm-12">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        No.captacion
                                    </th>                                        <th>
                                        Apariencia
                                    </th>                                        <th>
                                        Olor
                                    </th>                                        <th>
                                        Color
                                    </th>
                            </thead>
                            <tbody>
                                @foreach($calidadesagua as $calidadagua)
                                <tr>
                                    <td>
                                        {{$calidadagua->IDCaptacion}}
                                    </td>
                                    <td>
                                        {{$calidadagua->Apariencia}}
                                    </td>
                                    <td>
                                        {{$calidadagua->Olor}}
                                    </td>
                                    <td>
                                        {{$calidadagua->Color}}
                                    </td>

                                    <td>
                                   <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('calidadesagua.edit', $calidadagua->IDCaptacion)}}"> Editar </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!!  $calidadesagua->render() !!}
                    </div>
                </div>
            </div>
        </br>
    </div>
</div>