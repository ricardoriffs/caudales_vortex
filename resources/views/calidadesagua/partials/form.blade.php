<br/>
<div class="col-md-3">
<div class="form group">
{!! Form::label('lblApariencia','Apariencia') !!}
<select class="form-control" name="Apariencia">
	<option value="">-- Seleccion --</option>
	<option value="Clara">Clara</option>
	<option value="Sucia">Sucia</option>
</select>
</div>
</div>

<div class="col-md-3">
<div class="form group">
{!! Form::label('lblOlor','Olor') !!}
<select class="form-control" name="Olor">
	<option value="">-- Seleccion --</option>
	<option value="Inoloro">Inoloro</option>
	<option value="Con olor">Con olor</option>
</select>
</div>
</div>

<div class="col-md-3">
<div class="form group">
{!! Form::label('lblColor','Color') !!}
{!! Form::text('Color',null, ['class' => 'form-control']) !!}
</div>
</div>

</br>
<div class="col-md-3">
<div class="form group">
 {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>
</div>

@if(isset($calidadesagua))
    @include('calidadesagua.partials.list')
@endif