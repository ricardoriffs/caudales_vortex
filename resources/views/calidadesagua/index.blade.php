@extends('layouts.dashboard')
@section('page_heading','Calidad del Agua')
@section('section')
<div class="container">
    <div class="col-sm-8 col-sm-offset-2">
        <a class="btn btn-primary pull-right" href=" {{ route('calidadesagua.create') }}">
            <i class="fa fa-plus-circle" ></i> Nueva
        </a>
    </div>    
    @include('calidadesagua.partials.list')
</div> 
 @stop

