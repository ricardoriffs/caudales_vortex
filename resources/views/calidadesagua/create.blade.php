@extends('layouts.dashboard')
@section('page_heading','Registrar la calidad agua')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('calidadesagua.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Calidades Agua
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Calidad del agua
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')                        
                        {!!Form::open(['route' => 'calidadesagua.store'])!!}
                        {{ Form::hidden('TipoFormulario', 'individual') }} 
                        @include('partialsgeneral.nocaptacionsection') 
                        <hr>
                        @include('calidadesagua.partials.form')
                        {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop

