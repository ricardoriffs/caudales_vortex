@extends('layouts.dashboard')
@section('page_heading','Editar la calidad agua')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('calidadesagua.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver CalidadesAgua
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar calidad agua
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($calidadagua,['route' => ['calidadesagua.update',$calidadagua->IDCaptacion], 'method' =>'PUT']) !!}
                  	  @include('calidadesagua.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
