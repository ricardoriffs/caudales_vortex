@extends('layouts.dashboard')
@section('page_heading','Perfiles')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('perfiles.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de perfiles
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Perfil
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($perfiles as $perfil)
                                    <tr>
                                        <td>
                                            {{$perfil->perfil}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('perfiles.edit', $perfil->idperfil)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $perfiles->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

