@extends('layouts.dashboard')
@section('page_heading','Editar Perfiles')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('perfiles.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Perfiles
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar perfil
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($perfil,['route' => ['perfiles.update',$perfil->IdPerfil], 'method' =>'PUT']) !!}
                  	  @include('perfiles.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
