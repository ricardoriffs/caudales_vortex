@extends('layouts.dashboard')
@section('page_heading','Editar Acueductos V. y M.')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('acueductosvym.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver AcueductosVyM
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar acueductovym
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($acueductovym,['route' => ['acueductosvym.update',$acueductovym->IDAcueductosVyM], 'method' =>'PUT']) !!}
                  	  @include('acueductosvym.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
