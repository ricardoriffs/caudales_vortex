

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2">
          
      <div class="form group">
{!! Form::label('lblNoCaptacion','No.Captacion') !!}
{!! Form::number('IDCaptacion',null, ['class' => 'form-control','step' => 'any']) !!}
</div>
            </div>
            </div>
            </div>
            </div>
            <hr>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2">
               <div class="form group">
<small>{!! Form::label('lblAduccionDiametroIn','Aducción') !!}</small>
<br><small>{!! Form::label('lblAduccionLongitud2','(DIAMETRO(in)/ MATERIAL)') !!}</small>
{!! Form::text('A_DiametroIN',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-2">
              <div class="form group">
<small>{!! Form::label('lblAduccionLongitud','Aducción') !!}</small>
<br><small>{!! Form::label('lblAduccionLongitud2','( LONGITUD (m) )') !!}</small>
{!! Form::text('A_LongitudM',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-2">
              <div class="form group">
<small>{!! Form::label('lblAduccionEstado','Aducción') !!}</small>
<br><small>{!! Form::label('lblAduccionEstado2','(ESTADO (B/M/R))') !!}</small>
{!! Form::text('A_Estado',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-2">
              <div class="form group">
<small>{!! Form::label('lblDesaneradorLPS','Desanerador') !!}</small>
<br><small>{!! Form::label('lblDesaneradorLPS2','(CAPACIDAD (lps))') !!}</small>
{!! Form::text('D_CapacidadLPS',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-2">
               <div class="form group">
<small>{!! Form::label('lblDesaneradorEstado','Desanerador') !!}</small><br>
<small>{!! Form::label('lblDesaneradorEstado2','(ESTADO (B/M/R))') !!}</small>
{!! Form::text('D_Estado',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-2">
               <div class="form group">
<small>{!! Form::label('lblConduccionDiametroIn','Conducción') !!}</small>
<br><small>{!! Form::label('lblConduccionDiametroIn2','(DIAMETRO(IN)/ MATERIAL)') !!}</small>
{!! Form::text('C_DiametroIN',null, ['class' => 'form-control']) !!}
</div>
            </div>
        </div>


<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
               <div class="form group">
<small>{!! Form::label('lblConduccionLongitud','Conducción') !!}</small>
<br><small>{!! Form::label('lblConduccionLongitud2','(LONGITUD (m))') !!}</small>
{!! Form::text('C_Longitdu',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-3">
              <div class="form group">
<small>{!! Form::label('lblConduccionLongitud','Conducción') !!}</small><br>
<small>{!! Form::label('lblConduccionLongitud2','(ESTADO (B/M/R))') !!}</small>
{!! Form::text('C_Estado',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-3">
              <div class="form group">
<small>{!! Form::label('lblPotabilizacionLPS','Potabilización') !!}</small>
<br><small>{!! Form::label('lblPotabilizacionLPS2','(CAPACIDAD (LPS))') !!}</small>
{!! Form::text('P_CapacidadLPS',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-3">
              <div class="form group">
<small>{!! Form::label('lblPotabilizacionSedimentacion','Potabilización') !!}</small>
<br><small>{!! Form::label('lblPotabilizacionSedimentacion2','(TIPO TRATAMIENTO (SEDIMENTACION))') !!}</small>
{!! Form::text('P_Sedimentacion',null, ['class' => 'form-control']) !!}
</div>
            </div>
          
        </div>







<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
<small>{!! Form::label('lblPotabilizacionFiltracion','Potabilización') !!}</small><br>
<small>{!! Form::label('lblPotabilizacionFiltracion2','(TIPO TRATAMIENTO (FILTRACION))') !!}</small>
{!! Form::text('P_Filtracion',null, ['class' => 'form-control']) !!}
</div>
          
            <div class="col-md-3">
                           <div class="form group">
<small>{!! Form::label('lblPotabilizacionFlocuacion','Potabilización') !!}</small><br>
<small>{!! Form::label('lblPotabilizacionFlocuacion2','(TIPO TRATAMIENTO (FLOCULACION))') !!}</small>
{!! Form::text('P_Floculacion',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-3">
   <div class="form group">
<small>{!! Form::label('lblPotabilizacionDesinfeccion','Potabilización') !!}</small><br>
<small>{!! Form::label('lblPotabilizacionDesinfeccion','(TIPO TRATAMIENTO (DESINFECCION))') !!}</small>

{!! Form::text('P_Desinfeccion',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-3">
  <div class="form group">
<small>{!! Form::label('lblAlmacenamiento',' Almacenamiento') !!}</small><br>
<small>{!! Form::label('lblAlmacenamiento',' (CAPACIDAD m3)') !!}</small>
{!! Form::text('AlmacenaminetoM3',null, ['class' => 'form-control']) !!}
</div>
            </div>
          
           
        </div>



<hr>

<div class="row">
    <div class="col-md-12">
        <div class="row">
        <div class="col-md-3">
          <div class="form group">
          <small>{!! Form::label('lblTipoTanque',' ') !!}</small><br>
          <small>{!! Form::label('lblTipoTanque',' ') !!}</small><br>
<small>{!! Form::label('lblTipoTanque','Tipotanque') !!}</small>
{!! Form::text('TipoTanque',null, ['class' => 'form-control']) !!}
</div>
</div>
            <div class="col-md-3">
    <div class="form group">
<small>{!! Form::label('lblRedDistribucionVarios','Red Distribución') !!}</small>
<small>{!! Form::label('lblRedDistribucionVarios2','(DIAM. MAX. RED DISTRI. (IN)/(MATERIAL) / DIAM. MIN. RED DISTRI. (IN)/(MATERIAL)) ') !!}</small>
{!! Form::text('R_DiametroIN',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-3">
 <div class="form group">
<small>{!! Form::label('lblRedDistribucionLongitud','Red Distribución') !!}</small><br>

<small>{!! Form::label('lblRedDistribucionLongitud2','(LONGITUD RED DISTRIBUCION (m))') !!}</small><br>
<small>{!! Form::label('lblRedDistribucionLongitud3',' ') !!}</small>
{!! Form::text('R_LongitudM',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-3">
<div class="form group">

<small>{!! Form::label('lblRedDistribucionEstado2','Red Distribución') !!}</small><br>

<small>{!! Form::label('lblRedDistribucionEstado3','(ESTADO (B/M/R))') !!}</small><br>
<small>{!! Form::label('lblRedDistribucionEstado',' ') !!}</small>
{!! Form::text('R_Estado',null, ['class' => 'form-control']) !!}
</div>
            </div>
          
           
        </div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
     
          
            <div class="col-md-4">
    
<div class="form group">
<small>{!! Form::label('lblRedDistribucionSuscriptores','Red Distribución (No. CONEXIONES DOMICILIARIAS LEGALIZADAS (SUSCRIPTORES))') !!}</small>
{!! Form::text('R_ConexionesLegales',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-4">
 <div class="form group">
<small>{!! Form::label('lblRedDistribucionConexionesIlegales','Red Distribución (No. CONEXIONES DOMICILIARIAS ILEGALES (USUARIOS))') !!}</small>
{!! Form::text('R_ConexionesIlegales',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-4">
<div class="form group">
<small>{!! Form::label('lblRedDistribucionDeficit','Red Distribución (DEFICIT ATENDER LA NECESIDAD DEL MOMENTO)') !!}</small>
{!! Form::text('R_Deficit',null, ['class' => 'form-control']) !!}
</div>
            </div>
          
           
        </div>






<div class="row">
    <div class="col-md-12">
        <div class="row">
     
          
            <div class="col-md-4">
    <div class="form group">
<small>{!! Form::label('lblRT_TipoMedidor','Regimen Tarifario (TIPO MEDIDOR)') !!}</small>
{!! Form::text('RT_TipoMedidor',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-4">
 <div class="form group">
<small>{!! Form::label('lblRT_TarifaMEs','Regimen Tarifario (TARIFA MES)') !!}</small>
{!! Form::text('RT_TarifaMEs',null, ['class' => 'form-control']) !!}
</div>
            </div>
            <div class="col-md-4">
<div class="form group">
<small>{!! Form::label('lblRT_TarifaAnual','Regimen Tarifario (TARIFA ANUAL)') !!}</small>
{!! Form::text('RT_TarifaAnual',null, ['class' => 'form-control']) !!}
</div>
            </div>
          
           
        </div>






<div class="row">
    <div class="col-md-12">
        <div class="row">
          <div class="col-md-3">

<div class="form group">
<small>{!! Form::label('lblRegimenTarifarioTarifaUnica','Regimen Tarifario (TARIFA UNICA)') !!}</small>
{!! Form::text('RT_TarifaUnica',null, ['class' => 'form-control']) !!}
</div>
          </div>
             <div class="col-md-3">

<div class="form group">
<small>{!! Form::label('lblNoUsuarios','No Usuarios') !!}</small>
{!! Form::text('NoUsuarios',null, ['class' => 'form-control']) !!}
</div>
          </div>
             <div class="col-md-3">


<div class="form group">
<small>{!! Form::label('lblFechaCensoUsuarios','Fecha Censo Usuarios') !!}</small>
{!! Form::date('FechaCenso',null, ['class' => 'form-control']) !!}
</div>
          </div>
             <div class="col-md-3">
        <div class="form group">
<small>{!! Form::label('lblPUEEA','PUEEA') !!}</small>
{!! Form::text('PUEEA',null, ['class' => 'form-control']) !!}
</div>

          </div>     
     </div>
     </div>








<div class="row">
    <div class="col-md-12 ">
        <div class="row">

          <div class="col-md-3">




</br>

<div class="form group">
 {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>

  </div>     
     </div>
     </div>
     </div>