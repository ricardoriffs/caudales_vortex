@extends('layouts.dashboard')
@section('page_heading','Acueductos V. y M.')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <a class="btn btn-primary pull-right" href=" {{ route('acueductosvym.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de acueductosvym
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>   <th>
                                         No.Captacion
                                        </th>
                                        <th>
                                            AduccionDiametroIn
                                        </th>
                                        <th>
                                            AduccionLongitud
                                        </th>
                                        
                                        <th>
                                            DesaneradorLPS
                                        </th>
                                       
                                        <th>
                                            NoUsuarios
                                        </th>
                                        <th>
                                            FechaCensoUuarios
                                        </th>
                                        <th>
                                            PUEEA
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($acueductosvym as $acueductovym)
                                    <tr>
                                     <td>
                                        {{$acueductovym->NoCaptacion}}
                                        </td>
                                        <td>
                                            {{$acueductovym->AduccionDiametroIn}}
                                        </td>
                                        <td>
                                            {{$acueductovym->AduccionLongitud}}
                                        </td>
                                      
                                        <td>
                                            {{$acueductovym->DesaneradorLPS}}
                                        </td>
                                       
                                        <td>
                                            {{$acueductovym->NoUsuarios}}
                                        </td>
                                        <td>
                                            {{$acueductovym->FechaCensoUsuarios}}
                                        </td>
                                        <td>
                                            {{$acueductovym->PUEEA}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('acueductosvym.edit', $acueductovym->IDAcueductosVyM)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $acueductosvym->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

