@extends('layouts.dashboard')
@section('page_heading','Crear Acueductos V. y M.')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-12"> 
            <a class="btn btn-primary pull-left" href =" {{ route('acueductosvym.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver AcueductosVyM
            </a>
        </div>
        <div class="col-sm-12" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Nuevo acueductovym
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                        {!!Form::open(['route' => 'acueductosvym.store'])!!}
                        @include('acueductosvym.partials.form')
                        {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop

