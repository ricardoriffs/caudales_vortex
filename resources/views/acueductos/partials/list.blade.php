<div class="row">
    <div class="col-sm-12">
        <br>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de acueductos
                </div>
                <div class="panel-body">
               @include('widgets.info')
               @include('widgets.error')
                    <div class="col-sm-12">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>
                                       No.Captacion
                                    </th>                                        <th>
                                        AduccionDiametroIn
                                    </th>                                        <th>
                                        AduccionLongitud
                                    </th>                                        
                                    </th>                                        <th>
                                        Nousuarios
                                    </th>                                        <th>
                                        FechaCensoUuarios
                                    </th>                                        <th>
                                        PUEEA
                                    </th>
                            </thead>
                            <tbody>
                                @foreach($acueductos as $acueducto)
                                <tr>
                                    <td>
                                        {{$acueducto->IDCaptacion}}
                                    </td>
                                    <td>
                                        {{$acueducto->A_DiametroIN}}
                                    </td>
                                    <td>
                                        {{$acueducto->A_LongitudM}}
                                    </td>

                                    <td>
                                        {{$acueducto->NoUsuarios}}
                                    </td>
                                    <td>
                                        {{$acueducto->FechaCenso}}
                                    </td>
                                    <td>
                                        {{$acueducto->PUEEA}}
                                    </td>

                                    <td>
                                   <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('acueductos.edit', $acueducto->IDCaptacion)}}"> Editar </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!!  $acueductos->render() !!}
                    </div>
                </div>
            </div>
        </br>
    </div>
</div>
