<div class="row">
    <div class="col-md-6">
        <fieldset>        
            <legend>Aducción</legend>    
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblAduccionLongitud2','*Diámetro(In)/ Material') !!}
                    {!! Form::number('A_DiametroIN',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblAduccionLongitud2','*Longitud (m)') !!}
                    {!! Form::number('A_LongitudM',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblAduccionEstado2','*Estado (B/M/R)') !!}
                    {!! Form::select('A_Estado', ['' => 'Seleccione un estado ..','BUENO' => 'BUENO', 'REGULAR' => 'REGULAR','MALO' => 'MALO'], null, ['class' => 'form-control'])!!}
                </div>
            </div>
        </fieldset>
    </div>
    <div class="col-md-6">
        <fieldset>        
            <legend>Desanerador</legend>        
            <div class="acueductos-field-m">
                <div class="form group">
                    <br/>{!! Form::label('lblDesaneradorLPS2','*Capacidad (lps)') !!}
                    {!! Form::number('D_CapacidadLPS',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-m">
                <div class="form group">
                    <br/>{!! Form::label('lblDesaneradorEstado2','*Estado (B/M/R)') !!}
                    {!! Form::select('D_Estado', ['' => 'Seleccione un estado ..','BUENO' => 'BUENO', 'REGULAR' => 'REGULAR','MALO' => 'MALO'], null, ['class' => 'form-control'])!!}
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <fieldset>        
            <legend>Conducción</legend>        
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblConduccionDiametroIn2','*Diámetro(In)/ Material') !!}
                    {!! Form::number('C_DiametroIN',null, ['class' => 'form-control']) !!}
                </div>
            </div>    
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblConduccionLongitud2','*Longitud (m)') !!}
                    {!! Form::number('C_Longitdu',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblConduccionLongitud2','*Estado (B/M/R)') !!}
                    {!! Form::select('C_Estado', ['' => 'Seleccione un estado ..','BUENO' => 'BUENO', 'REGULAR' => 'REGULAR','MALO' => 'MALO'], null, ['class' => 'form-control'])!!}
                </div>
            </div>
        </fieldset>
    </div>
    <div class="col-md-6">
        <fieldset>        
            <legend>Potabilización</legend>                 
            <div class="acueductos-field-m">
                <div class="form group">
                    <br/>{!! Form::label('lblPotabilizacionLPS2','*Capacidad (lps)') !!}
                    {!! Form::number('P_CapacidadLPS',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-m">
                <br/>{!! Form::label('lblpactuallps3','*Actualmente Opera (lps)') !!}
                {!! Form::number('P_ActualLPS',null, ['class' => 'form-control']) !!}
            </div>            
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <fieldset>        
            <legend>Potabilización</legend>           
            <div class="acueductos-field-c">
                <div class="form group">
                    <br/>{!! Form::label('lblPotabilizacionSedimentacion2','*Tipo Tratamiento (Sedimentación)') !!}
                    {!! Form::text('P_Sedimentacion',null, ['class' => 'form-control']) !!}
                </div>
            </div>    
            <div class="acueductos-field-c">
                <div class="form group">        
                    <br/>{!! Form::label('lblPotabilizacionFiltracion2','*Tipo Tratamiento (Filtración)') !!}
                    {!! Form::text('P_Filtracion',null, ['class' => 'form-control']) !!}
                </div>        
            </div>
            <div class="acueductos-field-c">
                <div class="form group">
                    <br/>{!! Form::label('lblPotabilizacionFlocuacion2','*Tipo Tratamiento (Floculación)') !!}
                    {!! Form::text('P_Floculacion',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-c">
                <div class="form group">
                    <br/>{!! Form::label('lblPotabilizacionDesinfeccion3','*Tipo Tratamiento (Desinfección)') !!}
                    {!! Form::text('P_Desinfeccion',null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </fieldset>       
    </div>    
    <div class="col-md-3">
        <div class="form group">
            <br/><br/><br/><br/>{!! Form::label('lblTipoTanque','*Tipo tanque') !!}
            {!! Form::text('TipoTanque',null, ['class' => 'form-control']) !!}
        </div>
    </div>     
</div>

<hr/>

<div class="row">
    <div class="col-md-8">
        <fieldset>        
            <legend>Red Distribución</legend>        
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblRedDistribucionVarios2','*Diam. Max. Red Distri. (IN)/Material / Diam. Min. Red Distri. (IN)/Material') !!}
                    {!! Form::number('R_DiametroIN',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/><br/>{!! Form::label('lblRedDistribucionLongitud2','*Longitud Red Distribución (m)') !!}
                    {!! Form::number('R_LongitudM',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/><br/>{!! Form::label('lblRedDistribucionEstado3','*Estado (B/M/R)') !!}
                    {!! Form::select('R_Estado', ['' => 'Seleccione un estado ..','BUENO' => 'BUENO', 'REGULAR' => 'REGULAR','MALO' => 'MALO'], null, ['class' => 'form-control'])!!}
                </div>
            </div>    
        </fieldset>
    </div>
    <div class="col-md-3">
        <fieldset>        
            <legend>Almacenamiento</legend>                        
            <div class="form group">
                <br/><br/>{!! Form::label('lblAlmacenamiento','*Capacidad (m3)') !!}
                {!! Form::number('AlmacenaminetoM3',null, ['class' => 'form-control']) !!}
            </div>
        </fieldset>            
    </div>            
</div>
<div class="row">    
    <div class="col-md-8">
        <fieldset>        
            <legend>Red Distribución</legend>            
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblRedDistribucionSuscriptores','*No. Conexiones Domiciliarias Legalizadas (Suscriptores)') !!}
                    {!! Form::number('R_ConexionesLegales',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblRedDistribucionConexionesIlegales','*No. Conexiones Domiciliarias Ilegales (Usuarios)') !!}
                    {!! Form::number('R_ConexionesIlegales',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/><br/>{!! Form::label('lblRedDistribucionDeficit','*Déficit Atender la Necesidad del Momento') !!}
                    {!! Form::text('R_Deficit',null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </fieldset>            
    </div>              
</div>
<div class="row">
    <div class="col-md-8">
        <fieldset>        
            <legend>Régimen Tarifario</legend>           
            <div class="acueductos-field-c">
                <div class="form group">
                    <br/>{!! Form::label('lblRT_TipoMedidor','*Tipo Medidor') !!}
                    {!! Form::text('RT_TipoMedidor',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-c">
                <div class="form group">
                    <br/>{!! Form::label('lblRT_TarifaMEs','*Tarifa Mes') !!}
                    {!! Form::text('RT_TarifaMEs',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-c">
                <div class="form group">
                    <br/>{!! Form::label('lblRT_TarifaAnual','*Tarifa Anual') !!}
                    {!! Form::text('RT_TarifaAnual',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-c">
                <div class="form group">
                    <br/>{!! Form::label('lblRegimenTarifarioTarifaUnica','*Tarifa Única') !!}
                    {!! Form::text('RT_TarifaUnica',null, ['class' => 'form-control']) !!}
                </div>
            </div>            
        </fieldset>            
    </div>                   
</div>
<br/>
<div class="row">
    <div class="col-md-8">
        <fieldset>  
            <legend></legend>
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblNoUsuarios','*No. Usuarios') !!}
                    {!! Form::number('NoUsuarios',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblFechaCensoUsuarios','*Fecha Censo Usuarios') !!}
                    {!! Form::date('FechaCenso',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="acueductos-field-t">
                <div class="form group">
                    <br/>{!! Form::label('lblPUEEA','*Pueea') !!}
                    {!! Form::text('PUEEA',null, ['class' => 'form-control']) !!}
                </div>
            </div>    
        </fieldset>            
    </div>             
</div>
<br/>
<div class="form group">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>

@if(isset($acueductos))
@include('acueductos.partials.list')
@endif
