@extends('layouts.dashboard')
@section('page_heading','Acueductos')
@section('section')
<div class="container">
    <div class="col-sm-8 col-sm-offset-2">
        <a class="btn btn-primary pull-right" href=" {{ route('acueductos.create') }}">
            <i class="fa fa-plus-circle" ></i> Nuevo
        </a>
    </div>
    @include('acueductos.partials.list')
</div>  
 @stop

