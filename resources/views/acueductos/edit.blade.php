@extends('layouts.dashboard')
@section('page_heading','Editar Acueductos')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-12"> 
            <a class="btn btn-primary pull-left" href =" {{ route('acueductos.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Acueductos
            </a>
        </div>
        <div class="col-sm-12" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar acueducto
                   </div>
                    <div class="panel-body">
                      @include('widgets.info')
                      @include('widgets.error')
                      {!!Form::model($acueducto,['route' => ['acueductos.update',$acueducto->IDCaptacion], 'method' =>'PUT']) !!}
                  	  @include('acueductos.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
