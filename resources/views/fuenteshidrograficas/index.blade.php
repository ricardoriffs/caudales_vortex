@extends('layouts.dashboard')
@section('page_heading','Fuentes Hidrograficas')
@section('section')
<div class="container">
    <div class="row">
       
        <div class="col-sm-8 col-sm-offset-2">
            <br>


            <div class="panel panel-default">
                    <div class="panel-heading">
                       Ingresar fuente hidrografica
                    </div>
                    <div class="panel-body">
                     {!!Form::open(['route' => 'fuenteshidrograficas.store'])!!}
                         <div class="form group">
                        {!! Form::label('lblIDFuente','IDFuente') !!}
                        {!! Form::text('IDFuente',null, ['class' => 'form-control']) !!}
                        </div>
                         <div class="form group">
                        {!! Form::label('lblIDSubCuenca','IDSubCuenca') !!}
                        {!! Form::text('IDSubCuenca',null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form group">
                        {!! Form::label('lblNombre','Nombre') !!}
                        {!! Form::text('Nombre',null, ['class' => 'form-control']) !!}
                        </div>

                        </br>
                        <div class="form group">
                        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                        </div>               

                      {!! Form::close()!!}
                    </div>
            </div>
                   
                   @include('widgets.info')
                    @include('widgets.error')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de fuentes hidrograficas
                    </div>
                    <div class="panel-body">
                  
                        <div class="col-sm-12">

               

                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                     <th with="20px">
                                            IDFuente
                                        </th>
                                        <th with="20px">
                                            IDSubCuenca
                                        </th>
                                        <th>
                                          Nombre
                                        </th>
                                       
                                    </tr>
                                </thead>
                                <tbody>                                    
                                    @foreach($FuentesHidrograficas as $FuenteHidrografica)
                                    <tr>
                                    <td>
                                            {{$FuenteHidrografica->idfuente}}
                                        </td>
                                        <td>
                                            {{$FuenteHidrografica->idsubcuenca}}
                                        </td>
                                        <td>
                                            {{$FuenteHidrografica->nombre}}
                                        </td> 
                                         </tr>
                                    @endforeach                                 
                                </tbody>
                            </table>
                         {!!  $FuentesHidrograficas->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>    
 @stop
