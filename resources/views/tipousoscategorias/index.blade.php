@extends('layouts.dashboard')
@section('page_heading','Categorias de tipos de usos')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('tipousoscategorias.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado Tipo Usos Categorias
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Nombre Categoria
                                        </th>
                                        <th>
                                            Módulo de consumo
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($tipousoscategorias as $tipousocategoria)
                                    <tr>
                                        <td>
                                            {{$tipousocategoria->nombrecategoria}}
                                        </td>
                                        <td>
                                            {{$tipousocategoria->descripcion}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('tipousoscategorias.edit', $tipousocategoria->idtipousoscategoria)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $tipousoscategorias->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

