@extends('layouts.dashboard')
@section('page_heading','Editar categorias de tipos de usos')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('tipousoscategorias.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Tipo Usos Categorias
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar Tipo Usos Categorias
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($tipousocategoria,['route' => ['tipousoscategorias.update',$tipousocategoria->idtipousoscategoria], 'method' =>'PUT']) !!}
                  	  @include('tipousoscategorias.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
