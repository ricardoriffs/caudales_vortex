@extends('layouts.dashboard')
@section('page_heading','Crear categorias de tipos de usos ')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('tipousoscategorias.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Tipo Usos Categorias
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Nuevo Tipo Usos Categorias
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                        {!!Form::open(['route' => 'tipousoscategorias.store'])!!}
                        @include('tipousoscategorias.partials.form')
                        {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop

