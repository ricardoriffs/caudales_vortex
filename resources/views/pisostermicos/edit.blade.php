@extends('layouts.dashboard')
@section('page_heading','Editar PisosTermicos')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('pisostermicos.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver PisosTermicos
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar pisotermico
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($pisotermico,['route' => ['pisostermicos.update',$pisotermico->IDPisoTermico], 'method' =>'PUT']) !!}
                  	  @include('pisostermicos.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
