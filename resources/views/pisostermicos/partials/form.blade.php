<div class="form group">
{!! Form::label('lblPisoTermico','Pisotermico') !!}
{!! Form::text('PisoTermico',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblDescripcion','Descripcion') !!}
{!! Form::text('Descripcion',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblDesde','Desde') !!}
{!! Form::text('Desde',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblHasta','Hasta') !!}
{!! Form::text('Hasta',null, ['class' => 'form-control']) !!}
</div>

</br>
<div class="form group">
 {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>
