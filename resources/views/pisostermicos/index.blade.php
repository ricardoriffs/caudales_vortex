@extends('layouts.dashboard')
@section('page_heading','PisosTermicos')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('pisostermicos.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de pisostermicos
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Pisotermico
                                        </th>
                                        <th>
                                            Descripcion
                                        </th>
                                        <th>
                                            Desde
                                        </th>
                                        <th>
                                            Hasta
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($pisostermicos as $pisotermico)
                                    <tr>
                                        <td>
                                            {{$pisotermico->pisotermico}}
                                        </td>
                                        <td>
                                            {{$pisotermico->descripcion}}
                                        </td>
                                        <td>
                                            {{$pisotermico->desde}}
                                        </td>
                                        <td>
                                            {{$pisotermico->hasta}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('pisostermicos.edit', $pisotermico->idpisotermico)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $pisostermicos->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

