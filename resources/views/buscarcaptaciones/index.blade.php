@extends('layouts.dashboard')
@section('page_heading','Busqueda de captaciones')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="alert alert-info alert-dismissable " role="alert">
                <button aria-label="Close" class="close" data-dismiss="alert" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
                <i class="fa fa-info">
                </i>
                Esta vista esta sujeta a aprobación por parte de la dependencia de DGOAT
            </div>
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de captaciones
                    </div>

                    <div class="panel-body">
                        @include('widgets.info')
                    @include('widgets.error')
                        <div class="col-sm-12">


                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th with="20px">
                                            <div class="input-group custom-search-form">
                                                <input class="form-control" placeholder="Filtrar..." type="text">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button">
                                                            <i class="fa fa-filter">
                                                            </i>
                                                        </button>
                                                    </span>
                                                </input>
                                            </div>
                                            No.captacion
                                        </th>
                                        <th>
                                            <div class="input-group custom-search-form">
                                                <input class="form-control" placeholder="Filtrar..." type="text">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button">
                                                            <i class="fa fa-filter">
                                                            </i>
                                                        </button>
                                                    </span>
                                                </input>
                                            </div>
                                            Cuenca
                                        </th>
                                        <th>
                                            <div class="input-group custom-search-form">
                                                <input class="form-control" placeholder="Filtrar..." type="text">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button">
                                                            <i class="fa fa-filter">
                                                            </i>
                                                        </button>
                                                    </span>
                                                </input>
                                            </div>
                                            Encuestador
                                        </th>
                                        <th>
                                        <div class="input-group custom-search-form">
                                                <input class="form-control" placeholder="Filtrar..." type="text">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button">
                                                            <i class="fa fa-filter">
                                                            </i>
                                                        </button>
                                                    </span>
                                                </input>
                                            </div>
                                            Tipo de Uso
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <tr>

                                        <td>
                                            1
                                        </td>
                                        <td>
                                            Río Sumapaz
                                        </td>
                                        <td>
                                            Julian Contreras
                                        </td>
                                        <td>
                                            Agricola
                                        </td>
                                        <td>
                                            <a class="btn btn-primary btn-outline btn-sm pull-right" href="#">
                                                Cargar
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            
                        </div>
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
