@extends('layouts.dashboard')
@section('page_heading','Editar Captaciones')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('captaciones.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver Captaciones
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar captacion
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($captacion,['route' => ['captaciones.update',$captacion->IDCaptacion], 'method' =>'PUT']) !!}
                  	  @include('captaciones.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
