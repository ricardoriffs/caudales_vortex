@extends('layouts.dashboard')
@section('page_heading','Caracteristicas')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('captaciones.create') }}">
                <i class="fa fa-plus-circle" ></i> Nueva
            </a>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de captaciones
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                          @include('captaciones.partials.grid')
                   
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

