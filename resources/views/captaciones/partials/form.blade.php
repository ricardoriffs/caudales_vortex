<br/>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Cuenca 2do orden') !!}
                    {!!  Form::select('IDCuenca',$cuencashidrograficas,null,['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Cuenca 3do orden') !!}
                    <select name="IDSubCuenca" class="form-control"></select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Cuenca 4do orden') !!}
                    <select name="CodigoFuente" class="form-control"></select>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Pendiente') !!}

                    {!!  Form::select('Pendiente', ['' => 'Seleccione…', 'Alta' => 'Alta', 'Media' => 'Media',
                    'Baja' => 'Baja', 'Por definir' => 'Por definir'],  'S', ['class' => 'form-control' ]) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Tipo de Estructura:') !!}
                    {!! Form::select('TipoEstructura',$estructuracaptaciones,null,['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Estado:') !!}

                    {!!  Form::select('Estado', ['' => 'Seleccione…', 'Bueno' => 'Bueno', 'Regular' => 'Regular', 'Malo' => 'Malo'],  'S', ['class' => 'form-control' ]) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Capacidad Instalada (LPS):') !!}
                    {!! Form::number('CapacidadInstaladaLPS',null, ['class' => 'form-control','step' => 'any']) !!}
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Este (Coordenada X): ') !!}
                    {!! Form::number('CoordX',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Norte (Coordenada Y): ') !!}
                    {!! Form::number('CoordY',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Altura: ') !!}
                    {!! Form::number('Altura',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Capacidad Instalada (IN):') !!}
                    {!! Form::number('Dimension',null, ['class' => 'form-control','step' => 'any']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<br>

<div class="form group">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>