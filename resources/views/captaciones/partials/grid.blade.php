    <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr><th>
                                            No. Captación
                                        </th>
                                        
                                        <th>
                                            CodigoFuente
                                        </th>
                                       
                                        <th>
                                            Coordx
                                        </th>
                                        <th>
                                            Coordy
                                        </th>
                                        <th>
                                            Altura
                                        </th>
                                        <th>
                                            Pendiente
                                        </th>
                                        <th>
                                            TipoEstructura
                                        </th>
                                        <th>
                                            Estado
                                        </th>
                                        <th>
                                            Dimension
                                        </th>
                                        <th>
                                            Capacidad LPS
                                        </th>
                                       

                                </thead>
                                <tbody>
                                    @foreach($captaciones as $captacion)
                                    <tr>
                                     <td>
                                        <b>{{$captacion->idcaptacion}}</b>
                                        </td>
                                        
                                        <td>
                                            {{$captacion->codigofuente}}
                                        </td>
                                        
                                        <td>
                                            {{$captacion->coordx}}
                                        </td>
                                        <td>
                                            {{$captacion->coordy}}
                                        </td>
                                        <td>
                                            {{$captacion->altura}}
                                        </td>
                                        <td>
                                            {{$captacion->pendiente}}
                                        </td>
                                        <td>
                                            {{$captacion->tipoestructura}}
                                        </td>
                                        <td>
                                            {{$captacion->estado}}
                                        </td>
                                        <td>
                                            {{$captacion->dimension}}
                                        </td>
                                        <td>
                                            {{$captacion->capacidadinstaladalps}}
                                        </td>
                                       

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('captaciones.edit', $captacion->idcaptacion)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $captaciones->render() !!}
                        </div>
