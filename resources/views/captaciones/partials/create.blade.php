
@foreach($captaciones as $captacion)
<div class="row">
    <div class="col-md-12">


   <div class="row">
            <div class="col-md-3">
                <div class="form group">
                    {!! Form::label('modulo','*Tipo de Fuente') !!}
                   {!! Form::text('tipofuente',$captacion->nombretipofuente, ['class' => 'form-control','readonly']) !!}
                </div>
               
            </div>
            <div class="col-md-3">
         

                 <div class="form group">
                    {!! Form::label('modulo','*Cuenca') !!}
                  {!! Form::text('cuenca',$captacion->idcuenca.'-'.$captacion->nombrecuenca, ['class' => 'form-control','readonly']) !!}
                </div>
        
          

               </div>
               <div class="col-md-3">
               <div class="form group">
                  {!! Form::label('modulo','*SubCuenca') !!}
                 {!! Form::text('subcuenca',$captacion->idsubcuenca, ['class' => 'form-control']) !!}
                  </div>
               
             </div>
            <div class="col-md-3">
                   <div class="form group">
                              {!! Form::label('modulo','*Fuente') !!}
                                 {!! Form::text('fuente',$captacion->codigofuente, ['class' => 'form-control']) !!}
                    </div>        
        
               </div>

              
        </div>



           <div class="row">
            <div class="col-md-3">
             <div class="form group">
                                        {!! Form::label('modulo','*Pendiente') !!}
                     
                                     {!! Form::text('Pendiente',$captacion->pendiente, ['class' => 'form-control','readonly']) !!}
                                </div> 
               
                
            </div>
            <div class="col-md-3">

             <div class="form group">
                                    {!! Form::label('modulo','*Tipo de Estructura:') !!}
                                       {!! Form::text('tipoestructura',$captacion->tipoestructura, ['class' => 'form-control']) !!}
                                </div>
         
      
          

               </div>
               <div class="col-md-3">
                <div class="form group">

                    {!! Form::label('modulo','*Estado:') !!}
     
                    {!! Form::text('modulo',$captacion->estado, ['class' => 'form-control','readonly']) !!}

                </div>
               
               
                
               
             </div>
            <div class="col-md-3">
           <div class="form group">
                                            {!! Form::label('modulo','*Capacidad Instalada (LPS):') !!}
                                      {!! Form::number('CapacidadInstaladaLPS',$captacion->capacidadinstaladalps, ['class' => 'form-control','step' => 'any','readonly']) !!}
                                        </div>
         
          


               </div>
        </div>






   <div class="row">
            <div class="col-md-3">
                      <div class="form group">
                    {!! Form::label('modulo','*Este (Coordenada X): ') !!}
  {!! Form::number('CoordX',$captacion->coordx, ['class' => 'form-control','readonly']) !!}
                </div>         
               
            </div>
            <div class="col-md-3">

              <div class="form group">
                                    {!! Form::label('modulo','*Norte (Coordenada Y): ') !!}
                  {!! Form::number('CoordY',$captacion->coordy, ['class' => 'form-control','readonly']) !!}
             </div>
         
                       
                          

               </div>
               <div class="col-md-3">
                 <div class="form group">
                    {!! Form::label('modulo','*Altura: ') !!}
                      {!! Form::number('Altura',$captacion->altura, ['class' => 'form-control','readonly']) !!}
                </div>
                              
             </div>
            <div class="col-md-3">
         
                        <div class="form group">
                                    {!! Form::label('modulo','*Dimensión (in):') !!}
                                    {!! Form::number('Dimension',$captacion->dimension, ['class' => 'form-control','step' => 'any','readonly']) !!}
                        </div>          
        


               </div>
        </div>



 
  


    </div>
</div>
<br>
   @endforeach

  <a class="btn btn-primary pull-left" href="{{ route('captaciones.index') }}">
                <i class="fa fa-edit">
                </i>
                Editar
            </a>
