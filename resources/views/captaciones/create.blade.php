@extends('layouts.dashboard')
@section('page_heading','  Nueva captación ')
@section('section')

<div class="row">
    <div class="col-sm-2">
        <a class="btn btn-primary pull-left" href="{{ route('captaciones.index') }}">
            <i class="fa fa-search fa-fw"></i>
            Consultar Captaciónes
        </a>
    </div>

    <div class="col-sm-2 col-sm-offset-8">
        <a class="btn btn-primary pull-right" href="{{ route('fichatecnica.consultafichatecnica') }}">
            <i class="fa fa-file-text-o">
            </i>
            Vista previa
        </a>
    </div>


</div>
<br>




<div class="row">


    <div class="col-lg-12">

    </div>
</div>


<div class="row">
    <div class="col-sm-12">
        @include('widgets.info')
        @include('widgets.error')
    </div> 
</div> 




<div class="row">


    <div class="col-lg-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                @if ( !empty ( session('No_Captacion') ) )                               

                <b>  {{'Registro No. '.session('No_Captacion') }}</b>

                @endif

            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Nav tabs -->






                <ul class="nav nav-tabs">
                    <li class="active"><a href="#caracteristicas" data-toggle="tab">Características</a>
                    </li>
                    <li><a href="#encuestados" data-toggle="tab">Encuestados</a>
                    </li>

                    <li><a href="#predio" data-toggle="tab">Predio</a>
                    </li>

                    <li><a href="#infojuridica" data-toggle="tab">Información Juridica</a>
                    </li>

                    <li><a href="#aforos" data-toggle="tab">Aforos</a>
                    </li>
                    <li><a href="#acueductos" data-toggle="tab">Acueductos</a>
                    </li>
                    <li><a href="#calidaddelagua" data-toggle="tab">Calidad del agua</a>
                    </li>
                    <li><a href="#cuencas" data-toggle="tab">Cuenca</a>
                    </li>
                    <li><a href="#usosdelpredio" data-toggle="tab">Usos del predio</a>
                    </li>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">


                    <div class="tab-pane fade in active" id="caracteristicas">



                        @if ( empty ( session('No_Captacion') ) )                               

                        {!!Form::open(['route' => 'captaciones.store'])!!}
                        @include('captaciones.partials.form')
                        {!! Form::close()!!}  

                        @endif                 


                        @if ( !empty ( session('No_Captacion') ) )                               

                        @include('captaciones.partials.create')

                        @endif


                    </div>

                    <div class="tab-pane fade" id="encuestados">

                        {!!Form::open(['route' => 'encuestados.store'])!!}

                        {{ Form::hidden('TipoFormulario', 'tabular') }}
                        @include('encuestados.partials.form')

                        {!! Form::close()!!}
                    </div>



                    <div class="tab-pane fade" id="predio">
                        {!!Form::open(['route' => 'predios.store', 'files' => true])!!}
                        {{ Form::hidden('TipoFormulario', 'tabular') }}                                     
                        @include('predios.partials.form')

                        {!! Form::close()!!}

                    </div>


                    <div class="tab-pane fade" id="infojuridica">
                        <br>
                        <p class="text-danger">Solo diligenciar este formulario en caso de que la captación tenga una concesión.</p>
                        <br>

                        {!!Form::open(['route' => 'expedientesjuridicos.store'])!!}
                        {{ Form::hidden('TipoFormulario', 'tabular') }}                                     
                        @include('expedientesjuridicos.partials.form')

                        {!! Form::close()!!}

                    </div>

                    <div class="tab-pane fade" id="aforos">
                        {!!Form::open(['route' => 'aforos.store'])!!}
                        {{ Form::hidden('TipoFormulario', 'tabular') }}                                     
                        @include('aforos.partials.form')                                   
                        {!! Form::close()!!}

                    </div>
                    <div class="tab-pane fade" id="acueductos">
                        {!!Form::open(['route' => 'acueductos.store'])!!}
                        {{ Form::hidden('TipoFormulario', 'tabular') }}                                     
                        @include('acueductos.partials.form')                                   
                        {!! Form::close()!!}

                    </div>
                    <div class="tab-pane fade" id="calidaddelagua">

                        {!!Form::open(['route' => 'calidadesagua.store'])!!}
                        {{ Form::hidden('TipoFormulario', 'tabular') }}                                     
                        @include('calidadesagua.partials.form')                                   
                        {!! Form::close()!!}

                    </div>
                    <div class="tab-pane fade" id="cuencas">

                        {!!Form::open(['route' => 'cuencas.store'])!!}
                        {{ Form::hidden('TipoFormulario', 'tabular') }}                                     
                        @include('cuencas.partials.form')                                   
                        {!! Form::close()!!}

                    </div>
                    <div class="tab-pane fade" id="usosdelpredio">
                        {!!Form::open(['route' => 'usospredio.store'])!!}
                        {{ Form::hidden('TipoFormulario', 'tabular') }}                                     
                        @include('usospredio.partials.form')                                   
                        {!! Form::close()!!}
                    </div>


                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="IDCuenca"]').on('change', function () {
            var stateID = $(this).val();
            if (stateID) {
                $.ajax({
                    url: '../captaciones/SubCuencas/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {


                        $('select[name="IDSubCuenca"]').empty();
                        $('select[name="IDSubCuenca"]').append('<option value>Seleccione…</option>');

                        $.each(data, function (key, value) {
                            $('select[name="IDSubCuenca"]').append('<option value="' + key + '">' + value + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="IDSubCuenca"]').empty();
            }
        });

        $('select[name="IDSubCuenca"]').on('change', function () {
            var stateID = $(this).val();
            if (stateID) {
                $.ajax({
                    url: '../captaciones/ConsultarFuentes/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {


                        $('select[name="CodigoFuente"]').empty();
                        $('select[name="CodigoFuente"]').append('<option value>Seleccione…</option>');

                        $.each(data, function (key, value) {
                            $('select[name="CodigoFuente"]').append('<option value="' + key + '">' + value + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="CodigoFuente"]').empty();
            }
        });




        $('select[name="IDDepartamento"]').on('change', function () {
            var stateID = $(this).val();
            if (stateID) {
                $.ajax({
                    url: '../predios/Municipios/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {


                        $('select[name="IDMunicipio"]').empty();
                        $.each(data, function (key, value) {

                            $('select[name="IDMunicipio"]').append('<option value="' + key + '">' + value + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="IDMunicipio"]').empty();
            }
        });

        $('select[name="IDMunicipio"]').on('change', function () {
            var stateID = $(this).val();
            if (stateID) {
                $.ajax({
                    url: '../predios/Veredas/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {


                        $('select[name="IDVereda"]').empty();
                        $.each(data, function (key, value) {

                            $('select[name="IDVereda"]').append('<option value="' + key + '">' + value + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="IDVereda"]').empty();
            }
        });




        $('select[name="IDModulo"]').on('change', function () {
            var stateID = $(this).val();
            if (stateID) {
                $.ajax({
                    url: '../captaciones/consultarCategoriasDelTipoUso/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {

                        $('select[name="IDTipoUsosCategoria"]').empty();
                        //$('select[name="IDTipoUsosCategoria"]').append('<option value="">-- Seleccione... --</option>');
                        $.each(data, function (key, value) {

                            $('select[name="IDTipoUsosCategoria"]').append('<option value="' + key + '">' + value + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="IDTipoUsosCategoria"]').empty();
            }
        });



        $('select[name="IDTipoUsosCategoria"]').on('change', function () {
            var stateID = $(this).val();
            if (stateID) {
                $.ajax({
                    url: '../captaciones/TipoUso/' + stateID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {


                        $('select[name="IDTipoUso"]').empty();
                        //$('select[name="IDTipoUso"]').append('<option value="">-- Seleccione... --</option>');
                        $.each(data, function (key, value) {

                            $('select[name="IDTipoUso"]').append('<option value="' + key + '">' + value + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="IDTipoUso"]').empty();
            }
        });

    });
    function traerNombreCodigoCuenta(){
        document.getElementById("NombreCuenca_P").innerHTML = "Cargando...";
        document.getElementById("IDCuenca_P").innerHTML = "Cargando...";
        var IDCuenca = document.getElementById("Pes_CuencaIDCuenca").value;
        var IDSubCuenca = document.getElementById("Pes_CuencaIDSubCuenca").value;
        var CodigoFuente = document.getElementById("Pes_CuencaCodigoFuente").value;
        if(IDCuenca != "" && IDSubCuenca != "" && CodigoFuente != ""){
            setTimeout (function() {
                
                $.ajax({
                    url: '../nombreCuenca/'+IDCuenca+"/"+IDSubCuenca+"/"+CodigoFuente,
                    type: "get",
                    dataType: "json",
                    success: function (data) {
                        //document.getElementById("demo").innerHTML
                        document.getElementById("NombreCuenca_P").innerHTML = data;
                        document.getElementById("IDCuenca_P").innerHTML = IDCuenca+"-"+IDSubCuenca+"-"+CodigoFuente;
                    }
                });
            }, 2000);
        } else {
            document.getElementById("NombreCuenca_P").innerHTML = "";
            document.getElementById("IDCuenca_P").innerHTML = "";
        }
    }
    function desisionencuestados(){
        if($("#desicionS").is(':checked')) {  
            document.getElementById("nombreencuestado").value = document.getElementById("nombrepropietario").value + ' ' + document.getElementById("apellidopropietario").value;
            document.getElementById("documentoencuestado").value = document.getElementById("documentopropietario").value;
            
        } else {  
            if($("#desicionN").is(':checked')) {  
                document.getElementById("nombreencuestado").value = '';
                document.getElementById("documentoencuestado").value = '';
            } else {  
                alert("No está activado ninguno");  
            }
        } 
    }
    function representantelegal(){
        if($("#desicionRLS").is(':checked')) {  
            document.getElementById("representantelegalform").setAttribute("style","display:block");
        } else {  
            if($("#desicionRLN").is(':checked')) {  
                document.getElementById("representantelegalform").setAttribute("style","display:none");
            } else {  
                alert("No está activado ninguno");  
            }
        } 
    }
</script>
@stop