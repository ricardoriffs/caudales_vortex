<div class="form group">
{!! Form::label('lblCedula','Cédula') !!}
{!! Form::text('Cedula',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblIDCaptacion ','No.Captación ') !!}
{!! Form::text('IDCaptacion',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblIDTipoDoc','Tipo Documento') !!}
{!! Form::text('IDTipoDoc',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblNombres','Nombres') !!}
{!! Form::text('Nombres',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblApellidos','Apellidos') !!}
{!! Form::text('Apellidos',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblCargo','Cargo') !!}
{!! Form::text('Cargo',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblContrato','Contrato') !!}
{!! Form::text('Contrato',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblObservaciones','Observaciones') !!}
{!! Form::text('Observaciones',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblFecha','Fecha') !!}
{!! Form::text('Fecha',null, ['class' => 'form-control']) !!}
</div>

</br>
<div class="form group">
 {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>
