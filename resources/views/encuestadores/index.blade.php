@extends('layouts.dashboard')
@section('page_heading','Encuestadores')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <a class="btn btn-primary pull-right" href=" {{ route('encuestadores.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de encuestadores
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Cedula
                                        </th>                                        <th>
                                            No.captacion 
                                        </th>                                                                                <th>
                                            Nombres
                                        </th>                                        <th>
                                            Apellidos
                                        </th>                                        <th>
                                            Cargo
                                        </th>                                        <th>
                                            Contrato
                                        </th>                                        <th>
                                            Observaciones
                                        </th>                                        <th>
                                            Fecha
                                        </th>
                                </thead>
                                <tbody>
                                    @foreach($encuestadores as $encuestadores)
                                    <tr>
                                        <td>
                                            {{$encuestadores->Cedula}}
                                        </td>
                                        <td>
                                            {{$encuestadores->IDCaptacion }}
                                        </td>
                                       
                                        <td>
                                            {{$encuestadores->Nombres}}
                                        </td>
                                        <td>
                                            {{$encuestadores->Apellidos}}
                                        </td>
                                        <td>
                                            {{$encuestadores->Cargo}}
                                        </td>
                                        <td>
                                            {{$encuestadores->Contrato}}
                                        </td>
                                        <td>
                                            {{$encuestadores->Observaciones}}
                                        </td>
                                        <td>
                                            {{$encuestadores->Fecha}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('encuestadores.edit', $encuestadores->Cedula)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

