@extends('layouts.dashboard')
@section('page_heading','Editar encuestados')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('encuestados.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver encuestados
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar encuestado
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($encuestado,['route' => ['encuestados.update',$encuestado->Cedula], 'method' =>'PUT']) !!}
                  	  @include('encuestados.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
