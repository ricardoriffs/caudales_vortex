<div class="row">
    <div class="col-sm-12">
        <br>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de Encuestados
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Relación encuestado</th>
                                    <th>No. Documento</th>
                                    <th>Nombre Completo</th>
                                    <th>Celular</th>
                                </tr>    
                            </thead>
                            <tbody>
                                @foreach($encuestados as $encuestado)
                                <tr>
                                    <td>{{ $encuestado->Relacion }}</td>
                                    <td>{{ $encuestado->Cedula }}</td>
                                    <td>{{ $encuestado->Nombres.' '.$encuestado->Apellidos }}</td>
                                    <td>{{ $encuestado->Celular }}</td>
                                    <td>
                                        <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('encuestados.edit', $encuestado->Cedula)}}"> Editar</a>
                                    </td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                        @if($encuestados)                     
                            {!! $encuestados->render() !!}
                        @endif    
                    </div>
                </div>
            </div>
        </br>
    </div>
</div>