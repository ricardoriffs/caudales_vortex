<br/>
<div class="col-md-11">
    <fieldset>        
        <legend>Propietario</legend>
        <br>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblTipoDocumento','*Tipo Documento') !!}
                {!!  Form::select('IDTipoDoc',$tiposdocumento,null,['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblCedula','*No. Documento') !!}
                {!! Form::text('Cedula',null, ['class' => 'form-control','id'=>'documentopropietario']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblNombres','*Propietario') !!}
                {!! Form::text('Nombres',null, ['class' => 'form-control','id'=>'nombrepropietario']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblApellidos','*Apellidos') !!}
                {!! Form::text('Apellidos',null, ['class' => 'form-control', 'id'=>'apellidopropietario']) !!}
            </div>
        </div>
        <br>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblDireccionCorrespondencia','*Dirección correspondencia') !!}
                {!! Form::text('DireccionCorrespondencia',null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblTelefono','Teléfono') !!}
                {!! Form::number('Telefono',null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblCelular','*Celular') !!}
                {!! Form::number('Celular',null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblEmail','*Email') !!}
                {!! Form::text('Email',null, ['class' => 'form-control','Type'=> 'email']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblEmail','El encuestado es el mismo del propietario?') !!}
                <input type="radio" name="desicion" id="desicionS" value="S" onclick="desisionencuestados()">Si<br>
                <input type="radio" name="desicion" id="desicionN" checked="checked" value="N" onclick="desisionencuestados()">No<br>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblEmail','Tiene representante legal?') !!}<br>
                <input type="radio" name="desicion" id="desicionRLS" value="S" onclick="representantelegal()">Si<br>
                <input type="radio" name="desicion" id="desicionRLN" checked="checked" value="N" onclick="representantelegal()">No<br>
            </div>
        </div>
    </fieldset>
</div>
<br>
<div class="col-md-11" id="representantelegalform" style="display:none">
    <fieldset>        
        <legend>Representante legal</legend>
        <br>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblTipoDocumento','*Tipo Documento') !!}
                {!!  Form::select('IDTipoDoc',$tiposdocumento,null,['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblCedula','*No. Documento') !!}
                {!! Form::text('Cedula',null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblNombres','*Representante legal') !!}
                {!! Form::text('Nombres',null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </fieldset>
</div>
<br>
<div class="col-md-11">
    <fieldset>        
        <legend>Arrendatario</legend>
        <br>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblTipoDocumento','*Nombre') !!}
                {!! Form::text('Nombres',null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblCedula','*Tiempo tenencia (Años)') !!}
                {!! Form::text('Cedula',null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblNombres','*RepresentanteDireccion') !!}
                {!! Form::text('Nombres',null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </fieldset>
</div>
<br>
<div class="col-md-11">
    <fieldset>        
        <legend>Encuestado</legend>
        <br>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblTipoDocumento','*Nombre') !!}
                {!! Form::text('Nombres',null, ['class' => 'form-control','id'=>'nombreencuestado']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form group">
                {!! Form::label('lblCedula','*No. Documento') !!}
                {!! Form::text('Cedula',null, ['class' => 'form-control','id' => 'documentoencuestado']) !!}
            </div>
        </div>
    </fieldset>
</div>
<br />
<div class="row">      
    <div class="col-md-4" style="margin-top: 30px;margin-left: 25px;">
        <div class="form group">
            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>    
</div>
@if(isset($encuestados))
@include('encuestados.partials.list')
@endif