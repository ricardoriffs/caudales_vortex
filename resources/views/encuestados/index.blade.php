@extends('layouts.dashboard')
@section('page_heading','encuestados')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('encuestados.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de encuestados
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                          No. Captacion
                                        </th>                                        <th>
                                            Cédula
                                        </th>                                         <th>
                                            Nombres
                                        </th>                                        <th>
                                            Apellidos
                                        </th>                                      <th>
                                            Telefono
                                        </th>                                        <th>
                                            Celular
                                        </th>                                        <th>
                                            Email
                                        </th>
                                </thead>
                                <tbody>
                                    @foreach($encuestados as $encuestado)
                                    <tr>
                                        <td>
                                            {{$encuestado->IDCaptacion}}
                                        </td>
                                        <td>
                                            {{$encuestado->Cedula}}
                                        </td>
                                       
                                        <td>
                                            {{$encuestado->Nombres}}
                                        </td>
                                        <td>
                                            {{$encuestado->Apellidos}}
                                        </td>
                                       
                                        <td>
                                            {{$encuestado->Telefono}}
                                        </td>
                                        <td>
                                            {{$encuestado->Celular}}
                                        </td>
                                        <td>
                                            {{$encuestado->Email}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('encuestados.edit', $encuestado->Cedula)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $encuestados->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

