<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/logo_caudales.ico') }}" />
    <title>{{ config('app.name', 'Caudales') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <link href="{{ asset('css/caudales.css') }}" rel="stylesheet">
</head>
<body class="login-background">
   
       




<div class="container login-container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-caudales panel panel-primary">
                <div class="panel-heading">Iniciar sesion</div>
                <div class="panel-body">
<center>
                      <img src="{{ asset('images/logo_caudales.png') }}" class="img-circle"  width="120" height="120">
                <p>Distribución de Caudales</p>
                </center>

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4">Usuario</label>

                            <div class="col-md-12">
                                <input id="email"  class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4">Contraseña</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-lg btn-success btn-block" height: auto; style="height:42px;">
                                    Ingresar
                                </button>

                                
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    
 <div class="login-emblema">
      <img src="{{ asset('images/logocar.png') }}" class="img-rounded"  width="204" height="136">
    </div>
    
<!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

