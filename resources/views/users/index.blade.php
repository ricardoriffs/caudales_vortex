@extends('layouts.dashboard')
@section('page_heading','Usuarios')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a class="btn btn-primary pull-right" href=" {{ route('users.create') }}">
                <i class="fa fa-plus-circle" ></i> Nuevo
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de usuarios
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                       
                                        <th>
                                            Dependencia
                                        </th>
                                        <th>
                                            Estado
                                        </th>
                                        <th>
                                            Perfil
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>
                                            {{$user->name}}
                                        </td>
                                        <td>
                                            {{$user->email}}
                                        </td>
                                       
                                        <td>
                                            {{$user->IdDependencia}}
                                        </td>
                                        <td>
                                            {{$user->Estado}}
                                        </td>
                                        <td>
                                            {{$user->IdPerfil}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('users.edit', $user->id)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $users->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

