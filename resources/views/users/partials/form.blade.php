<div class="form group">
{!! Form::label('lblname','Nombre') !!}
{!! Form::text('name',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblemail','Email') !!}
  {!! Form::text('email',null, ['class' => 'form-control','Type'=> 'email']) !!}
</div>
<div class="form group">
{!! Form::label('lblpassword','Password') !!}
{!! Form::password('password', ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblIdDependencia','Dependencia') !!}
{!! Form::text('IdDependencia',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblEstado','Estado') !!}
{!! Form::text('Estado',null, ['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblIdPerfil','Perfil') !!}
{!! Form::text('IdPerfil',null, ['class' => 'form-control']) !!}
</div>

</br>
<div class="form group">
 {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>
