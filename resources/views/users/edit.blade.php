@extends('layouts.dashboard')
@section('page_heading','Editar users')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('users.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver usuarios
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar usuario
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($user,['route' => ['users.update',$user->id], 'method' =>'PUT']) !!}
                  	  @include('users.partials.editform')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
