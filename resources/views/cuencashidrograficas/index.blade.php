@extends('layouts.dashboard')
@section('page_heading','Cuencas Hidrograficas')
@section('section')
<div class="container">
    <div class="row">
       
        <div class="col-sm-8 col-sm-offset-2">
            <br>


            <div class="panel panel-default">
                    <div class="panel-heading">
                       Ingresar cuenca hidrografica
                    </div>
                    <div class="panel-body">
                     {!!Form::open(['route' => 'cuencashidrograficas.store'])!!}

                         <div class="form group">
                        {!! Form::label('lblIDCuenca','IDCuenca') !!}
                        {!! Form::text('IDCuenca',null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form group">
                        {!! Form::label('lblNombre','Nombre') !!}
                        {!! Form::text('Nombre',null, ['class' => 'form-control']) !!}
                        </div>

                        </br>
                        <div class="form group">
                        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                        </div>               

                      {!! Form::close()!!}
                    </div>
            </div>
                   
                   @include('widgets.info')
                    @include('widgets.error')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de cuencas hidrograficas
                    </div>
                    <div class="panel-body">
                  
                        <div class="col-sm-12">

               

                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th with="20px">
                                            IDCuenca
                                        </th>
                                        <th>
                                          Nombre
                                        </th>
                                       
                                    </tr>
                                </thead>
                                <tbody>                                    
                                    @foreach($CuencasHidrograficas as $CuencaHidrografica)
                                    <tr>
                                        <td>
                                            {{$CuencaHidrografica->idcuenca}}
                                        </td>
                                        <td>
                                            {{$CuencaHidrografica->nombre}}
                                        </td> 
                                         </tr>
                                    @endforeach                                 
                                </tbody>
                            </table>
                         {!!  $CuencasHidrograficas->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>    
 @stop
