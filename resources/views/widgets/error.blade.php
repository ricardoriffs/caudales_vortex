@if(count($errors))
<div class="alert alert-danger  alert-dismissable " role="alert">
 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
  

   @foreach($errors -> all() as $error)
		<li>{{ $error }}</li>
   @endforeach

</div>
@endif