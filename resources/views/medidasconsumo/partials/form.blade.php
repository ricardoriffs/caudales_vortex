
<div class="form group">
{!! Form::label('lblTipoMedida','Tipomedida') !!}
{!!  Form::select('TipoMedida', ['LINEA' => 'Linea', 'UNIDAD' => 'Unidad'],  'S', ['class' => 'form-control' ]) !!}
</div>
<div class="form group">
{!! Form::label('lblIDTipoUsoCategorias','Categoria Tipo Uso') !!}
{!! Form::select('IDTipoUsosCategoria',$tiposusoscategorias,null,['class' => 'form-control']) !!}
</div>
<div class="form group">
{!! Form::label('lblNombre','Nombre') !!}
{!! Form::text('Nombre',null, ['class' => 'form-control']) !!}
</div>

</br>
<div class="form group">
 {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>
