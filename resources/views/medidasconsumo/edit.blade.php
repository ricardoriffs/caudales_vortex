@extends('layouts.dashboard')
@section('page_heading','Editar MedidasConsumo')
@section('section')
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-8 col-sm-offset-2"> 
            <a class="btn btn-primary pull-left" href =" {{ route('medidasconsumo.index') }}">
                <i class="fa fa-backward"> 
                </i>
                Ver MedidasConsumo
            </a>
        </div>
        <div class="col-sm-8 col-sm-offset-2" > 
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading" > 
                        Modificar medidaconsumo
                   </div>
                    <div class="panel-body">
                        @include('widgets.info')
                        @include('widgets.error')
                      {!!Form::model($medidaconsumo,['route' => ['medidasconsumo.update',$medidaconsumo->IDMedidaConsumo], 'method' =>'PUT']) !!}
                  	  @include('medidasconsumo.partials.form')
                      {!! Form::close()!!}
                    </div>
                </div>
            </br>
        </div>
    </div>
</div>
@stop
