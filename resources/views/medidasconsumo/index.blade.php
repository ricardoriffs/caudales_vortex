@extends('layouts.dashboard')
@section('page_heading','Medidas de Consumo')
@section('section')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a class="btn btn-primary pull-right" href=" {{ route('medidasconsumo.create') }}">
                <i class="fa fa-plus-circle" ></i> Nueva
            </a>
        </div>
        <div class="col-sm-12">
            <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de medidasconsumo
                    </div>
                    <div class="panel-body">
                   @include('widgets.info')
                   @include('widgets.error')
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            Tipo Medida
                                        </th>
                                        <th>
                                           Categoria Tipo Uso 
                                        </th>
                                         <th>
                                          Módulo
                                        </th>
                                        <th>
                                            Nombre
                                        </th>

                                </thead>
                                <tbody>
                                    @foreach($medidasconsumo as $medidaconsumo)
                                    <tr>
                                        <td>
                                            {{$medidaconsumo->tipomedida}}
                                        </td>
                                        <td>
                                            {{$medidaconsumo->nombrecategoria}}
                                        </td>
                                         <td>
                                            {{$medidaconsumo->descripcion}}
                                        </td>
                                        <td>
                                            {{$medidaconsumo->nombre}}
                                        </td>

                                        <td>
                                       <a class="btn btn-primary btn-outline btn-sm pull-right"  href="{{ route('medidasconsumo.edit', $medidaconsumo->idmedidaconsumo)}}"> Editar </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!!  $medidasconsumo->render() !!}
                        </div>
                    </div>
                </div>
            </br>
        </div>
        </div>
    </div>  
 @stop

