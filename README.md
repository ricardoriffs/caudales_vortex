# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Cargar Docker base de datos oracle-xe-11g ###

docker pull ricardoriffs/oracle-xe-11g

docker run -d -p 49160:22 -p 49161:1521 -e ORACLE_ALLOW_REMOTE=true --name oracleserver ricardoriffs/oracle-xe-11g

docker exec -it oracleserver bash

docker inspect oracleserver | grep IPAddress

# conectar con oracle via linea de comandos: #

su - oracle
sqlplus 'system/oracle as sysdba'


### Parametros de conexion ###

DB_HOST=localhost

DB_PORT=49161

DB_DATABASE=xe

DB_USERNAME=DBCAUDALES

DB_PASSWORD=********