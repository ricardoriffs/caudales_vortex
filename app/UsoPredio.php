<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class UsoPredio extends Model
{
public $timestamps = false;
protected $table = 'UsosPredios';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='IDUsoPredio';
protected $fillable = [
'IDUsoPredio','IDCaptacion','IDModulo','IDTipoUsosCategoria','IDTipoUso','Cantidad','TotalConsumo'
];
}
 
 
