<?php

namespace caudales;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CuencaHidrografica extends Model
{
    public $timestamps = false;
	protected $table = 'CuencaHidrog';// se define la tabla a utilizar
	protected $perPage = 15; // se define la cantidad de filas en la grilla
 	protected $primaryKey='IDCuenca';
     protected $fillable = [
        'IDCuenca','Nombre'
    ];
     
    public static function obtenerCuencas(){
        $regionales = DB::table('cuencahidrog')   
                ->select('idcuenca', 'nombre')
                ->get();
        
        return $regionales;
    }     
}
