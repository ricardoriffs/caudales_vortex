<?php

namespace caudales;

use Illuminate\Database\Eloquent\Model;

class Parametro extends Model
{
	
	protected $primaryKey='IDParametro';
     protected $fillable = [
        'IDParametro','Parametro', 'Modulo', 'Observaciones'
    ];
}
