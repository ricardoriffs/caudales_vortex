<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Log extends Model
{
public $timestamps = false;
protected $table = 'Logs';// se define la tabla a utilizar
protected $perPage = 15; // se define la cantidad de filas en la grilla
protected $primaryKey='IDLog';
protected $fillable = [
'IDLog','Usuario','Detalle','Accion','Fecha'
];
}
 
 
