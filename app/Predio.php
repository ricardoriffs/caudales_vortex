<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Predio extends Model
{
public $timestamps = false;
protected $table = 'Predios';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='CedulaCatastral';
protected $fillable = [
    'CedulaCatastral',
    'IDCaptacion',
    'IDDepartamento',
    'IDMunicipio',
    'IDVereda',
    'NombrePredio',
    'DireccionPredio',
    'Matricula',
    'CoordX',
    'CoordY',
    'Altura',
    'AreaTotal',
    'NombreImagen'
];
}
 
 
