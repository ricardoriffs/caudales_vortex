<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Departamento extends Model
{
public $timestamps = false;
protected $table = 'Departamentos';// se define la tabla a utilizar
protected $perPage = 15; // se define la cantidad de filas en la grilla
protected $primaryKey='IDDepartamento';
protected $fillable = [
'IDDepartamento','Nombre'
];
}
 
 
