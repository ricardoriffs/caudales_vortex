<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Municipio extends Model
{
public $timestamps = false;
protected $table = 'Municipios';// se define la tabla a utilizar
protected $perPage = 20; // se define la cantidad de filas en la grilla
protected $primaryKey='IDMunicipio';
protected $fillable = [
'IDMunicipio','IDDepartamento','Nombre'
];
}
 
 
