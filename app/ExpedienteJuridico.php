<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class ExpedienteJuridico extends Model
{
public $timestamps = false;
protected $table = 'ExpedientesJuridicos';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='IDExpediente';
protected $fillable = [
'IDExpediente','IDCaptacion','Concesion','ExpedienteNo','ExpedienteFecha','ResolucionNo','ResolucionFecha','VigenciaDesde','VigenciaHasta','CaudalConcesionadoLPS','CaudalUtilizadoLPS','FechaObra','OficinaProvincial','CaudalUsoDomestico','CaudalUsoPecuario','CaudalUsoAgricola','Observaciones','IDExpediente','AprovacionDiseno'
];
}
 
 
