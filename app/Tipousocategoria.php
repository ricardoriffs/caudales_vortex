<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class TipoUsoCategoria extends Model
{
public $timestamps = false;
protected $table = 'tipousoscategorias';// se define la tabla a utilizar
protected $perPage = 10; // se define la cantidad de filas en la grilla
protected $primaryKey='idtipousoscategoria';
protected $fillable = [
'idtipousoscategoria','nombrecategoria','idmodulo'
];
}
 
 
