<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Encuestado extends Model
{
public $timestamps = false;
protected $table = 'Encuestados';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='Cedula';
protected $fillable = [
'Cedula','IDCaptacion','IDTipoDoc','Nombres','Apellidos','Direccion','DireccionCorrespondencia','Telefono','Celular','Email','Relacion'
];
}
 
 
