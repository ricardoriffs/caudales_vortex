<?php

namespace caudales\Http\Controllers;

use Illuminate\Http\Request;
use caudales\CuencaHidrografica;
use caudales\Http\Requests\CuencaHidrograficaRequest;
use caudales\Log;
use Auth;
use Carbon\Carbon;

class CuencaHidrograficaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $CuencasHidrograficas = CuencaHidrografica::orderBy('IDCuenca', 'DESC')->paginate();
        return view('cuencashidrograficas.index', compact('CuencasHidrograficas'));

    }

      public function store(CuencaHidrograficaRequest $request)
    {
        $CuencaHidrografica = new CuencaHidrografica;
        $CuencaHidrografica->IDCuenca = $request->IDCuenca; 
        $CuencaHidrografica->Nombre = $request->Nombre;     
        $CuencaHidrografica->save();

        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle=  $nombreusuario.' creo la Cuenca hidrográfica No. '.$request->IDCuenca;
        $log->Accion= 'Crear';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();        

     return redirect()->route('cuencashidrograficas.index') -> with('info','La cuenca hidrografica ha sido registrada');
    }
}
