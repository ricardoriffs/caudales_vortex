<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\PisoTermico;
use caudales\Http\Requests\PisoTermicoRequest;
 
class PisoTermicoController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $pisostermicos=PisoTermico::orderBy('IDPisoTermico', 'DESC')->paginate();
 
        return view('pisostermicos.index',compact('pisostermicos'));
    }
 
    public function create()
    {
 
        return view('pisostermicos.create');
    }
 
 
    public function store(PisoTermicoRequest $request)
    {
 
      $pisotermico = new PisoTermico;
 
      $pisotermico->PisoTermico= $request->PisoTermico;
      $pisotermico->Descripcion= $request->Descripcion;
      $pisotermico->Desde= $request->Desde;
      $pisotermico->Hasta= $request->Hasta;

      $pisotermico->save();
 
      return redirect()->route('pisostermicos.create') -> with('info','El pisotermico fue registrado');
    }
 
 
    public function edit($id)
    {
        $pisotermico = PisoTermico::find($id);
        return view('pisostermicos.edit', compact('pisotermico'));
    } 
   public function update(PisoTermicoRequest $request,$id)
   {
        $pisotermico = PisoTermico::find($id);
        $pisotermico->PisoTermico= $request->PisoTermico;
        $pisotermico->Descripcion= $request->Descripcion;
        $pisotermico->Desde= $request->Desde;
        $pisotermico->Hasta= $request->Hasta;

        $pisotermico->save();
        return redirect()->route('pisostermicos.index') -> with('info','El pisotermico fue actualizado');
 
    }
}
