<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\ExpedienteJuridico;
use caudales\Http\Requests\ExpedienteJuridicoRequest;
 
class ExpedienteJuridicoController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $expedientesjuridicos=ExpedienteJuridico::orderBy('IDExpediente', 'DESC')->paginate();
 
        return view('expedientesjuridicos.index',compact('expedientesjuridicos'));
    }
 
    public function create()
    {
 
        return view('expedientesjuridicos.create');
    }
 
 
    public function store(ExpedienteJuridicoRequest $request)
    {
 
      $expedientejuridico = new ExpedienteJuridico;
 
     
      $expedientejuridico->Concesion= $request->Concesion;
      $expedientejuridico->ExpedienteNo= $request->ExpedienteNo;
      $expedientejuridico->ExpedienteFecha= $request->ExpedienteFecha;
      $expedientejuridico->ResolucionNo= $request->ResolucionNo;
      $expedientejuridico->ResolucionFecha= $request->ResolucionFecha;
      $expedientejuridico->VigenciaDesde= $request->VigenciaDesde;
      $expedientejuridico->VigenciaHasta= $request->VigenciaHasta;
      $expedientejuridico->CaudalConcesionadoLPS= $request->CaudalConcesionadoLPS;
      $expedientejuridico->CaudalUtilizadoLPS= $request->CaudalUtilizadoLPS;
      $expedientejuridico->FechaObra= $request->FechaObra;
      $expedientejuridico->OficinaProvincial= $request->OficinaProvincial;
      $expedientejuridico->CaudalUsoDomestico= $request->CaudalUsoDomestico;
      $expedientejuridico->CaudalUsoPecuario= $request->CaudalUsoPecuario;
      $expedientejuridico->CaudalUsoAgricola= $request->CaudalUsoAgricola;
      $expedientejuridico->Observaciones= $request->Observaciones;
      $expedientejuridico->AprovacionDiseno= $request->AprovacionDiseno;
   


   if ($request->TipoFormulario == "individual")
     {

           $expedientejuridico->IDCaptacion= $request->IDCaptacion;

          $expedientejuridico->save();
     
         return redirect()->route('expedientesjuridicos.create') -> with('info','El expedientejuridico fue registrado');

     }else
     {


         if ($request->TipoFormulario == "tabular")
         {
               $SessionIDCaptacion = session('No_Captacion');


                if ($request->session()->has('No_Captacion')) {
                  $expedientejuridico->IDCaptacion= $SessionIDCaptacion;         
                  $expedientejuridico->save();  
                 return redirect()->route('captaciones.create') -> with('info','La información juridica fue registrada');
                
    
                }
                else{


                  $errors = $validator->messages()->all();                 
                    return redirect()->route('captaciones.create') ->withInput()->withErrors('Por favor registre primero una captación.');
                }
                
         }
    }




      
      

    }
 
 
    public function edit($id)
    {
        $expedientejuridico = ExpedienteJuridico::find($id);
        return view('expedientesjuridicos.edit', compact('expedientejuridico'));
    } 
   public function update(ExpedienteJuridicoRequest $request,$id)
   {
        $expedientejuridico = ExpedienteJuridico::find($id);
        $expedientejuridico->IDCaptacion= $request->IDCaptacion;
        $expedientejuridico->Concesion= $request->Concesion;
        $expedientejuridico->ExpedienteNo= $request->ExpedienteNo;
        $expedientejuridico->ExpedienteFecha= $request->ExpedienteFecha;
        $expedientejuridico->ResolucionNo= $request->ResolucionNo;
        $expedientejuridico->ResolucionFecha= $request->ResolucionFecha;
        $expedientejuridico->VigenciaDesde= $request->VigenciaDesde;
        $expedientejuridico->VigenciaHasta= $request->VigenciaHasta;
        $expedientejuridico->CaudalConcesionadoLPS= $request->CaudalConcesionadoLPS;
        $expedientejuridico->CaudalUtilizadoLPS= $request->CaudalUtilizadoLPS;
        $expedientejuridico->FechaObra= $request->FechaObra;
        $expedientejuridico->OficinaProvincial= $request->OficinaProvincial;
        $expedientejuridico->CaudalUsoDomestico= $request->CaudalUsoDomestico;
        $expedientejuridico->CaudalUsoPecuario= $request->CaudalUsoPecuario;
        $expedientejuridico->CaudalUsoAgricola= $request->CaudalUsoAgricola;
        $expedientejuridico->Observaciones= $request->Observaciones;
        $expedientejuridico->AprovacionDiseno= $request->AprovacionDiseno;

        $expedientejuridico->save();
        return redirect()->route('expedientesjuridicos.index') -> with('info','El expedientejuridico fue actualizado');
 
    }
}
