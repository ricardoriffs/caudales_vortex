<?php

namespace caudales\Http\Controllers;

use Illuminate\Http\Request;
use caudales\TipoFuente;
use caudales\Http\Requests\TipoFuenteRequest;
use caudales\Log;
use Auth;
use Carbon\Carbon;

class TipoFuentesController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tipofuentes = TipoFuente::orderBy('idfuente', 'DESC')->paginate();
        return view('tipofuentes.index', compact('tipofuentes'));

    }

    public function store(TipoFuenteRequest $request)
    {

        $tipofuente = new TipoFuente;
        $tipofuente->nombre = $request->Nombre;     
        $id_sig = TipoFuente::ultimoId();
        $tipofuente->idfuente = $id_sig;
        if($tipofuente->save()){
            $log = new Log;
            $idusuario = Auth::user()->id;
            $nombreusuario =Auth::user()->name;
            $log->Usuario= $idusuario;
            $log->Detalle=  $nombreusuario.' creo el Tipo Fuente No. '.$tipofuente->idfuente;
            $log->Accion= 'Crear';
            $log->Fecha= Carbon::now()->toDateTimeString();
            $log->save();            
            return redirect()->route('tipofuentes.index') -> with('info','El tipo de fuente fue registrado');
        } else {
            return redirect()->route('tipofuentes.index') -> with('info','El tipo de fuente NO fue registrado.');
        }
        
       
        
    }
}

