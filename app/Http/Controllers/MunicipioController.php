<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Municipio;
use caudales\Http\Requests\MunicipioRequest;
use caudales\Log;
use Auth;
use Carbon\Carbon; 

class MunicipioController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $municipios=Municipio::orderBy('IDMunicipio', 'DESC')->paginate();
 
        return view('municipios.index',compact('municipios'));
    }
 
    public function create()
    {
 
        return view('municipios.create');
    }
 
 
    public function store(MunicipioRequest $request)
    {
 
        $municipio = new Municipio;

        $municipio->IDMunicipio= $request->IDMunicipio;
        $municipio->IDDepartamento= $request->IDDepartamento;
        $municipio->Nombre= $request->Nombre;
        $municipio->save();
      
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle= $nombreusuario.' creo el Municipio No. '.$request->IDMunicipio;
        $log->Accion= 'Crear';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();          
 
      return redirect()->route('municipios.create') -> with('info','El municipio fue registrado');
    }
 
 
    public function edit($id)
    {
        $municipio = Municipio::find($id);
        return view('municipios.edit', compact('municipio'));
    } 
   public function update(MunicipioRequest $request,$id)
   {
        $municipio = Municipio::find($id);
        $municipio->IDDepartamento= $request->IDDepartamento;
        $municipio->Nombre= $request->Nombre;

        $municipio->save();
        
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle= $nombreusuario.' modificó el Municipio No. '.$request->IDMunicipio;
        $log->Accion= 'Modificar';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();         
        
        return redirect()->route('municipios.index') -> with('info','El municipio fue actualizado');
 
    }





    // #################### LLAMADAS AJAX ####################
    public function consultarMunicipios($id)
    {
       $municipios = Municipio::where('iddepartamento', '=', $id)->get();     
    
       return json_encode($municipios->pluck('nombre','idmunicipio'));
     }

}
