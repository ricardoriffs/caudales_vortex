<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\user;
use caudales\Http\Requests\userRequest;
 
class userController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $users=user::orderBy('id', 'DESC')->paginate();
 
        return view('users.index',compact('users'));
    }
 
    public function create()
    {
 
        return view('users.create');
    }
 
 
    public function store(userRequest $request)
    {
 
      $user = new user;
 
      $user->name= $request->name;
      $user->email= $request->email;
      $user->password= bcrypt($request->password);
      $user->IdDependencia= $request->IdDependencia;
      $user->Estado= $request->Estado;
      $user->IdPerfil= $request->IdPerfil;

      $user->save();
 
      return redirect()->route('users.create') -> with('info','El user fue registrado');
    }
 
 
    public function edit($id)
    {
        $user = user::find($id);
        return view('users.edit', compact('user'));
    } 
   public function update(userRequest $request,$id)
   {
        $user = user::find($id);
        $user->name= $request->name;
        $user->email= $request->email;
        $user->IdDependencia= $request->IdDependencia;
        $user->Estado= $request->Estado;
        $user->IdPerfil= $request->IdPerfil;

        $user->save();
        return redirect()->route('users.index') -> with('info','El user fue actualizado');
 
    }
}
