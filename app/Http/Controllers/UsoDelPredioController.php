<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\UsoDelPredio;
use caudales\Http\Requests\UsoDelPredioRequest;
use caudales\Modulo;
 
class UsoDelPredioController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $usosdelpredio=UsoDelPredio::orderBy('iduso', 'DESC')->paginate();
 
        return view('usosdelpredio.index',compact('usosdelpredio'));
    }
 
    public function create()
    {
       $modulos = Modulo::pluck('Descripcion', 'IDModulo')->toArray();
       return view('usosdelpredio.create',compact('modulos', $modulos));
    }
 
 
    public function store(UsoDelPredioRequest $request)
    {
 
      $usodelpredio = new UsoDelPredio;
 
      $usodelpredio->iduso= $request->iduso;
      $usodelpredio->campo1= $request->campo1;

      $usodelpredio->save();
 
      return redirect()->route('usosdelpredio.create') -> with('info','El usodelpredio fue registrado');
    }
 
 
    public function edit($id)
    {
        $usodelpredio = UsoDelPredio::find($id);
        return view('usosdelpredio.edit', compact('usodelpredio'));
    } 
   public function update(UsoDelPredioRequest $request,$id)
   {
        $usodelpredio = UsoDelPredio::find($id);
        $usodelpredio->campo1= $request->campo1;

        $usodelpredio->save();
        return redirect()->route('usosdelpredio.index') -> with('info','El usodelpredio fue actualizado');
 
    }
}
