<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Departamento;
use caudales\Http\Requests\DepartamentoRequest;
use caudales\Log;
use Auth;
use Carbon\Carbon;

class DepartamentoController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $departamentos=Departamento::orderBy('Nombre', 'DESC')->paginate();
 
        return view('departamentos.index',compact('departamentos'));
    }
 
    public function create()
    {
 
        return view('departamentos.create');
    }
 
 
    public function store(DepartamentoRequest $request)
    {
 
        $departamento = new Departamento;

        $departamento->IDDepartamento= $request->IDDepartamento;
        $departamento->Nombre= $request->Nombre;
        $departamento->save();
      
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle= $nombreusuario.' creo el Departamento No. '.$request->IDDepartamento;
        $log->Accion= 'Crear';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();            
 
      return redirect()->route('departamentos.create') -> with('info','El departamento fue registrado');
    }
 
 
    public function edit($id)
    {
        $departamento = Departamento::find($id);
        return view('departamentos.edit', compact('departamento'));
    } 
   public function update(DepartamentoRequest $request,$id)
   {
        $departamento = Departamento::find($id);
        $departamento->Nombre= $request->Nombre;

        $departamento->save();
        
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle= $nombreusuario.' modificó la Departamento No. '.$request->IDDepartamento;
        $log->Accion= 'Modificar';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();           
        
        return redirect()->route('departamentos.index') -> with('info','El departamento fue actualizado');
 
    }
}
