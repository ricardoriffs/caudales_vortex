<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Modulo;
use caudales\Http\Requests\ModuloRequest;
 
class ModuloController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $modulos=Modulo::orderBy('IDModulo', 'DESC')->paginate();
 
        return view('modulos.index',compact('modulos'));
    }
 
    public function create()
    {
 
        return view('modulos.create');
    }
 
 
    public function store(ModuloRequest $request)
    {
 
      $modulo = new Modulo;
 
      $modulo->Descripcion= $request->Descripcion;

      $modulo->save();
 
      return redirect()->route('modulos.create') -> with('info','El modulo fue registrado');
    }
 
 
    public function edit($id)
    {
        $modulo = Modulo::find($id);
        return view('modulos.edit', compact('modulo'));
    } 
   public function update(ModuloRequest $request,$id)
   {
        $modulo = Modulo::find($id);
        $modulo->Descripcion= $request->Descripcion;

        $modulo->save();
        return redirect()->route('modulos.index') -> with('info','El modulo fue actualizado');
 
    }
}
