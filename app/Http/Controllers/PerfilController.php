<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Perfil;
use caudales\Http\Requests\PerfilRequest;
 
class PerfilController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $perfiles=Perfil::orderBy('IdPerfil', 'DESC')->paginate();
 
        return view('perfiles.index',compact('perfiles'));
    }
 
    public function create()
    {
 
        return view('perfiles.create');
    }
 
 
    public function store(PerfilRequest $request)
    {
 
      $perfil = new Perfil;
 
      $perfil->Perfil= $request->Perfil;

      $perfil->save();
 
      return redirect()->route('perfiles.create') -> with('info','El perfil fue registrado');
    }
 
 
    public function edit($id)
    {
        $perfil = Perfil::find($id);
        return view('perfiles.edit', compact('perfil'));
    } 
   public function update(PerfilRequest $request,$id)
   {
        $perfil = Perfil::find($id);
        $perfil->Perfil= $request->Perfil;

        $perfil->save();
        return redirect()->route('perfiles.index') -> with('info','El perfil fue actualizado');
 
    }
}
