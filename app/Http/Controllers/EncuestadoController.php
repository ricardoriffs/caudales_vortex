<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Encuestado;
use caudales\Http\Requests\EncuestadoRequest;
use Validator, Input, Redirect; 
 
class EncuestadoController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $encuestados=Encuestado::orderBy('Cedula', 'DESC')->paginate();
 
        return view('encuestados.index',compact('encuestados'));
    }
 
    public function create()
    {
      $relacionesencuestados = array('Arrendatario' => 'Arrendatario', 'Copropietario' => 'Copropietario', 'Infractor' => 'Infractor','Poseedor'=>'Poseedor','Propietario'=>'Propietario','Otro'=>'Otro');
        $tiposdocumento =array('CC' =>'Cédula de Extranjería','CE' =>'Cédula de Ciudadanía');

        return view('encuestados.create',compact('relacionesencuestados',$relacionesencuestados), ['tiposdocumento'=>$tiposdocumento]);
       
    }
 
 
    public function store(EncuestadoRequest $request)
    {

      $encuestado = new Encuestado;      

      $encuestado->Cedula= $request->Cedula;
      $encuestado->IDTipoDoc= $request->IDTipoDoc;
      $encuestado->Nombres= $request->Nombres;
      $encuestado->Apellidos= $request->Apellidos;
      $encuestado->Direccion= $request->Direccion;
      $encuestado->DireccionCorrespondencia= $request->DireccionCorrespondencia;
      $encuestado->Telefono= $request->Telefono;
      $encuestado->Celular= $request->Celular;
      $encuestado->Email= $request->Email;
      $encuestado->Relacion = $request->RelacionEncuestado;

      if ($request->TipoFormulario == "individual")
     {

          $encuestado->IDCaptacion= $request->IDCaptacion;

          $encuestado->save();
     
          return redirect()->route('encuestados.create') -> with('info','El encuestado fue registrado');

     } elseif ($request->TipoFormulario == "tabular")
         {
               $SessionIDCaptacion = session('No_Captacion');

              $validator = Validator::make(array('IDCaptacion' => $SessionIDCaptacion), array('IDCaptacion' => 'required'));

                if($validator->fails() or !$request->session()->has('No_Captacion')){
                    $errors = $validator->messages()->all();                 
                    return redirect()->route('captaciones.create') ->withInput()->withErrors('Por favor registre primero una captación, o pulse el botón: agregar encuestado a otra captación'.$errors); 
                }
                else{
                  $encuestado->IDCaptacion= $SessionIDCaptacion;                       
                  $encuestado->save();     
                  return redirect()->route('captaciones.create') -> with('info','El encuestado fue registrado');
                }               
         }
    }
 
 
    public function edit($id)
    {
        $encuestado = Encuestado::find($id);
        $relacionesencuestados = array('Arrendatario' => 'Arrendatario', 'Copropietario' => 'Copropietario', 'Infractor' => 'Infractor','Poseedor'=>'Poseedor','Propietario'=>'Propietario','Otro'=>'Otro');
        $tiposdocumento =array('CC' =>'Cédula de Extranjería','CE' =>'Cédula de Ciudadanía');
        
        return view('encuestados.edit', compact('encuestado'),['relacionesencuestados'=>$relacionesencuestados,  'tiposdocumento'=>$tiposdocumento]);
    } 
    
   public function update(EncuestadoRequest $request,$id)
   {
        $encuestado = Encuestado::find($id);
        $encuestado->IDCaptacion= $request->IDCaptacion;
        $encuestado->IDTipoDoc= $request->IDTipoDoc;
        $encuestado->Nombres= $request->Nombres;
        $encuestado->Apellidos= $request->Apellidos;
        $encuestado->Direccion= $request->Direccion;
        $encuestado->DireccionCorrespondencia= $request->DireccionCorrespondencia;
        $encuestado->Telefono= $request->Telefono;
        $encuestado->Celular= $request->Celular;
        $encuestado->Email= $request->Email;

        $encuestado->save();
        return redirect()->route('encuestados.index') -> with('info','El encuestado fue actualizado');
 
    }


  



}
