<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\TipoUso;
use caudales\Http\Requests\TipoUsoRequest;
use caudales\PisoTermico;
use caudales\TipoUsoCategoria;
use caudales\MedidaConsumo;
 
class TipoUsoController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
      


        $tipousos = \DB::table('tipousos')
            ->join('tipousoscategorias', 'tipousos.IDTipoUsosCategorias', '=', 'tipousoscategorias.IDTipoUsosCategoria')
            ->join('modulos', 'modulos.IDModulo', '=', 'tipousoscategorias.IDModulo')
            ->join('PisosTermicos', 'PisosTermicos.IDPisoTermico', '=', 'TipoUsos.IDPisoTermico')
            ->join('MedidasConsumo', 'MedidasConsumo.IDMedidaConsumo', '=', 'TipoUsos.Linea')
            ->join('MedidasConsumo as Unidad', 'Unidad.IDMedidaConsumo', '=', 'TipoUsos.Unidad')
            ->orderBy('tipousos.IDTipoUso', 'DESC')
            ->select('medidasconsumo.*', 'tipousos.IDTipoUso','tipousos.Nombre as NombreTU','PisosTermicos.Descripcion as DescripcionPT','PisosTermicos.PisoTermico','tipousos.LitrosDiaPersona','tipousoscategorias.NombreCategoria','Modulos.Descripcion','Unidad.Nombre as Unidad')
            ->paginate();
       
 

        return view('tipousos.index',compact('tipousos',$tipousos));

     
    }
 
    public function create()
    {
     
     $lineas =  MedidaConsumo::where([ ['TipoMedida', '=', 'LINEA']])->get(); 
     $unidades =  MedidaConsumo::where([ ['TipoMedida', '=', 'UNIDAD']])->get();

     $tiposusoscategorias = collect(\DB::select("SELECT CONCAT (tipousoscategorias.NombreCategoria, '  -  ', Modulos.Descripcion ) AS CategoriaTipoUso,tipousoscategorias.IDTipoUsosCategoria FROM tipousoscategorias inner join modulos on modulos.IDModulo = tipousoscategorias.IDModulo"))
     ->pluck('CategoriaTipoUso', 'IDTipoUsosCategoria');   


     $pisostermicos = PisoTermico::orderBy('IDPisoTermico', 'DESC')->get()->pluck('full_name', 'IDPisoTermico');   
     return view('tipousos.create',compact('pisostermicos',$pisostermicos),['tiposusoscategorias' => $tiposusoscategorias,'lineas'=>$lineas,'unidades'=>$unidades]);
     


    }
 
 
    public function store(TipoUsoRequest $request)
    {
 
      $tipouso = new TipoUso;
 
     
      $tipouso->Nombre= $request->Nombre;
      $tipouso->IDPisoTermico= $request->IDPisoTermico;
      $tipouso->LitrosDiaPersona= $request->LitrosDiaPersona;
      $tipouso->IDTipoUsosCategorias= $request->IDTipoUsosCategorias;
      $tipouso->Linea= $request->Linea;
      $tipouso->Unidad= $request->Unidad;

      $tipouso->save();
 
      return redirect()->route('tipousos.create') -> with('info','El tipouso fue registrado');
    }
 
 
    public function edit($id)
    {
        $tipouso = TipoUso::find($id); // encontar el tipo de uso a editar y traer toda la información de la tabla tipouso

        $lineas =  MedidaConsumo::where([ ['TipoMedida', '=', 'LINEA']])->get(); // traer las lineas para la lista deplegable
        $unidades =  MedidaConsumo::where([ ['TipoMedida', '=', 'UNIDAD']])->get(); // traer las unidades para la lista deplegable

        $pisostermicos = PisoTermico::orderBy('IDPisoTermico', 'DESC')->get()->pluck('full_name', 'IDPisoTermico'); // traer los pisos termicos o altitud para  la lista deplegable

        $tiposusoscategorias = collect(\DB::select("SELECT CONCAT (tipousoscategorias.NombreCategoria, '  -  ', Modulos.Descripcion ) AS CategoriaTipoUso,tipousoscategorias.IDTipoUsosCategoria FROM tipousoscategorias inner join modulos on modulos.IDModulo = tipousoscategorias.IDModulo"))
     ->pluck('CategoriaTipoUso', 'IDTipoUsosCategoria');   


      

     return view('tipousos.edit',compact('tipouso',$tipouso),['pisostermicos'=>$pisostermicos,'tiposusoscategorias' => $tiposusoscategorias,'lineas'=>$lineas,'unidades'=>$unidades]);




    } 
   public function update(TipoUsoRequest $request,$id)
   {
        $tipouso = TipoUso::find($id);
       
        $tipouso->Nombre= $request->Nombre;
        $tipouso->IDPisoTermico= $request->IDPisoTermico;
        $tipouso->LitrosDiaPersona= $request->LitrosDiaPersona;
        $tipouso->IDTipoUsosCategorias= $request->IDTipoUsosCategorias;
        $tipouso->Linea= $request->Linea;
        $tipouso->Unidad= $request->Unidad;

        $tipouso->save();
        return redirect()->route('tipousos.index') -> with('info','El tipouso fue actualizado');
 
    }

      // #################### LLAMADAS AJAX ####################
    public function consultarLineas($id)
    {
       $lineas = MedidaConsumo::where([
         ['TipoMedida', '=', 'LINEA'],
            ['IDTipoUsosCategoria', '=', $id],
        ])->get();     
    
       return json_encode($lineas->pluck('Nombre','IDMedidaConsumo'));

      
     }

     public function consultarUnidades($id)
    {
       $unidades = MedidaConsumo::where([
         ['TipoMedida', '=', 'UNIDAD'],
            ['IDTipoUsosCategoria', '=', $id],
        ])->get();     
    
       return json_encode($unidades->pluck('Nombre','IDMedidaConsumo'));

      
     }
     
}
