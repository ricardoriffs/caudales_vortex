<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\CalidadAgua;
use caudales\Http\Requests\CalidadAguaRequest;
 
class CalidadAguaController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $calidadesagua=CalidadAgua::orderBy('IDCaptacion', 'DESC')->paginate();
 
        return view('calidadesagua.index',compact('calidadesagua'));
    }
 
    public function create()
    {
 
        return view('calidadesagua.create');
    }
 
 
    public function store(CalidadAguaRequest $request)
    {
 
      $calidadagua = new CalidadAgua;     
      $calidadagua->Apariencia= $request->Apariencia;
      $calidadagua->Olor= $request->Olor;
      $calidadagua->Color= $request->Color;

   if ($request->TipoFormulario == "individual")
     {
               $calidadagua->IDCaptacion= $request->IDCaptacion;
               $calidadagua->save(); 
               return redirect()->route('calidadesagua.create') -> with('info','La calidad del agua fue registrada');

     }elseif ($request->TipoFormulario == "tabular")
         {
            $SessionIDCaptacion = session('No_Captacion');

              if ($request->session()->has('No_Captacion')) {
                  $calidadagua->IDCaptacion=  $SessionIDCaptacion;         
                  $calidadagua->save(); 
                      
                  return redirect()->route('captaciones.create') -> with('info','La calidad del agua fue registrada');
    
                }
                else{

                  //$errors = $validator->messages()->all();                 
                  return redirect()->route('captaciones.create') ->withInput()->withErrors('Por favor registre primero una captación');
                }
         }
    }
 
 
    public function edit($id)
    {
        $calidadagua = CalidadAgua::find($id);
        return view('calidadesagua.edit', compact('calidadagua'));
    } 
   public function update(CalidadAguaRequest $request,$id)
   {
        $calidadagua = CalidadAgua::find($id);
        $calidadagua->Apariencia= $request->Apariencia;
        $calidadagua->Olor= $request->Olor;
        $calidadagua->Color= $request->Color;

        $calidadagua->save();
        return redirect()->route('calidadesagua.index') -> with('info','La calidadagua fue actualizada');
 
   
  }
}
