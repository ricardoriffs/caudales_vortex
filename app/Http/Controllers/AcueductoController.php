<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Acueducto;
use caudales\Http\Requests\AcueductoRequest;
 
class AcueductoController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $acueductos=Acueducto::orderBy('IDCaptacion', 'DESC')->paginate();
 
        return view('acueductos.index',compact('acueductos'));
    }
 
    public function create()
    {
 
        return view('acueductos.create');
    }
 
 
    public function store(AcueductoRequest $request)
    {
 
      $acueducto = new Acueducto;
 
      $acueducto->IDCaptacion= $request->IDCaptacion;
      $acueducto->A_DiametroIN= $request->A_DiametroIN;
      $acueducto->A_LongitudM= $request->A_LongitudM;
      $acueducto->A_Estado= $request->A_Estado;
      $acueducto->D_CapacidadLPS= $request->D_CapacidadLPS;
      $acueducto->D_Estado= $request->D_Estado;
      $acueducto->C_DiametroIN= $request->C_DiametroIN;
      $acueducto->C_Longitdu= $request->C_Longitdu;
      $acueducto->C_Estado= $request->C_Estado;
      $acueducto->P_CapacidadLPS= $request->P_CapacidadLPS;
      $acueducto->P_ActualLPS= $request->P_ActualLPS;
      $acueducto->P_Sedimentacion= $request->P_Sedimentacion;
      $acueducto->P_Floculacion= $request->P_Floculacion;
      $acueducto->P_Filtracion= $request->P_Filtracion;
      $acueducto->P_Desinfeccion= $request->P_Desinfeccion;
      $acueducto->AlmacenaminetoM3= $request->AlmacenaminetoM3;
      $acueducto->TipoTanque= $request->TipoTanque;
      $acueducto->R_DiametroIN= $request->R_DiametroIN;
      $acueducto->R_LongitudM= $request->R_LongitudM;
      $acueducto->R_Estado= $request->R_Estado;
      $acueducto->R_ConexionesLegales= $request->R_ConexionesLegales;
      $acueducto->R_ConexionesIlegales= $request->R_ConexionesIlegales;
      $acueducto->R_Deficit= $request->R_Deficit;
      $acueducto->RT_TipoMedidor= $request->RT_TipoMedidor;
      $acueducto->RT_TarifaMEs= $request->RT_TarifaMEs;
      $acueducto->RT_TarifaAnual= $request->RT_TarifaAnual;
      $acueducto->RT_TarifaUnica= $request->RT_TarifaUnica;
      $acueducto->NoUsuarios= $request->NoUsuarios;
      $acueducto->FechaCenso= $request->FechaCenso;
      $acueducto->PUEEA= $request->PUEEA;




   if ($request->TipoFormulario == "individual")
     {
               $acueducto->IDCaptacion= $request->IDCaptacion;
               $acueducto->save(); 
               return redirect()->route('acueductos.create') -> with('info','El acueducto fue registrado');

     }elseif ($request->TipoFormulario == "tabular")
         {
            $SessionIDCaptacion = session('No_Captacion');

              if ($request->session()->has('No_Captacion')) {
                  $acueducto->IDCaptacion=  $SessionIDCaptacion;         
                  $acueducto->save();  
                      
                  return redirect()->route('captaciones.create') -> with('info','El acueducto fue registrado');
    
                }
                else{
                    
//                  $errors = $validator->messages()->all();                 
                    return redirect()->route('captaciones.create') ->withInput()->withErrors('Por favor registre primero una captación');
                }
         }
    }
 
 
    public function edit($id)
    {
        $acueducto = Acueducto::find($id);
        return view('acueductos.edit', compact('acueducto'));
    } 
   public function update(AcueductoRequest $request,$id)
   {
        $acueducto = Acueducto::find($id);
        $acueducto->A_DiametroIN= $request->A_DiametroIN;
        $acueducto->A_LongitudM= $request->A_LongitudM;
        $acueducto->A_Estado= $request->A_Estado;
        $acueducto->D_CapacidadLPS= $request->D_CapacidadLPS;
        $acueducto->D_Estado= $request->D_Estado;
        $acueducto->C_DiametroIN= $request->C_DiametroIN;
        $acueducto->C_Longitdu= $request->C_Longitdu;
        $acueducto->C_Estado= $request->C_Estado;
        $acueducto->P_CapacidadLPS= $request->P_CapacidadLPS;
        $acueducto->P_ActualLPS= $request->P_ActualLPS;
        $acueducto->P_Sedimentacion= $request->P_Sedimentacion;
        $acueducto->P_Floculacion= $request->P_Floculacion;
        $acueducto->P_Filtracion= $request->P_Filtracion;
        $acueducto->P_Desinfeccion= $request->P_Desinfeccion;
        $acueducto->AlmacenaminetoM3= $request->AlmacenaminetoM3;
        $acueducto->TipoTanque= $request->TipoTanque;
        $acueducto->R_DiametroIN= $request->R_DiametroIN;
        $acueducto->R_LongitudM= $request->R_LongitudM;
        $acueducto->R_Estado= $request->R_Estado;
        $acueducto->R_ConexionesLegales= $request->R_ConexionesLegales;
        $acueducto->R_ConexionesIlegales= $request->R_ConexionesIlegales;
        $acueducto->R_Deficit= $request->R_Deficit;
        $acueducto->RT_TipoMedidor= $request->RT_TipoMedidor;
        $acueducto->RT_TarifaMEs= $request->RT_TarifaMEs;
        $acueducto->RT_TarifaAnual= $request->RT_TarifaAnual;
        $acueducto->RT_TarifaUnica= $request->RT_TarifaUnica;
        $acueducto->NoUsuarios= $request->NoUsuarios;
        $acueducto->FechaCenso= $request->FechaCenso;
        $acueducto->PUEEA= $request->PUEEA;

        $acueducto->save();
        return redirect()->route('acueductos.index') -> with('info','El acueducto fue actualizado');
 
    }
}
