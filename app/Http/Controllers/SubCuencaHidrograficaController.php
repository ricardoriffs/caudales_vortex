<?php

namespace caudales\Http\Controllers;

use Illuminate\Http\Request;
use caudales\SubCuencaHidrografica;
use caudales\Http\Requests\SubCuencaHidrograficaRequest;
use caudales\Log;
use Auth;
use Carbon\Carbon;

class SubCuencaHidrograficaController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $SubCuencasHidrograficas = SubCuencaHidrografica::orderBy('IDCuenca', 'DESC')->paginate();
        return view('subcuencashidrograficas.index', compact('SubCuencasHidrograficas'));

    }

      public function store(SubCuencaHidrograficaRequest $request)
    {
        $SubCuencaHidrografica = new SubCuencaHidrografica;
        $SubCuencaHidrografica->IDSubCuenca = $request->IDSubCuenca; 
        $SubCuencaHidrografica->IDCuenca = $request->IDCuenca; 
        $SubCuencaHidrografica->Nombre = $request->Nombre;     
        $SubCuencaHidrografica->save();
      
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle=  $nombreusuario.' creo la Subcuenca hidrográfica No. '.$request->IDSubCuenca;
        $log->Accion= 'Crear';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();        

     return redirect()->route('subcuencashidrograficas.index') -> with('info','La subcuenca hidrografica ha sido registrada');
    }
}
