<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\UsoPredio;
use caudales\Http\Requests\UsoPredioRequest;
use caudales\Modulo;
 
class UsoPredioController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $usospredio=UsoPredio::orderBy('IDUsoPredio', 'DESC')->paginate();
 
        return view('usospredio.index',compact('usospredio'));
    }
 
    public function create()
    {
       $modulos = Modulo::pluck('Descripcion', 'IDModulo')->toArray();
        return view('usospredio.create',compact('modulos', $modulos));
       
    }
 
 
    public function store(Request $request)
    {
 
      $tipouso = $request->input('IDTipoUso');
      $idmodulo= $request->IDModulo;
      $idtipousoscategoria= $request->IDTipoUsosCategoria;
      $cantidad= $request->Cantidad;
      

    // IDENTIFICAR EL TIPO DE FORMULARIO
    // FORMULARIO DE ADICIONAR ELEMENTOS
   if ($request->TipoFormulario == "individual")
     {
              $NoCaptacion= $request->IDCaptacion; 

               self::identificarModulo($tipouso,$NoCaptacion,$idmodulo,$idtipousoscategoria,$cantidad);            
             
      // FORMULARIO DE NUEVA CAPTACIÓN
     }elseif ($request->TipoFormulario == "tabular")
         {
             $NoCaptacion = session('No_Captacion');

              if ($request->session()->has('No_Captacion')) {
                 
                   self::identificarModulo($tipouso,$NoCaptacion,$idmodulo,$idtipousoscategoria,$cantidad);
                  }
                else{

              
                    return redirect()->route('captaciones.create') ->withInput()->withErrors('Por favor registre primero una captación');
                }

     }



    
   
  

 /*
      $usopredio = new UsoPredio;
 
      $usopredio->IDCaptacion= $request->IDCaptacion;
      $usopredio->IDModulo= $request->IDModulo;
      $usopredio->IDTipoUsosCategoria= $request->IDTipoUsosCategoria;
      $usopredio->IDTipoUso= $request->IDTipoUso;
      $usopredio->Cantidad= $request->Cantidad;
      $usopredio->TotalConsumo= $request->TotalConsumo;

      $usopredio->save();
 
      return redirect()->route('usospredio.create') -> with('info','El usopredio fue registrado');
      */
    }
 
 
    public function edit($id)
    {
        $usopredio = UsoPredio::find($id);
        return view('usospredio.edit', compact('usopredio'));
    } 
   public function update(UsoPredioRequest $request,$id)
   {
        $usopredio = UsoPredio::find($id);
        $usopredio->IDCaptacion= $request->IDCaptacion;
        $usopredio->IDModulo= $request->IDModulo;
        $usopredio->IDTipoUsosCategoria= $request->IDTipoUsosCategoria;
        $usopredio->IDTipoUso= $request->IDTipoUso;
        $usopredio->Cantidad= $request->Cantidad;
        $usopredio->TotalConsumo= $request->TotalConsumo;

        $usopredio->save();
        return redirect()->route('usospredio.index') -> with('info','El usopredio fue actualizado');
 
    }

    /* *****************************  FORMULAS MODULOS DE CONSUMO  *****************************

             ***************************** IDENTIFICAR MODULO  *****************************    */

      public function identificarModulo($tipouso,$NoCaptacion, $idmodulo,$idtipousoscategoria,$cantidad)
      {
        // SI ES MODULO DOMESTICO
          if ($tipouso == 1) 
            {
                self::calcularModuloDomestico($tipouso,$NoCaptacion,$tipouso, $idmodulo,$idtipousoscategoria,$cantidad);

            }
      }



    //       ***************************** MODULO DOMESTICO  *****************************  

      public function calcularModuloDomestico($tipouso,$NoCaptacion,$idmodulo,$idtipousoscategoria,$cantidad,$totalconsumo)
      {
        /*

        SE BORRO LA TABLA DE LA ACUEDUCTOS RESTAURARLA
           $totalusuarios = \DB::table('clientes')
            ->join('barrios', 'clientes.idbarrio', '=', 'barrios.idbarrio')
            ->join('ciudades', 'clientes.idciudad', '=', 'ciudades.idciudad')
            ->orderBy('secuencia', 'ASC')
            ->select('clientes.*', 'barrios.barrio','ciudades.ciudad')
            ->where('estado', 'Aprobado');

      //  = 
       dd($tipouso,$NoCaptacion,$tipouso, $idmodulo,$idtipousoscategoria,$cantidad,$totalconsumo);*/
      }
}
