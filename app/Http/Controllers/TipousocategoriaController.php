<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\TipoUsoCategoria;
use caudales\Http\Requests\TipoUsoCategoriaRequest;
use caudales\Modulo;
 
class TipoUsoCategoriaController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
  
          $tipousoscategorias= \DB::table('tipousoscategorias')
            ->join('modulos', 'modulos.IDModulo', '=', 'tipousoscategorias.IDModulo')
            ->orderBy('IDTipoUsosCategoria', 'DESC')
            ->select('tipousoscategorias.*', 'modulos.Descripcion')
            ->paginate();
 
        return view('tipousoscategorias.index',compact('tipousoscategorias'));
    }
 
    public function create()
    {
        $modulos = Modulo::pluck('Descripcion','IDModulo')->toArray();

        return view('tipousoscategorias.create',compact('modulos',$modulos));
    }
 
 
    public function store(TipoUsoCategoriaRequest $request)
    {
 
      $tipousocategoria = new TipoUsoCategoria;
 
      $tipousocategoria->NombreCategoria= $request->NombreCategoria;
      $tipousocategoria->IDModulo= $request->IDModulo;

      $tipousocategoria->save();
 
      return redirect()->route('tipousoscategorias.create') -> with('info','El tipousocategoria fue registrado');
    }
 
 
    public function edit($id)
    {
        $tipousocategoria = TipoUsoCategoria::find($id);
      

        $tipousocategoria = TipoUsoCategoria::find($id);      
        $modulos = Modulo::pluck('descripcion','idmodulo')->toArray();
        return view('tipousoscategorias.edit',compact('tipousocategoria'),['modulos' =>$modulos]);
    } 
   public function update(TipoUsoCategoriaRequest $request,$id)
   {
        $tipousocategoria = TipoUsoCategoria::find($id);
        $tipousocategoria->NombreCategoria= $request->NombreCategoria;
        $tipousocategoria->IDModulo= $request->IDModulo;

        $tipousocategoria->save();
        return redirect()->route('tipousoscategorias.index') -> with('info','El tipousocategoria fue actualizado');
 
    }
}
