<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Proposito;
use caudales\Http\Requests\PropositoRequest;
 
class PropositoController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        
 
        return view('proposito.index');
    }
 
   
}
