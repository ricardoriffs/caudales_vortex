<?php

namespace caudales\Http\Controllers;

use Illuminate\Http\Request;
use caudales\FuenteHidrografica;
use caudales\Http\Requests\FuenteHidrograficaRequest;
use caudales\Log;
use Auth;
use Carbon\Carbon;

class FuenteHidrograficaController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $FuentesHidrograficas = FuenteHidrografica::orderBy('IDSubCuenca', 'DESC')->paginate();
        return view('fuenteshidrograficas.index', compact('FuentesHidrograficas'));

    }

      public function store(FuenteHidrograficaRequest $request)
    {
        $FuenteHidrografica = new FuenteHidrografica;
        $FuenteHidrografica -> IDSubCuenca = $request-> IDSubCuenca; 
        $FuenteHidrografica -> IDFuente = $request-> IDFuente; 
        $FuenteHidrografica -> Nombre = $request->Nombre;     
        $FuenteHidrografica -> save();
      
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle= $nombreusuario.' creo la Fuente Hidrográfica No. '.$request->IDFuente;
        $log->Accion= 'Crear';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();        
      
      return redirect()->route('fuenteshidrograficas.index') -> with('info','La fuente hidrografica ha sido registrada');
    }
}
