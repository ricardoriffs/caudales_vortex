<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\EstructuraCaptacion;
use caudales\Http\Requests\EstructuraCaptacionRequest;
use caudales\Log;
use Auth;
use Carbon\Carbon;
 
class EstructuraCaptacionController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $estructuracaptaciones=EstructuraCaptacion::orderBy('IDEstructura', 'DESC')->paginate();
 
        return view('estructuracaptaciones.index',compact('estructuracaptaciones'));
    }
 
    public function create()
    {
 
        return view('estructuracaptaciones.create');
    }
 
 
    public function store(EstructuraCaptacionRequest $request)
    {
 
        $estructuracaptacion = new EstructuraCaptacion;

        $estructuracaptacion->Nombre= $request->Nombre;

        $estructuracaptacion->save();
      
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle= $nombreusuario.' creo la Estructura de Captación No. '.$estructuracaptacion->IDEstructura;
        $log->Accion= 'Crear';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();      
 
      return redirect()->route('estructuracaptaciones.create') -> with('info','La estructuracaptacion fue registrada');
    }
 
 
    public function edit($id)
    {
        $estructuracaptacion = EstructuraCaptacion::find($id);
        return view('estructuracaptaciones.edit', compact('estructuracaptacion'));
    } 
   public function update(EstructuraCaptacionRequest $request,$id)
   {
        $estructuracaptacion = EstructuraCaptacion::find($id);
        $estructuracaptacion->Nombre= $request->Nombre;

        $estructuracaptacion->save();
        
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle= $nombreusuario.' modificó la Estructura de Captación No. '.$estructuracaptacion->IDEstructura;
        $log->Accion= 'Modificar';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();  
        
        return redirect()->route('estructuracaptaciones.index') -> with('info','La estructuracaptacion fue actualizada');
 
    }


}
