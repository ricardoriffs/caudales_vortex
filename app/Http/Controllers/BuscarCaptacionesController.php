<?php

namespace caudales\Http\Controllers;

use Illuminate\Http\Request;

class BuscarCaptacionesController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
 			return view('buscarcaptaciones.index');

	}
}
