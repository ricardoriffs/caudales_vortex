<?php

namespace caudales\Http\Controllers;

use Illuminate\Http\Request;
use caudales\Notificacion;
use caudales\Http\Requests\NotificacionRequest;
use Illuminate\Support\Facades\Auth;



class NotificacionController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

    	return view('notificaciones.create');
    }
    public function create()
    {

    	return view('notificaciones.create');
    }
     public function store (NotificacionRequest $request)
    {
    	// Obtiene el objeto del Usuario Autenticado
		$user = Auth::user();

		// Obtiene el ID del Usuario Autenticado
		$id = Auth::id();
    	
 	    $notificacion = new Notificacion;
      $notificacion->notificacion = $request->notificacion;
      $notificacion->id_usuario=$id;
      $notificacion->save();

     return redirect()->route('notificacions.create') -> with('info','La notificación fue registrada');

    
    }
}
