<?php

namespace caudales\Http\Controllers;

use Illuminate\Http\Request;
use caudales\Parametro;
use caudales\Http\Requests\ParametroRequest;

class ParametroController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $parametros = Parametro::orderBy('idparametro', 'DESC')->paginate();

        return view('parametros.index', compact('parametros'));

    }

    public function create()
    {

        return view('parametros.create');
    }


    public function store(ParametroRequest $request)
    {

      $parametro = new Parametro;
      $parametro->Observaciones = $request->Observaciones;
      $parametro->Modulo = $request->Modulo;
      $parametro->Parametro = $request->Parametro;
      $parametro->save();

     return redirect()->route('parametros.create') -> with('info','El parametro fue registrado');
    }

    
    public function edit($id)
    {
        $parametro = Parametro::find($id);
        return view('parametros.edit', compact('parametro'));
    }
    public function update(ParametroRequest $request,$id)
    {
        $parametro = Parametro::find($id);
        $parametro->Observaciones = $request->Observaciones;
        $parametro->Modulo = $request->Modulo;
        $parametro->Parametro = $request->Parametro;
        $parametro->save();
        return redirect()->route('parametros.index') -> with('info','El parametro fue actualizado');

    }
}
