<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\FichaTecnica;
use caudales\Encuestado;
use caudales\Http\Requests\FichaTecnicaRequest;
use caudales\Captacion;
use caudales\Predio;
use caudales\ExpedienteJuridico;
use caudales\Aforo;
use JasperPHP\JasperPHP;
use PHPExcel_Worksheet_MemoryDrawing;
use Response;
 
class FichaTecnicaController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {


                   

        

      
            $captaciones=Captacion::orderBy('IDCaptacion', 'DESC')->paginate();
            $encuestados=Encuestado::orderBy('Cedula', 'DESC')->paginate();
            $predios=Predio::orderBy('CedulaCatastral', 'DESC')->paginate();
            $expedientesjuridicos=ExpedienteJuridico::orderBy('IDExpediente', 'DESC')->paginate();
            $aforos=Aforo::orderBy('IDAforo', 'DESC')->paginate();
         
             

            return view('fichatecnica.index',compact('captaciones',$captaciones),['encuestados' => $encuestados,'predios' => $predios,'expedientesjuridicos' => $expedientesjuridicos,'aforos' => $aforos]);

        
     
    }
 
   public function reportefichatecnica(FichaTecnicaRequest $request)
   {



       /*     \Excel::create('New file', function($excel) {

            $excel->sheet('New sheet', function($sheet) 

            {

                    
                        // manipulate the c
                  

                        $results =  Captacion::all();

                        $objDrawing = new \PHPExcel_Worksheet_Drawing;
                        $objDrawing->setPath(public_path('images/logo_reporte.JPG')); //your image path
                        $objDrawing->setCoordinates('M1');
                        $objDrawing->setWorksheet($sheet);
                        $sheet->
                        loadView('fichatecnica.reportefichatecnica', array('results' => $results))->
                        //Encabezado
                        // Titulo del reporte
                        mergeCells('A1:L1')->mergeCells('A2:L2')->mergeCells('A3:L3')->
                        // Logo del reporte
                        mergeCells('M1:O3')->
                        // Fotografia del predio
                        mergeCells('L5:O10')->

                        //SUBTITULO 
                        mergeCells('A4:K4')-> mergeCells('L4:O4')->

                        getStyle('A1:A3')->
                        getAlignment()->applyFromArray(array('horizontal' => 'center'));
                        //$sheet->loadView('fichatecnica.reportefichatecnica', array('results' => $results));

                          $sheet->cell('B2', function($cell) {
                    $cell->setBackground('#000000');
                    $cell->setFontColor('#ffffff');
                    $cell->setFontSize(16);

             
                     
                         
                        });

            })->export('xls');




*/   



  $NoCapatacion = $request->NoCapatacion;




 \Excel::create('New file', function($excel)use($NoCapatacion) {


//$capataciones =  Captacion::all();

    $encuestados =  \DB::table('encuestados')->where('IDCaptacion', '=', $NoCapatacion)->get();
  
    

$captaciones_predios =  \DB::table('captaciones')
            ->join('predios', 'captaciones.IDCaptacion', '=', 'predios.IDCaptacion')            
            ->select('captaciones.*','predios.NombrePredio','predios.DireccionPredio','predios.Matricula','predios.CedulaCatastral','predios.CoordX','predios.CoordY','predios.Altura','predios.AreaTotal', 'predios.NombreImagen')->where('captaciones.IDCaptacion', $NoCapatacion)
            ->get();



 

  

//$data = json_decode(json_encode((array) $capatacion), true);

\Excel::load('/storage/app/FichaTecnica.xls', function($file) use($encuestados, $captaciones_predios) {

       



  // INFORMACIÓN ENCUESTADOS
  // Celda a partir de la cual se empezara a imprimir la información    
  $celda =6;

  foreach( $encuestados as $encuestado){
       
                $file->setActiveSheetIndex(0)->setCellValue('B'.$celda, $encuestado->Cedula);
                $file->setActiveSheetIndex(0)->setCellValue('B'.$celda,$encuestado->IDTipoDoc);
                $file->setActiveSheetIndex(0)->setCellValue('C'.$celda,$encuestado->Relacion);
                $file->setActiveSheetIndex(0)->mergeCells('D'.$celda.':'.'G'.$celda)->setCellValue('D'.$celda, $encuestado->Nombres.' '.$encuestado->Apellidos);
                $file->setActiveSheetIndex(0)->mergeCells('H'.$celda.':'.'I'.$celda)->setCellValue('H'.$celda,$encuestado->DireccionCorrespondencia);
                $file->setActiveSheetIndex(0)->setCellValue('J'.$celda,$encuestado->Telefono);
                $file->setActiveSheetIndex(0)->setCellValue('K'.$celda,$encuestado->Email);

                $celda++;
          }






  // INFORMACIÓN ENCUESTADOS


 
  foreach($captaciones_predios as $capt_pre){
       
                $file->setActiveSheetIndex(0)->setCellValue('B11', $capt_pre->NombrePredio);
                $file->setActiveSheetIndex(0)->setCellValue('J11',$capt_pre->CedulaCatastral);
                $file->setActiveSheetIndex(0)->setCellValue('C12',$capt_pre->CoordX); 
                $file->setActiveSheetIndex(0)->setCellValue('E12',$capt_pre->CoordY); 
                $file->setActiveSheetIndex(0)->setCellValue('G12',$capt_pre->Altura); 
                $file->setActiveSheetIndex(0)->setCellValue('I12',$capt_pre->AreaTotal); 
                $file->setActiveSheetIndex(0)->setCellValue('I13',$capt_pre->DireccionPredio);
                $file->setActiveSheetIndex(0)->setCellValue('K15',$capt_pre->Dimension);
                
                if($capt_pre->NombreImagen){
                    $gdImage = imagecreatefromjpeg(asset( 'storage/' . $capt_pre->NombreImagen));
                    // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
                    $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                    $objDrawing->setName('Sample image');
                    $objDrawing->setDescription('Sample image');
                    $objDrawing->setImageResource($gdImage);
                    $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
                    $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
                    $objDrawing->setHeight(245);
                    $objDrawing->setWidth(255);
                    $objDrawing->setCoordinates('L5');
                    $objDrawing->setWorksheet($file->setActiveSheetIndex(0));
                }
          }










    
             })->export('xls');



        });                  
                    
              
   }
 
 
    public function consultarFichaTecnica(){

        return view('fichatecnica.consulta');
    }   
 
    public function generarFichaTecnica(FichaTecnicaRequest $request){
   
        $NoCapatacion = $request->NoCapatacion;

        $encuestados =  \DB::table('encuestados')->where('IDCaptacion', '=', $NoCapatacion)->get();


        $captaciones_predios =  \DB::table('captaciones')
                    ->join('predios', 'captaciones.IDCaptacion', '=', 'predios.IDCaptacion')            
                    ->select('captaciones.*','predios.NombrePredio','predios.DireccionPredio','predios.Matricula','predios.CedulaCatastral','predios.CoordX','predios.CoordY','predios.Altura','predios.AreaTotal', 'predios.NombreImagen')->where('captaciones.IDCaptacion', $NoCapatacion)
                    ->first();
        
        $view = view('fichatecnica.partials.tabla')->with([
            'encuestados' => $encuestados,
            'capt_pre' => $captaciones_predios,
            ]);        
        $html = $view->render();

        return Response::json($html);            
    }   
 
 
    public function edit($id)
    {
        $fichatecnica = FichaTecnica::find($id);
        return view('fichatecnica.edit', compact('fichatecnica'));
    } 
   public function update(FichaTecnicaRequest $request,$id)
   {
        $fichatecnica = FichaTecnica::find($id);

        $fichatecnica->save();
        return redirect()->route('fichatecnica.index')->with('info','La fichatecnica fue actualizada');
 
    }
}
