<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Predio;
use caudales\Http\Requests\PredioRequest;
use caudales\Departamento;
use caudales\Municipio;
use Illuminate\Support\Facades\Storage;
use Exception;
 
class PredioController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $predios=Predio::orderBy('CedulaCatastral', 'DESC')->paginate();
 
        return view('predios.index',compact('predios'));
    }
 
    public function create()
    {
        $departamentos = Departamento::pluck('Nombre','IDDepartamento')->toArray();
        $municipios = Municipio::pluck('Nombre','IDMunicipio')->toArray();
   
        return view('predios.create',compact('departamentos',$departamentos),['municipios' => $municipios]);
    }
 
 
    public function store(PredioRequest $request)
    {
       
      $predio = new Predio;
 
      $predio->CedulaCatastral= $request->CedulaCatastral;
      $predio->IDDepartamento= $request->IDDepartamento;
      $predio->IDMunicipio= $request->IDMunicipio;
      $predio->IDVereda= $request->IDVereda;
      $predio->NombrePredio= $request->NombrePredio;
      $predio->DireccionPredio= $request->DireccionPredio;
      $predio->Matricula= $request->Matricula;
      $predio->CoordX= $request->CoordX;
      $predio->CoordY= $request->CoordY;
      $predio->Altura= $request->Altura;
      $predio->AreaTotal= $request->AreaTotal;     
      $file = $request->file('ImagenPredio');
      $extension = $file->getClientOriginalExtension();
                        
       if ($request->TipoFormulario == "individual")
     {
            $storedImage = PredioController::storeImage($request, $file);
            if($storedImage and $request->file('ImagenPredio')->isValid()){
                    $predio->IDCaptacion= $request->IDCaptacion; 
                    $predio->NombreImagen = $file->getFilename().'.'.$extension;
                    $predio->save();     
                    return redirect()->route('captaciones.create')->with('info','El predio fue registrado');
                } else {
                    return redirect()->route('captaciones.create')->withInput()->withErrors('El archivo de imagen NO es válido. Intente nuevamente.');                     
                }                                
     }else
     {
      if ($request->TipoFormulario == "tabular")
         {
            $SessionIDCaptacion = session('No_Captacion');

            $storedImage = PredioController::storeImage($request, $file);
            if ($storedImage and $request->session()->has('No_Captacion') and $request->file('ImagenPredio')->isValid()) {
                $predio->IDCaptacion= $SessionIDCaptacion;   
                $predio->NombreImagen = $file->getFilename().'.'.$extension;
                $predio->save();     
                return redirect()->route('captaciones.create')->with('info','El predio fue registrado');
            }
            else{
                //$errors = $validator->messages()->all();                 
                return redirect()->route('captaciones.create')->withInput()->withErrors('Por favor registre primero una captación, o pulse el botón: agregar el predio a otra captación');
            }
         }

     }
      
    }
    
    public function updateImage($request, $file, $nombreImagen)
    {
        if ($request->hasFile('ImagenPredio')) {
            try {
                Storage::delete($nombreImagen); 
                $extension = $file->getClientOriginalExtension();
                Storage::disk('public')->put($file->getFilename().'.'.$extension,  \File::get($file));
            
                return true;  
            } catch (Exception $e) {
//                return redirect()->route('captaciones.create')->withInput()->withErrors($e); 
            } 
        } else {
//            return redirect()->route('captaciones.create')->withInput()->withErrors('El archivo de imagen NO fue cargado. Por favor agrege una imagen de predio correcta.');                        
        }          
    } 
    
    public function storeImage($request, $file)
    {
        if ($request->hasFile('ImagenPredio')) {
            try {
                $extension = $file->getClientOriginalExtension();
                Storage::disk('public')->put($file->getFilename().'.'.$extension,  \File::get($file));
            
                return true;
            //} catch (Illuminate\Filesystem\FileNotFoundException $e) {    
            } catch (Exception $e) {
                return redirect()->route('captaciones.create')->withInput()->withErrors($e); 
            } 
        } else {
            return redirect()->route('captaciones.create')->withInput()->withErrors('El archivo de imagen NO fue cargado. Por favor agrege una imagen de predio correcta.');                        
        }          
    }    
    
    public function getImage($idcaptacion){
        
        $imagen = Predio::where('IDCaptacion', '=', $idcaptacion)->firstOrFail();
        $nombreImagen = $imagen->NombreImagen;
        $file = Storage::disk('public')->get($nombreImagen);
        
        $response = Response::make($file, 200);
        $response->header("Content-Type", 'image');

        return $response;         
    }
 
    public function edit($id)
    {
        $predio = Predio::find($id);
        $nombreimagen = $predio->NombreImagen;
        $departamentos = Departamento::pluck('Nombre','IDDepartamento')->toArray();
        $municipios = Municipio::pluck('Nombre','IDMunicipio')->toArray();
//        PredioController::getImage($predio->IDCaptacion);
   
        return view('predios.edit',compact('predio'),['departamentos' => $departamentos, 'municipios' => $municipios, 'nombreimagen' => $nombreimagen]);
    } 
    
   public function update(PredioRequest $request,$id)
   {
        $predio = Predio::find($id);
        $predio->IDCaptacion= $request->IDCaptacion;
        $predio->IDDepartamento= $request->IDDepartamento;
        $predio->IDMunicipio= $request->IDMunicipio;
        $predio->IDVereda= $request->IDVereda;
        $predio->NombrePredio= $request->NombrePredio;
        $predio->DireccionPredio= $request->DireccionPredio;
        $predio->Matricula= $request->Matricula;
        $predio->CoordX= $request->CoordX;
        $predio->CoordY= $request->CoordY;
        $predio->Altura= $request->Altura;
        $predio->AreaTotal= $request->AreaTotal;
        $file = $request->file('ImagenPredio');
        $extension = $file->getClientOriginalExtension();

        $nombreImagen = $predio->NombreImagen;
        $existe = Storage::disk('public')->exists($nombreImagen);    
                  
        if($existe and $nombreImagen){
            $storedImage = PredioController::updateImage($request, $file, $nombreImagen);      
        } else {
            $storedImage = PredioController::storeImage($request, $file);                 
        }      
        if($storedImage and $request->file('ImagenPredio')->isValid()){
            $predio->NombreImagen = $file->getFilename().'.'.$extension;
            $predio->save();     
            return redirect()->route('predios.index') -> with('info','El predio fue actualizado');
        } else {
            return redirect()->route('predios.update')->withInput()->withErrors('El archivo de imagen NO es válido. Intente nuevamente.');                     
        }                   
    }
}
