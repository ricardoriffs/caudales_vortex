<?php

namespace caudales\Http\Controllers;
use Illuminate\Http\Request;
use caudales\Notificacion;
use Charts;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $chart = Charts::create('donut', 'highcharts')
          ->title('Ranking encuestadores')
          ->labels(['Julian Contreras', 'Andres quintero', 'Luis Rodriguez'])
          ->values([5,10,20])
          ->dimensions(750,550)
          ->responsive(true);


        $notificacions = Notificacion::orderBy('id','DESC')->paginate();
        return view('home',compact('notificacions'), ['chart' => $chart]);
      //  return view('home');
    }
}
