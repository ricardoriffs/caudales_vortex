<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Dependencia;
use caudales\Http\Requests\DependenciaRequest;
use caudales\Log;
use Auth;
use Carbon\Carbon;
 
class DependenciaController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $dependencias=Dependencia::orderBy('IdDependencia', 'DESC')->paginate();
 
        return view('dependencias.index',compact('dependencias'));
    }
 
    public function create()
    {
 
        return view('dependencias.create');
    }
 
 
    public function store(DependenciaRequest $request)
    {

        $dependencia = new Dependencia;

        $dependencia->Dependencia= $request->Dependencia;
        $dependencia->Acronimo= $request->Acronimo;
        $dependencia->save();
      
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle= $nombreusuario.' creo la Dependencia No. '.$dependencia->IdDependencia;
        $log->Accion= 'Crear';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();            
 
      return redirect()->route('dependencias.create') -> with('info','La dependencia fue registrada');
    }
 
 
    public function edit($id)
    {
        $dependencia = Dependencia::find($id);
        return view('dependencias.edit', compact('dependencia'));
    } 
   public function update(DependenciaRequest $request,$id)
   {
        $dependencia = Dependencia::find($id);
        $dependencia->Dependencia= $request->Dependencia;
        $dependencia->Acronimo= $request->Acronimo;

        $dependencia->save();
        
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle= $nombreusuario.' modificó la Dependencia No. '.$dependencia->IdDependencia;
        $log->Accion= 'Modificar';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();       
        
        return redirect()->route('dependencias.index') -> with('info','La dependencia fue actualizada');
 
    }
}
