<?php

namespace caudales\Http\Controllers\Auth;

use caudales\User;
use caudales\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |   
    | Este controlador gestiona el registro de nuevos usuarios, así como su
    | Validación y creación. Por defecto, este controlador usa un rasgo para
    | Proporcionar esta funcionalidad sin necesidad de ningún código adicional.
    |
    */

    use RegistersUsers;

    /**
     * Lugar dónde se redirige a los usuarios después del registro.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * 
     * Crear una nueva instancia de controlador..
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 
    * Obtener un validador para una solicitud de registro entrante.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
             'dependencia' => 'required|string|min:3',

        ]);
    }

    /**
     * 
     * Cree una instancia de usuario nueva después de un registro válido.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
             'dependencia' => $data['dependencia'],
        ]);
    }
}
