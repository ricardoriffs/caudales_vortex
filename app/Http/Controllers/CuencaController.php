<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Cuenca;
use caudales\Http\Requests\CuencaRequest;
 
class CuencaController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $cuencas=Cuenca::orderBy('IDCuenca', 'DESC')->paginate();
 
        return view('cuencas.index',compact('cuencas'));
    }
 
    public function create()
    {
 
        return view('cuencas.create');
    }
 
 
    public function store(CuencaRequest $request)
    {
 
      $cuenca = new Cuenca;
 
      $cuenca->IDCuenca=$request->IDCuenca;
      $cuenca->Nombre= $request->Nombre;
      $cuenca->Corriente= $request->Corriente;
      $cuenca->CodigoAreaHidrografica= $request->CodigoAreaHidrografica;
      $cuenca->NombreAreaHidrografica= $request->NombreAreaHidrografica;
   
   if ($request->TipoFormulario == "individual")
     {
              $cuenca->IDCaptacion= $request->IDCaptacion;
              $cuenca->save(); 
              return redirect()->route('cuencas.create') -> with('info','La cuenca fue registrada');
     }else
     {
      if ($request->TipoFormulario == "tabular")
         {
            $SessionIDCaptacion = session('No_Captacion');

              if ($request->session()->has('No_Captacion')) {
                  $cuenca->IDCaptacion=  $SessionIDCaptacion;         
                  $cuenca->save(); 
                      
                  return redirect()->route('captaciones.create') -> with('info','La cuenca fue registrada');
    
                }
                else{

                 // $errors = $validator->messages()->all();                 
                    return redirect()->route('captaciones.create') ->withInput()->withErrors('Por favor registre primero una captación');
                }



         }

     }
    }
 
    public function edit($id)
    {
        $cuenca = Cuenca::find($id);
        return view('cuencas.edit', compact('cuenca'));
    } 
   public function update(CuencaRequest $request,$id)
   {
        $cuenca = Cuenca::find($id);
        $cuenca->IDCaptacion= $request->IDCaptacion;
        $cuenca->Nombre= $request->Nombre;
        $cuenca->Corriente= $request->Corriente;
        $cuenca->CodigoAreaHidrografica= $request->CodigoAreaHidrografica;
        $cuenca->NombreAreaHidrografica= $request->NombreAreaHidrografica;

        $cuenca->save();
        return redirect()->route('cuencas.index') -> with('info','La cuenca fue actualizada');
 
    }
}
