<?php

namespace caudales\Http\Controllers;

use Illuminate\Http\Request;
use caudales\Captacion;
use caudales\Http\Requests\CaptacionRequest;
use caudales\TipoFuente;
use caudales\Encuestado;
use caudales\Aforo;
use caudales\Acueducto;
use caudales\CalidadAgua;
use caudales\Cuenca;
use caudales\CuencaHidrografica;
use caudales\FuenteHidrografica;
use caudales\SubCuencaHidrografica;
use caudales\EstructuraCaptacion;
use caudales\Departamento;
use caudales\UsoPredio;
use caudales\TipoDocumento;
use caudales\Log;
use caudales\Http\Requests\LogRequest;
use Carbon\Carbon;
use caudales\Modulo;
use caudales\TipoUso;
use caudales\TipoUsoCategoria;
use Auth;
use Illuminate\Support\Facades\DB;

class CaptacionController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $captaciones = Captacion::orderBy('IDCaptacion', 'DESC')->paginate();

        return view('captaciones.index', compact('captaciones'));
    }

    public function create() {

        $cuencashidrograficas = array();        
        
        $departamentos = ['' => 'Seleccione…'] + Departamento::pluck('nombre', 'iddepartamento')->toArray();
        $dependencias = Captacion::getdependencias();
        $tipofuentes = ['' => 'Seleccione…'] + TipoFuente::pluck('nombre', 'idfuente')->toArray();
        
//        $cuencashidrograficas = ['' => 'Seleccione…'] + CuencaHidrografica::pluck('nombre', 'idcuenca')->toArray();
        

        $cuencashidrograficas = Captacion::getCuencaHidrog();
        $estructuracaptaciones = ['' => 'Seleccione…'] + EstructuraCaptacion::pluck('nombre', 'idestructura')->toArray();
        $relacionesencuestados = array('' => 'Seleccione…', 'Arrendatario' => 'Arrendatario', 'Copropietario' => 'Copropietario', 'Infractor' => 'Infractor', 'Poseedor' => 'Poseedor', 'Propietario' => 'Propietario', 'Otro' => 'Otro');
        $tiposdocumento = ['' => 'Seleccione…'] + TipoDocumento::pluck('descripcion', 'idtipodoc')->toArray();
        $modulos = Modulo::pluck('descripcion', 'idmodulo')->toArray();
        $encuestados = array();
        $aforos = array();
        $acueductos = array();
        $calidadesagua = array();
        $cuenca = array();
        $usospredio = array();

        if (session()->has('No_Captacion')) {

            $NoCaptacion = \Session::get('No_Captacion');

            $captaciones = \DB::table('captaciones')
                    ->join('tipofuente', 'tipofuente.idfuente', '=', 'captaciones.tipofuente')
                    ->join('cuencahidrog', 'cuencahidrog.idcuenca', '=', 'captaciones.idcuenca')
                    ->where('captaciones.idcaptacion', '=', $NoCaptacion)
                    ->select('captaciones.*', 'tipofuente.nombre as NombreTipoFuente', 'cuencahidrog.nombre as NombreCuenca', 'cuencahidrog.idcuenca as IDCuenca')
                    ->paginate(1);

            $encuestados = Encuestado::where('idcaptacion', '=', $NoCaptacion)->paginate();
            $aforos = Aforo::where('idcaptacion', '=', $NoCaptacion)->paginate();
            $acueductos = Acueducto::where('idcaptacion', '=', $NoCaptacion)->paginate();
            $calidadesagua = CalidadAgua::where('idcaptacion', '=', $NoCaptacion)->paginate();
            $cuenca = Cuenca::where('idcaptacion', '=', $NoCaptacion)->paginate();
            $usospredio = UsoPredio::where('idcaptacion', '=', $NoCaptacion)->paginate();

            return view('captaciones.create', compact('tipofuentes', $tipofuentes), ['cuencashidrograficas' => $cuencashidrograficas, 'estructuracaptaciones' => $estructuracaptaciones, 'departamentos' => $departamentos, 'dependencias' => $dependencias,'relacionesencuestados' => $relacionesencuestados, 'tiposdocumento' => $tiposdocumento, 'modulos' => $modulos
                , 'captaciones' => $captaciones, 'encuestados' => $encuestados, 'aforos' => $aforos, 'acueductos' => $acueductos, 'calidadesagua' => $calidadesagua, 'cuenca' => $cuenca, 'usospredio' => $usospredio]);
        } else {

            return view('captaciones.create', compact('tipofuentes', $tipofuentes), ['cuencashidrograficas' => $cuencashidrograficas, 'estructuracaptaciones' => $estructuracaptaciones, 'departamentos' => $departamentos, 'dependencias' => $dependencias, 'relacionesencuestados' => $relacionesencuestados, 'tiposdocumento' => $tiposdocumento, 'modulos' => $modulos, 'encuestados' => $encuestados]);
        }
    }

    public function store(CaptacionRequest $request, LogRequest $logrequest) {

        date_default_timezone_set('America/Bogota');
        $captacion = new Captacion;
        $log = new Log;

        $captacion->TipoFuente = $request->TipoFuente;
        $captacion->CodigoFuente = $request->CodigoFuente;
        $captacion->CoordX = $request->CoordX;
        $captacion->CoordY = $request->CoordY;
        $captacion->Altura = $request->Altura;
        $captacion->Pendiente = $request->Pendiente;
        $captacion->TipoEstructura = $request->TipoEstructura;
        $captacion->Estado = $request->Estado;
        $captacion->Dimension = $request->Dimension;
        $captacion->CapacidadInstaladaLPS = $request->CapacidadInstaladaLPS;
        $captacion->IDCuenca = $request->IDCuenca;
        $captacion->IDSubCuenca = $request->IDSubCuenca;

        if($captacion->save()):
            $idusuario = Auth::user()->id;
            $nombreusuario = Auth::user()->name;
        
            $log->Usuario = $idusuario;
            $log->Detalle = $nombreusuario . ' creo la captación No. ' . $captacion->IDCaptacion;
            $log->Accion = 'Crear';
            $log->Fecha = Carbon::now()->toDateTimeString();

            $log->save();
            session(['No_Captacion' => $captacion->IDCaptacion]);            
        endif;

        return redirect()->route('captaciones.create')->with('info', 'La captacion No.' . $captacion->IDCaptacion . ' fue registrada');
    }

    public function edit($id) {
        $captacion = Captacion::find($id);

        $tipofuentes = TipoFuente::pluck('nombre', 'idfuente')->toArray();
        $cuencashidrograficas = CuencaHidrografica::pluck('nombre', 'idcuenca')->toArray();
        $estructuracaptaciones = EstructuraCaptacion::pluck('nombre', 'idestructura')->toArray();

        return view('captaciones.edit', compact('tipofuentes', $tipofuentes), ['cuencashidrograficas' => $cuencashidrograficas, 'estructuracaptaciones' => $estructuracaptaciones, 'captacion' => $captacion]);
    }

    public function update(CaptacionRequest $request, $id) {
        $captacion = Captacion::find($id);
        $captacion->TipoFuente = $request->TipoFuente;
        $captacion->CodigoFuente = $request->CodigoFuente;
        $captacion->NombreFuente = $request->NombreFuente;
        $captacion->CoordX = $request->CoordX;
        $captacion->CoordY = $request->CoordY;
        $captacion->Altura = $request->Altura;
        $captacion->Pendiente = $request->Pendiente;
        $captacion->TipoEstructura = $request->TipoEstructura;
        $captacion->Estado = $request->Estado;
        $captacion->Dimension = $request->Dimension;
        $captacion->CapacidadInstaladaLPS = $request->CapacidadInstaladaLPS;
        $captacion->IDCuenca = $request->IDCuenca;
        $captacion->IDSubCuenca = $request->IDSubCuenca;

        $captacion->save();
        return redirect()->route('captaciones.index')->with('info', 'La captacion fue actualizada');
    }

    // #################### LLAMADAS AJAX ####################
    public function consultarSubCuencas($id) {
//        $subcuencas = SubCuencaHidrografica::where('idcuenca', '=', $id)->get();
        $subcuencas =  \DB::table('SubCuencasHidrog')
                    ->select('nombre', 'idsubcuenca')
                    ->where('idcuenca', '=', $id)
                    ->get();  

            foreach ($subcuencas as $subcuenca):
                $subcuencashidrograficas[$subcuenca->idsubcuenca] = $subcuenca->idsubcuenca.' - '.$subcuenca->nombre;
            endforeach;
        
        return json_encode($subcuencashidrograficas);
    }

    public function consultarFuentes($id) {

//        $fuentes = FuenteHidrografica::where('idsubcuenca', '=', $id)->get();
        $fuentes =  \DB::table('FuenteHidrog')
                    ->select('nombre', 'idfuente_hidrog')
                    ->where('idsubcuenca', '=', $id)
                    ->get();  

        foreach ($fuentes as $fuente):
            $fuentehidrograficas[$fuente->idfuente_hidrog] = $fuente->idfuente_hidrog.' - '.$fuente->nombre;
        endforeach;        

        return json_encode($fuentehidrograficas);
    }

    public function consultarCategoriasDelTipoUso($id) {
        $tipousoscategorias = TipoUsoCategoria::where('idmodulo', '=', $id)->get();

        return json_encode($tipousoscategorias->pluck('nombrecategoria', 'idtipousoscategoria'));
    }

    public function consultarTiposDeUsos($id) {
        $tiposdeusos = TipoUso::where('idtipousoscategorias', '=', $id)->get();

        return json_encode($tiposdeusos->pluck('nombre', 'idtipouso'));
    }
    public function consultarNombreCuenca($IDCuenca, $IDSubCuenca, $CodigoFuente){
        $nombre = Captacion::nombreCuenca($IDCuenca, $IDSubCuenca, $CodigoFuente);
        //$nombre = "asdasdasd";
        return json_encode($nombre);
    }

}

