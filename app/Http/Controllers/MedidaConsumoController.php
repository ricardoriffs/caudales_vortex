<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\MedidaConsumo;
use caudales\Http\Requests\MedidaConsumoRequest;

 
class MedidaConsumoController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {

           $medidasconsumo = \DB::table('medidasconsumo')
            ->join('tipousoscategorias', 'medidasconsumo.IDTipoUsosCategoria', '=', 'tipousoscategorias.IDTipoUsosCategoria')
            ->join('modulos', 'tipousoscategorias.IDModulo', '=', 'modulos.IDModulo')
            ->orderBy('medidasconsumo.IDTipoUsosCategoria', 'DESC')
            ->select('medidasconsumo.*', 'tipousoscategorias.NombreCategoria','modulos.Descripcion')
            ->paginate();
      
     
 
        return view('medidasconsumo.index',compact('medidasconsumo'));
    }
 
    public function create()
    {
      $tiposusoscategorias = collect(\DB::select("SELECT CONCAT (tipousoscategorias.NombreCategoria, '  -  ', Modulos.Descripcion ) AS CategoriaTipoUso,tipousoscategorias.IDTipoUsosCategoria FROM tipousoscategorias inner join modulos on modulos.IDModulo = tipousoscategorias.IDModulo"))
      ->pluck('CategoriaTipoUso', 'IDTipoUsosCategoria');  

      
        return view('medidasconsumo.create',compact('tiposusoscategorias',$tiposusoscategorias));
    }
 
 
    public function store(MedidaConsumoRequest $request)
    {
 
      $medidaconsumo = new MedidaConsumo;
 
      $medidaconsumo->TipoMedida= $request->TipoMedida;
      $medidaconsumo->IDTipoUsosCategoria= $request->IDTipoUsosCategoria;
      $medidaconsumo->Nombre= $request->Nombre;

      $medidaconsumo->save();
 
      return redirect()->route('medidasconsumo.create') -> with('info','La medidaconsumo fue registrada');
    }
 
 
    public function edit($id)
    {
        $medidaconsumo = MedidaConsumo::find($id);
        $tiposusoscategorias = collect(\DB::select("SELECT CONCAT (tipousoscategorias.NombreCategoria, '  -  ', Modulos.Descripcion ) AS CategoriaTipoUso,tipousoscategorias.IDTipoUsosCategoria FROM tipousoscategorias inner join modulos on modulos.IDModulo = tipousoscategorias.IDModulo"))
        ->pluck('CategoriaTipoUso', 'IDTipoUsosCategoria'); 

        return view('medidasconsumo.edit', compact('medidaconsumo'),['tiposusoscategorias' => $tiposusoscategorias]);
    } 
   public function update(MedidaConsumoRequest $request,$id)
   {
        $medidaconsumo = MedidaConsumo::find($id);
        $medidaconsumo->TipoMedida= $request->TipoMedida;
        $medidaconsumo->IDTipoUsosCategoria= $request->IDTipoUsosCategoria;
        $medidaconsumo->Nombre= $request->Nombre;

        $medidaconsumo->save();
        return redirect()->route('medidasconsumo.index') -> with('info','La medidaconsumo fue actualizada');
 
    }
}
