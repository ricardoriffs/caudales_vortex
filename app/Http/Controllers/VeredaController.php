<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Vereda;
use caudales\Http\Requests\VeredaRequest;
use caudales\Log;
use Auth;
use Carbon\Carbon;

class VeredaController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $veredas=Vereda::orderBy('IDVereda', 'DESC')->paginate();
 
        return view('veredas.index',compact('veredas'));
    }
 
    public function create()
    {
 
        return view('veredas.create');
    }
 
 
    public function store(VeredaRequest $request)
    {
 
        $vereda = new Vereda;

        $vereda->IDVereda= $request->IDVereda;
        $vereda->IDMunicipio= $request->IDMunicipio;
        $vereda->Nombre= $request->Nombre;
        $vereda->Descripcion= $request->Descripcion;
        $vereda->save();
      
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle= $nombreusuario.' creo la Vereda No. '.$request->IDVereda;
        $log->Accion= 'Crear';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();        
 
      return redirect()->route('veredas.create') -> with('info','La vereda fue registrada');
    }
 
 
    public function edit($id)
    {
        $vereda = Vereda::find($id);
        return view('veredas.edit', compact('vereda'));
    } 
   public function update(VeredaRequest $request,$id)
   {
        $vereda = Vereda::find($id);
        $vereda->IDMunicipio= $request->IDMunicipio;
        $vereda->Nombre= $request->Nombre;
        $vereda->Descripcion= $request->Descripcion;

        $vereda->save();
        
        $log = new Log;
        $idusuario = Auth::user()->id;
        $nombreusuario =Auth::user()->name;
      
        $log->Usuario= $idusuario;
        $log->Detalle= $nombreusuario.' modificó la Vereda No. '.$request->IDVereda;
        $log->Accion= 'Modificar';
        $log->Fecha= Carbon::now()->toDateTimeString();
        $log->save();           
        
        return redirect()->route('veredas.index') -> with('info','La vereda fue actualizada');
 
    }

    // #################### LLAMADAS AJAX ####################
    public function consultarVeredas($id)
    {
       $veredas = Vereda::where('idmunicipio', '=', $id)->get();     
    
       return json_encode($veredas->pluck('nombre','idvereda'));
     }
}
