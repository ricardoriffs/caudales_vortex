<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Encuestadores;
use caudales\Http\Requests\EncuestadoresRequest;
 
class EncuestadoresController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $encuestadores=Encuestadores::orderBy('Cedula', 'DESC')->paginate();
 
        return view('encuestadores.index',compact('encuestadores'));
    }
 
    public function create()
    {
 
        return view('encuestadores.create');
    }
 
 
    public function store(EncuestadoresRequest $request)
    {
 
      $encuestadores = new Encuestadores;
 
      $encuestadores->Cedula= $request->Cedula;
      $encuestadores->IDCaptacion = $request->IDCaptacion ;
      $encuestadores->IDTipoDoc= $request->IDTipoDoc;
      $encuestadores->Nombres= $request->Nombres;
      $encuestadores->Apellidos= $request->Apellidos;
      $encuestadores->Cargo= $request->Cargo;
      $encuestadores->Contrato= $request->Contrato;
      $encuestadores->Observaciones= $request->Observaciones;
      $encuestadores->Fecha= $request->Fecha;

      $encuestadores->save();
 
      return redirect()->route('encuestadores.create') -> with('info','El encuestadores fue registrado');
    }
 
 
    public function edit($id)
    {
        $encuestadores = Encuestadores::find($id);
        return view('encuestadores.edit', compact('encuestadores'));
    } 
   public function update(EncuestadoresRequest $request,$id)
   {
        $encuestadores = Encuestadores::find($id);
        $encuestadores->IDCaptacion = $request->IDCaptacion ;
        $encuestadores->IDTipoDoc= $request->IDTipoDoc;
        $encuestadores->Nombres= $request->Nombres;
        $encuestadores->Apellidos= $request->Apellidos;
        $encuestadores->Cargo= $request->Cargo;
        $encuestadores->Contrato= $request->Contrato;
        $encuestadores->Observaciones= $request->Observaciones;
        $encuestadores->Fecha= $request->Fecha;

        $encuestadores->save();
        return redirect()->route('encuestadores.index') -> with('info','El encuestadores fue actualizado');
 
    }
}
