<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Log;
use caudales\Http\Requests\LogRequest;
 
class LogController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $logs=Log::orderBy('IDLog', 'DESC')->paginate();
 
        return view('logs.index',compact('logs'));
    }
 
    public function create()
    {
 
        return view('logs.create');
    }
 
 
 
    
   
}
