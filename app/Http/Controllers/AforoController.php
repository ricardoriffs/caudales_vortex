<?php
 
namespace caudales\Http\Controllers;
 
use Illuminate\Http\Request;
use caudales\Aforo;
use caudales\Http\Requests\AforoRequest;
 
class AforoController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
        $aforos=Aforo::orderBy('IDAforo', 'DESC')->paginate();
 
        return view('aforos.index',compact('aforos'));
    }
 
    public function create()
    {
 
        return view('aforos.create');
    }
 
 
    public function store(AforoRequest $request)
    {
 
      $aforo = new Aforo;
 
    
      $aforo->Diametro= $request->Diametro;
      $aforo->Velocidad= $request->Velocidad;
      $aforo->Hora= $request->Hora;
      $aforo->Profundidad= $request->Profundidad;
      $aforo->Perimetro= $request->Perimetro;
      $aforo->CaudalTotal= $request->CaudalTotal;
      $aforo->Area= $request->Area;
      $aforo->RadioHidraulico= $request->RadioHidraulico;
      $aforo->VelocidadMedia= $request->VelocidadMedia;
      $aforo->FechaAforo= $request->FechaAforo;


       if ($request->TipoFormulario == "individual")
     {
               $aforo->IDCaptacion= $request->IDCaptacion;
               $aforo->save();
                return redirect()->route('aforos.create') -> with('info','El aforo fue registrado');

     }elseif ($request->TipoFormulario == "tabular")
         {
            $SessionIDCaptacion = session('No_Captacion');

              if ($request->session()->has('No_Captacion')) {
                  $aforo->IDCaptacion=  $SessionIDCaptacion;         
                  $aforo->save(); 
                      
                  return redirect()->route('captaciones.create') -> with('info','El aforo fue registrado');  
                }
                else{

//                  $errors = $validator->messages()->all();                 
                    return redirect()->route('captaciones.create') ->withInput()->withErrors('Por favor registre primero una captación');
                }
         }
    }
 
 
    public function edit($id)
    {
        $aforo = Aforo::find($id);
        return view('aforos.edit', compact('aforo'));
    } 
   public function update(AforoRequest $request,$id)
   {
        $aforo = Aforo::find($id);
        $aforo->IDCaptacion= $request->IDCaptacion;
        $aforo->Diametro= $request->Diametro;
        $aforo->Velocidad= $request->Velocidad;
        $aforo->Hora= $request->Hora;
        $aforo->Profundidad= $request->Profundidad;
        $aforo->Perimetro= $request->Perimetro;
        $aforo->CaudalTotal= $request->CaudalTotal;
        $aforo->Area= $request->Area;
        $aforo->RadioHidraulico= $request->RadioHidraulico;
        $aforo->VelocidadMedia= $request->VelocidadMedia;
        $aforo->FechaAforo= $request->FechaAforo;

        $aforo->save();
        return redirect()->route('aforos.index') -> with('info','El aforo fue actualizado');
 
    }
}
