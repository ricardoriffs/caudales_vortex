<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubCuencaHidrograficaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
           'IDSubCuenca' => 'required',
           'IDCuenca' => 'required',
           'Nombre' => 'required'
        ];
    }
}
