<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AforoRequest extends FormRequest
{
   /**
     * Determine si el usuario est� autorizado para realizar esta solicitud.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
      * Obtenga las reglas de validaci�n que se aplican a la solicitud
      *
      * @return array
      */
    public function rules()
    {
    return [      
         'Diametro' => 'required|numeric|min:0',
         'Velocidad' => 'required|numeric|min:0',   
         'Hora' => 'required|date_format:H:i',
         'Profundidad' => 'required|numeric|min:0',
         'Perimetro' => 'required|numeric|min:0',
         'Area' => 'required|numeric|min:0',
         'RadioHidraulico' => 'required|numeric|min:0',
         'VelocidadMedia' => 'required|numeric|min:0',
         'FechaAforo' => 'required|date',        
         'CaudalTotal' => 'required|numeric|min:0',
        ];
    }
}
