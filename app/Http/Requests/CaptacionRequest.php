<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CaptacionRequest extends FormRequest
{
   /**
     * Determine si el usuario est� autorizado para realizar esta solicitud.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
      * Obtenga las reglas de validaci�n que se aplican a la solicitud
      *
      * @return array
      */
    public function rules()
    {
    return [

        'TipoFuente' => 'required',
        'IDCuenca' => 'required',
        'IDSubCuenca' => 'required',        
        'CodigoFuente' => 'required',        
        'CoordX' => 'required|numeric|min:0',
        'CoordY' => 'required|numeric|min:0',
        'Altura' => 'required|numeric|min:0',
        'TipoEstructura' => 'required',
        'Estado' => 'required',
        'Dimension' => 'required|numeric|min:0',
        'CapacidadInstaladaLPS' => 'required|numeric|min:0'
        ];
    }
    
    public function attributes()
    {
    return [
        'TipoFuente' => 'Tipo de Fuente',
        ];
    }    
}
