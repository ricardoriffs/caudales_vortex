<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FuenteHidrograficaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
           'IDFuente' => 'required',
           'IDSubCuenca' => 'required',
           'Nombre' => 'required'
        ];
    }
}
