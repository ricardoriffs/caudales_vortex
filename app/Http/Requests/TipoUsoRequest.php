<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipoUsoRequest extends FormRequest
{
   /**
     * Determine si el usuario est� autorizado para realizar esta solicitud.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
      * Obtenga las reglas de validaci�n que se aplican a la solicitud
      *
      * @return array
      */
    public function rules()
    {
    return [

       
         'Nombre' => 'required',
         'IDPisoTermico' => 'required',
         'LitrosDiaPersona' => 'required',
         'IDTipoUsosCategorias' => 'required',
         'Linea' => 'required',
         'Unidad' => 'required'

        ];
    }
}
