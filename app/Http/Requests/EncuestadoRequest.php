<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EncuestadoRequest extends FormRequest
{
   /**
     * Determine si el usuario est� autorizado para realizar esta solicitud.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
      * Obtenga las reglas de validaci�n que se aplican a la solicitud
      *
      * @return array
      */
    public function rules()
    {
    return [
         'RelacionEncuestado' => 'required',         
         'IDTipoDoc' => 'required|numeric',
         'Cedula' => 'required|alpha_num|min:5',
         'Nombres' => 'required|min:3',
         'Apellidos' => 'required|min:3',
         'Direccion' => 'required|min:5',
         'DireccionCorrespondencia' => 'required|min:5',
         'Telefono' => 'required|numeric|min:0',        
         'Celular' => 'required|numeric|min:0',
         'TipoFormulario' => 'required|in:tabular,individual',
         'Email' => 'required|email'

        ];
    }
}
