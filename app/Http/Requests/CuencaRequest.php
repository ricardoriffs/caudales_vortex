<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CuencaRequest extends FormRequest
{
   /**
     * Determine si el usuario est� autorizado para realizar esta solicitud.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
      * Obtenga las reglas de validaci�n que se aplican a la solicitud
      *
      * @return array
      */
    public function rules()
    {
    return [

         'IDCuenca' => 'required',
         'Nombre' => 'required',
         'Corriente' => 'required',
         'CodigoAreaHidrografica' => 'required',
         'NombreAreaHidrografica' => 'required'

        ];
    }
}
