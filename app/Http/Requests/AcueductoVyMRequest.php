<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AcueductoVyMRequest extends FormRequest
{
   /**
     * Determine si el usuario est� autorizado para realizar esta solicitud.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
      * Obtenga las reglas de validaci�n que se aplican a la solicitud
      *
      * @return array
      */
    public function rules()
    {
    return [
         'NoCaptacion'=> 'required',
         'AduccionDiametroIn' => 'required',
         'AduccionLongitud' => 'required',
         'AduccionEstado' => 'required',
         'DesaneradorLPS' => 'required',
         'DesaneradorEstado' => 'required',
         'ConduccionDiametroIn' => 'required',
         'ConduccionLongitud' => 'required',
         'ConduccionLongitud' => 'required',
         'PotabilizacionLPS' => 'required',
         'PotabilizacionSedimentacion' => 'required',
         'PotabilizacionFlocuacion' => 'required',
         'PotabilizacionFiltracion' => 'required',
         'PotabilizacionDesinfeccion' => 'required',
         'Almacenamiento' => 'required',
         'TipoTanque' => 'required',
         'RedDistribucionVarios' => 'required',
         'RedDistribucionLongitud' => 'required',
         'RedDistribucionEstado' => 'required',
         'RedDistribucionSuscriptores' => 'required',
         'RedDistribucionConexionesIlegales' => 'required',
         'RedDistribucionDeficit' => 'required',
         'RegimenTarifarioTipoMedidor' => 'required',
         'RegimenTarifarioTarifaMes' => 'required',
         'RegimenTarifarioTarifaAnual' => 'required',
         'RegimenTarifarioTarifaUnica' => 'required',
         'NoUsuarios' => 'required',
         'FechaCensoUsuarios' => 'required',
         'PUEEA' => 'required'

        ];
    }
}
