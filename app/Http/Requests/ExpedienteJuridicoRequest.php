<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExpedienteJuridicoRequest extends FormRequest
{
   /**
     * Determine si el usuario est� autorizado para realizar esta solicitud.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
      * Obtenga las reglas de validaci�n que se aplican a la solicitud
      *
      * @return array
      */
    public function rules()
    {
    return [
         'ResolucionNo' => 'required|numeric|min:0',
         'ExpedienteNo' => 'required|numeric|min:0',        
         'Concesion' => 'required|alpha_num|min:5',
         'ResolucionFecha' => 'required|date',
         'ExpedienteFecha' => 'required|date',
         'AprovacionDiseno' => 'required|alpha_num',
         'VigenciaDesde' => 'required|date',
         'VigenciaHasta' => 'required|date',
         'FechaObra' => 'required|date',        
         'CaudalUtilizadoLPS' => 'required',        
         'CaudalConcesionadoLPS' => 'required',
         'OficinaProvincial' => 'required',
         'CaudalUsoDomestico' => 'required',
         'CaudalUsoPecuario' => 'required',
         'CaudalUsoAgricola' => 'required',
         'Observaciones' => 'required|min:5',


        ];
    }
}
