<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AcueductoRequest extends FormRequest
{
   /**
     * Determine si el usuario est� autorizado para realizar esta solicitud.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
      * Obtenga las reglas de validaci�n que se aplican a la solicitud
      *
      * @return array
      */
    public function rules()
    {
    return [

         
         'A_DiametroIN' => 'required|numeric|min:0',
         'A_LongitudM' => 'required|numeric|min:0',
         'A_Estado' => 'required',
         'D_CapacidadLPS' => 'required|numeric|min:0',
         'D_Estado' => 'required',
         'C_DiametroIN' => 'required|numeric|min:0',
         'C_Longitdu' => 'required|numeric|min:0',
         'C_Estado' => 'required',
         'P_CapacidadLPS' => 'required|numeric|min:0',
         'P_ActualLPS' => 'required|numeric|min:0',
         'P_Sedimentacion' => 'required',
         'P_Floculacion' => 'required',
         'P_Filtracion' => 'required',
         'P_Desinfeccion' => 'required',
         'AlmacenaminetoM3' => 'required|numeric|min:0',
         'TipoTanque' => 'required',
         'R_DiametroIN' => 'required|numeric|min:0',
         'R_LongitudM' => 'required|numeric|min:0',
         'R_Estado' => 'required',
         'R_ConexionesLegales' => 'required|numeric|min:0',
         'R_ConexionesIlegales' => 'required|numeric|min:0',
         'R_Deficit' => 'required',
         'RT_TipoMedidor' => 'required',
         'RT_TarifaMEs' => 'required',
         'RT_TarifaAnual' => 'required',
         'RT_TarifaUnica' => 'required',
         'NoUsuarios' => 'required|numeric|min:0',
         'FechaCenso' => 'required|date',
         'PUEEA' => 'required'

        ];
    }
    
    public function attributes()
    {
    return [
        'A_DiametroIN' => 'Diámetro (Aducción)',
        'A_LongitudM' => 'Longitud (Aducción)',
        'A_Estado' => 'Estado (Aducción)',
        'D_CapacidadLPS' => 'Capacidad (Desanerador)',
        'D_Estado' => 'Estado (Desanerador)',
        'C_DiametroIN' => 'Diámetro (Conducción)',
        'C_Longitdu' => 'Longitud (Conducción)',
        'C_Estado' => 'Estado (Conducción)',
        'P_CapacidadLPS' => 'Capacidad (Potabilización)',
        'P_ActualLPS' => 'Actualmente Opera (Potabilización)',
        'P_Sedimentacion' => 'Sedimentación (Potabilización)',
        'P_Floculacion' => 'Floculación (Potabilización)',
        'P_Filtracion' => 'Filtración (Potabilización)',
        'P_Desinfeccion' => 'Desinfección (Potabilización)',
        'AlmacenaminetoM3' => 'Capacidad (Almacenamiento)',
        'TipoTanque' => 'Tipo Tanque',
        'R_DiametroIN' => 'Diámetro (Red Distribución)',
        'R_LongitudM' => 'Longitud (Red Distribución)',
        'R_Estado' => 'Estado (Red Distribución)',
        'R_ConexionesLegales' => 'Conexiones Legales (Red Distribución)',
        'R_ConexionesIlegales' => 'Conexiones Ilegales (Red Distribución)',
        'R_Deficit' => 'Déficit (Red Distribución)',
        'RT_TipoMedidor' => 'Tipo Medidor (Régimen Tarifario)',
        'RT_TarifaMEs' => 'Tarifa Mes (Régimen Tarifario)',
        'RT_TarifaAnual' => 'Tarifa Anual (Régimen Tarifario)',
        'RT_TarifaUnica' => 'Tarifa Única (Régimen Tarifario)',
        'NoUsuarios' => 'No. Usuarios',
        'FechaCenso' => 'Fecha Censo',
        'PUEEA' => 'Pueea'        
        ];
    }     
}
