<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PredioRequest extends FormRequest
{
   /**
     * Determine si el usuario est� autorizado para realizar esta solicitud.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
      * Obtenga las reglas de validaci�n que se aplican a la solicitud
      *
      * @return array
      */
    public function rules()
    {
    return [
         'NombrePredio' => 'required|min:3',
         'CedulaCatastral' => 'required|alpha_num|min:5',
         'Matricula' => 'required|alpha_num|min:5',
         'IDDepartamento' => 'required|numeric',
         'IDMunicipio' => 'required|numeric',
         'IDVereda' => 'required|numeric',
         'DireccionPredio' => 'required|min:5',
         'CoordX' => 'required|numeric|min:0',
         'CoordY' => 'required|numeric|min:0',
         'Altura' => 'required|numeric|min:0',
         'AreaTotal' => 'required|numeric|min:0',
         'ImagenPredio' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048' //|dimensions:max_width=600,max_height=600
        ];
    }
}
