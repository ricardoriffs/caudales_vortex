<?php

namespace caudales\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FichaTecnicaRequest extends FormRequest
{
   /**
     * Determine si el usuario está autorizado para realizar esta solicitud.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
    /**
      * Obtenga las reglas de validación que se aplican a la solicitud
      *
      * @return array
      */
    public function rules()
    {
    return [

         'NoCapatacion' => 'required',
        

        ];
    }
}