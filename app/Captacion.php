<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Captacion extends Model
{
	public $timestamps = false;
	protected $table = 'captaciones';// se define la tabla a utilizar
	protected $perPage = 8; // se define la cantidad de filas en la grilla
	protected $primaryKey='captacion';
	protected $fillable = [
		'TipoFuente','CodigoFuente','CoordX','CoordY','Altura','Pendiente','TipoEstructura','Estado','Dimension','CapacidadInstaladaLPS','IDCuenca','IDSubCuenca'
	];
	public static function getCuencaHidrog(){
		$cuencas =  \DB::table('CuencaHidrog')
                    ->select('nombre', 'idcuenca')
                    ->get();  

        foreach ($cuencas as $cuenca):
            $cuencashidrograficas[$cuenca->idcuenca] = $cuenca->idcuenca.' - '.$cuenca->nombre;
        endforeach;
        
		return $cuencashidrograficas;
	}	

	public static function ultimoId(){
		$ultimoId =  \DB::table('captaciones')
						->select('idcaptacion')
						->orderby('idcaptacion','desc')
						->limit('1')
						->get();
		$id_sig = "";
		foreach ($ultimoId as $tipofuente):
			$id_sig = $tipofuente->idfuente + 1;
		endforeach;
		return $id_sig;
	}
	public static function nombreCuenca($IDCuenca, $IDSubCuenca, $CodigoFuente){
		//return $IDCuenca.", ".$IDSubCuenca.", ".$CodigoFuente;
		$nombrecuenca =  \DB::table('fuentehidrog')
						->where('idfuente_hidrog',$CodigoFuente)
						->where('idsubcuenca',$IDSubCuenca)
						->where('idcuenca',$IDCuenca)
						->select('nombre')
						->get();
		foreach ($nombrecuenca as $res_nombrecuenca):
			$Snombrecuenca = $res_nombrecuenca->nombre;
		endforeach;
		if($Snombrecuenca == null || $Snombrecuenca == "null" || $Snombrecuenca == "")
			$Snombrecuenca = "Nombre no establecido";
		return $Snombrecuenca;
	}
	public static function getdependencias(){
		$dependencias =  \DB::table('dependencias')
                    ->select('dependencia', 'iddependencia')
                    ->get();  
        $Resdependencias[] = 'Seleccione…';
        foreach ($dependencias as $res_dependencias):
            $Resdependencias[$res_dependencias->iddependencia] = $res_dependencias->iddependencia.' - '.$res_dependencias->dependencia;
        endforeach;
        
		return $Resdependencias;
	}	
}
 
 
