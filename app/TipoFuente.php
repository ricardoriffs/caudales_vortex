<?php

namespace caudales;

use Illuminate\Database\Eloquent\Model;

class TipoFuente extends Model
{   public $timestamps = false;
	protected $table = 'tipofuente';// se define la tabla a utilizar
	protected $perPage = 5; // se define la cantidad de filas en la grilla
   	protected $primaryKey='idfuente';
    protected $fillable = [
        'idfuente','nombre'
    ];
    public static function ultimoId(){
        $ultimoId =  \DB::table('tipofuente')
                    ->select('idfuente')
                    ->orderby('idfuente','desc')
                    ->limit('1')
                    ->get();
        $id_sig = "";
        foreach ($ultimoId as $tipofuente):
            $id_sig = $tipofuente->idfuente + 1;
        endforeach;
        return $id_sig;
    }
}
