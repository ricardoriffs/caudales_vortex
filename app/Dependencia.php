<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Dependencia extends Model
{
public $timestamps = false;
protected $table = 'Dependencias';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='IdDependencia';
protected $fillable = [
'IdDependencia','Dependencia','Acronimo'
];
}
 
 
