<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class TipoUso extends Model
{
public $timestamps = false;
protected $table = 'tipousos';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='idtipouso';
protected $fillable = [
'idtipouso','nombre','idpisotermico','litrosdiapersona','idtipousoscategorias','linea','unidad'
];
}
 
 
