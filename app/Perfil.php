<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Perfil extends Model
{
public $timestamps = false;
protected $table = 'Perfiles';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='IdPerfil';
protected $fillable = [
'IdPerfil','Perfil'
];
}
 
 
