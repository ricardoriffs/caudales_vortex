<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Acueducto extends Model
{
public $timestamps = false;
protected $table = 'Acueductos';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='IDCaptacion';
protected $fillable = [
'IDCaptacion','A_DiametroIN','A_LongitudM','A_Estado','D_CapacidadLPS','D_Estado','C_DiametroIN','C_Longitdu','C_Estado','P_CapacidadLPS','P_ActualLPS','P_Sedimentacion','P_Floculacion','P_Filtracion','P_Desinfeccion','AlmacenaminetoM3','TipoTanque','R_DiametroIN','R_LongitudM','R_Estado','R_ConexionesLegales','R_ConexionesIlegales','R_Deficit','RT_TipoMedidor','RT_TarifaMEs','RT_TarifaAnual','RT_TarifaUnica','NoUsuarios','FechaCenso','PUEEA'
];
}
 
 
