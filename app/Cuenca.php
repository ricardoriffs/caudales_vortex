<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Cuenca extends Model
{
public $timestamps = false;
protected $table = 'Cuencas';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='IDCuenca';
protected $fillable = [
'IDCuenca','IDCaptacion','Nombre','Corriente','CodigoAreaHidrografica','NombreAreaHidrografica'
];
}
 
 
