<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class MedidaConsumo extends Model
{
public $timestamps = false;
protected $table = 'MedidasConsumo';// se define la tabla a utilizar
protected $perPage = 10; // se define la cantidad de filas en la grilla
protected $primaryKey='IDMedidaConsumo';
protected $fillable = [
'IDMedidaConsumo','TipoMedida','IDTipoUsosCategoria','Nombre'
];
}
 
 
