<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class EstructuraCaptacion extends Model
{
public $timestamps = false;
protected $table = 'EstructuraCaptaciones';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='IDEstructura';
protected $fillable = [
'IDEstructura','Nombre'
];
}
 
 

 
 
