<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class AcueductoVyM extends Model
{
public $timestamps = false;
protected $table = 'AcueductosVyM';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='IDAcueductosVyM';
protected $fillable = [
'NoCaptacion','IDAcueductosVyM','AduccionDiametroIn','AduccionLongitud','AduccionEstado','DesaneradorLPS','DesaneradorEstado','ConduccionDiametroIn','ConduccionLongitud','ConduccionLongitud','PotabilizacionLPS','PotabilizacionSedimentacion','PotabilizacionFlocuacion','PotabilizacionFiltracion','PotabilizacionDesinfeccion','Almacenamiento','TipoTanque','RedDistribucionVarios','RedDistribucionLongitud','RedDistribucionEstado','RedDistribucionSuscriptores','RedDistribucionConexionesIlegales','RedDistribucionDeficit','RegimenTarifarioTipoMedidor','RegimenTarifarioTarifaMes','RegimenTarifarioTarifaAnual','RegimenTarifarioTarifaUnica','NoUsuarios','FechaCensoUsuarios','PUEEA'
];
}
 
 
