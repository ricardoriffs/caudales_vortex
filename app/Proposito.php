<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Proposito extends Model
{
public $timestamps = false;
protected $table = 'Propositos';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='IDProposito';
protected $fillable = [
'IDProposito','Proposito'
];
}
 
 
