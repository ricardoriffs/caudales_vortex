<?php

namespace caudales;

use Illuminate\Database\Eloquent\Model;

class FuenteHidrografica extends Model
{
    public $timestamps = false;
	protected $table = 'FuenteHidrog';// se define la tabla a utilizar
	protected $perPage = 25; // se define la cantidad de filas en la grilla
    protected $primaryKey='IDFuente';
     protected $fillable = [
        'IDFuente','IDSubCuenca','Nombre'
    ];
}
