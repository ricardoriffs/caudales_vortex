<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class Vereda extends Model
{
public $timestamps = false;
protected $table = 'veredas';// se define la tabla a utilizar
protected $perPage = 5; // se define la cantidad de filas en la grilla
protected $primaryKey='idvereda';
protected $fillable = [
'idvereda','idmunicipio','nombre','descripcion'
];
}
 
 
