<?php

namespace caudales;

use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model
{
   	public $timestamps = false;
	protected $table = 'TipoDocumento';// se define la tabla a utilizar
	protected $perPage = 5; // se define la cantidad de filas en la grilla
  	protected $primaryKey='IDTipodoc';
    protected $fillable = [
        'Descripcion'
    ];
}

