<?php

namespace caudales;

use Illuminate\Database\Eloquent\Model;

class SubCuencaHidrografica extends Model
{
    public $timestamps = false;
	protected $table = 'SubCuencasHidrog';// se define la tabla a utilizar
	protected $perPage = 20; // se define la cantidad de filas en la grilla
  	protected $primaryKey='IDSubCuenca';
    protected $fillable = [
         'IDSubCuenca','IDCuenca','Nombre'
    ];
}
