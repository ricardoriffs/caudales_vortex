<?php
 
namespace caudales;
 
use Illuminate\Database\Eloquent\Model;
 
class CalidadAgua extends Model
{
	public $timestamps = false;
	protected $table = 'CalidadesAgua';// se define la tabla a utilizar
	protected $perPage = 5; // se define la cantidad de filas en la grilla
	protected $primaryKey='IDCaptacion';
	protected $fillable = [
	'IDCaptacion','Apariencia','Olor','Color'
	];
}
 
 
