<?php

namespace caudales;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
     protected $fillable = [
        'notificacion', 'id_usuario',
    ];

}
